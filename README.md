# Final Project at DAN-IT Education

[![pipeline status](https://gitlab.com/final-project-fe20/final/badges/master/pipeline.svg)](https://gitlab.com/final-project-fe20/final/commits/master)

## Description
**ABC-SHOP** - is an e-commerce project, an online store selling computers and electronic devices.

#### Technologies used in the development:
- **Frontend:** React, Redux, React-Router, react-slick, react-slider, formik, query-string, react-google-login, react-number-format, axios etc.
- **Backend:** Node.js, Express
- **Database:** MongoDB
- **Deployment:**   
    - Frontend: GitLab Pages   
    - Backend: Heroku

#### Project participants:
- **@GitVP:**     
Catalog, Cart, Cabinet, ErrorPage, Page404, Header, Card, BreadCrumbs, Pagination, Loader, ErrorBoundary, Comments, PhotoGallery, SimilarModels, ScrollToTop, LinkToTop, API, Logo, Favicon, DB filling and structure, CI/CD, Backend: errors logging, mailing, configs and issues
- **@Semen_Kravchenko:**  
FooterPages, Login, Footer, Button, Input, Modal, NovaPoshta api, Backend: forgot-pass, google-login
- **@misha546756867456:**  
Checkout, BannerList, Populars, Slider
- **@jurii.akhramovich:**  
Product, Promotions