import './theme/styles/App.scss';
import React, { useEffect } from 'react';
import { useLogin } from './hooks/useLogin';
import ErrorMsg from './components/ErrorMsg/ErrorMsg';
import Header from './components/Header/Header';
import AppRoutes from './routes/AppRoutes';
import Footer from './components/Footer/Footer';
import Login from './components/Login/Login';
import LinkToTop from './components/LinkToTop/LinkToTop';

const App = () => {
  const { handleLogin, handleNoAuth } = useLogin();

  useEffect(() => {
    const token = localStorage.getItem('token') || sessionStorage.getItem('token');
    if (token) {
      handleLogin(token);
    } else handleNoAuth()
  }, [handleLogin, handleNoAuth]);

  return (
    <div className='App'>
      <ErrorMsg/>
      <Header/>
      <AppRoutes/>
      <Footer/>
      <Login/>
      <LinkToTop/>
    </div>
  );
}

export default App;