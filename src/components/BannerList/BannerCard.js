import React, {memo} from 'react'
import {Link} from 'react-router-dom'
import PropTypes from 'prop-types';

const BannerCard = ({imgUrl, linkTo, customId}) => {
  return (
    <div className='slider__wrapper-item'>
      <Link to={linkTo}>
        <img src={imgUrl}
             alt={`изображение для ${customId}`}
             className='slider__wrapper-photo'>
        </img>
      </Link>
    </div>
  )
}

BannerCard.propTypes = {
  imgUrl: PropTypes.string.isRequired,
  linkTo: PropTypes.string.isRequired,
  customId: PropTypes.string.isRequired
}

export default memo(BannerCard)