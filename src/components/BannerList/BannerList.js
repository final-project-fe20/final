import React, {useEffect, useState} from 'react'
import Sliders from '../Slider/Slider'
import BannerCard from './BannerCard'
import './BannerList.scss'
import API from '../../utils/API';
import Loader from '../Loader/Loader';

const BannerList = () => {
  const [dbInfo, setDBInfo] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    API.request('slides')
      .then(({data}) => {
        const filteredSlides = data.filter(({section}) => section === 'banner-list');
        setIsLoading(false);
        setDBInfo(filteredSlides);
      })
  }, [])

  const bannerItems = dbInfo.map(i => (
    <BannerCard imgUrl={i.imageUrl}
                customId={i.customId}
                key={i._id}
                linkTo={i.linkTo}
    />
  ))

  if (isLoading) return <Loader height='400px'/>

  return (
    <section className='banner-list'>
      <Sliders
        quantity={1}
        dots={true}>
        {bannerItems}
      </Sliders>
    </section>
  )
}

export default BannerList