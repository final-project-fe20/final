import React, {memo, useCallback, useEffect, useMemo, useState} from 'react';
import {Link, useLocation, useParams} from 'react-router-dom';
import API from '../../utils/API';
import setPageTitle from '../../utils/setPageTitle';
import Loader from '../Loader/Loader';
import './BreadCrumbs.scss';

const PATH_NAMES_MATCHING = {
  cart: 'Моя Корзина',
  catalog: 'Каталог',
  product: 'Каталог',
  cabinet: 'Личный кабинет',
  checkout: 'Оформление заказа'
}

const BreadCrumbs = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [categoryArr, setCategoryArr] = useState([]);
  const [productName, setProductName] = useState('');
  const {pathname} = useLocation();
  const params = useParams();

  const pageName = useMemo(() => {
    const pathNameArr = pathname.slice(1).split('/');
    return pathNameArr[0] !== 'product' ? pathNameArr[0] : 'catalog'
  }, [pathname]);

  const setTitleForPage = () => {
    if (isLoading) return;
    if (params.category) {
      const title = categoryArr.length ? categoryArr[categoryArr.length - 1].name : '';
      setPageTitle(title);
      return;
    }
    if (params.itemNo) {
      setPageTitle(productName);
      return;
    }
    setPageTitle(PATH_NAMES_MATCHING[pageName]);
  }

  const buildCategory = useCallback(async () => {
    if (!Object.values(params)[0]) {
      setCategoryArr([]);
      setIsLoading(false);
      return;
    }
    let category;
    if (params.category) {
      category = params.category
    } else if (params.itemNo) {
      const {data: {products: [product]}} = await API.request(`products/filter?itemNo=${params.itemNo}`);
      category = product.categories;
      setProductName(product.name);
    } else {
      setIsLoading(false);
      return;
    }
    const cat = await API.request(`catalog/${category}`);
    const newCategoryArr = [{
      link: `${pageName}/${category}`,
      name: cat.data.name
    }];
    if (cat.data.parentId === 'null') {
      setCategoryArr(newCategoryArr);
      setIsLoading(false);
      return
    }
    const subCat = await API.request(`catalog/${cat.data.parentId}`)
    newCategoryArr.unshift({
      link: `${pageName}/${subCat.data.id}`,
      name: subCat.data.name
    });
    setCategoryArr(newCategoryArr);
    setIsLoading(false);
  }, [pageName, params])

  useEffect(() => buildCategory(), [buildCategory]);
  useEffect(setTitleForPage, [isLoading, params.category, categoryArr, params.itemNo, pageName, productName]);

  const extraCrumbsItems = categoryArr.map((cat, i) => (
    <li key={i} className='bread-crumbs__item'>
      <Link to={`/${cat.link}`} className='bread-crumbs__link'>
        {cat.name}
      </Link>
    </li>
  ));

  return (
    <ul className='bread-crumbs'>
      <li className='bread-crumbs__item'>
        <Link to='/' className='bread-crumbs__link'>
          Главная
        </Link>
      </li>
      <li className='bread-crumbs__item'>
        <Link to={`/${pageName}`} className='bread-crumbs__link'>
          {PATH_NAMES_MATCHING[pageName]}
        </Link>
      </li>
      {isLoading
        ? <Loader/>
        : (
          <>
            {extraCrumbsItems}
            {productName && <li className='bread-crumbs__item'>
              <Link to={`/product/${params.itemNo}`} className='bread-crumbs__link'>
                {productName}
              </Link>
            </li>}
          </>
          )
      }
    </ul>
  );
}

export default memo(BreadCrumbs);