import React from 'react';
import {render} from '@testing-library/react';
import BreadCrumbs from './BreadCrumbs';

describe('BreadCrumbs test', () => {
  test('smoke test', () => {
    render(<BreadCrumbs/>);
  });

  test('base crumbs are shown with test values', () => {
    const {getAllByTestId} = render(<BreadCrumbs/>);
    const crumbsLinks = getAllByTestId('testLink');
    const mainPageCrumb = crumbsLinks[0];
    const pathNameCrumb = crumbsLinks[1];
    expect(mainPageCrumb).toHaveAttribute('to', '/');
    expect(pathNameCrumb).toHaveAttribute('to', '/testPath');
  });
})