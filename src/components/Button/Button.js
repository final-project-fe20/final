import React, { memo } from 'react';
import PropTypes from 'prop-types';

const Button = (props) => {
  const { children, onClick, type = 'button', 'data-testid': testId = 'testButton', ...rest } = props;

  return (
        <button onClick={onClick} {...rest} type={type} data-testid={testId}>
            {children}
        </button>
  );
};

Button.propTypes = {
  onClick: PropTypes.func,
  type: PropTypes.string,
  'data-testid': PropTypes.string
}

export default memo(Button);