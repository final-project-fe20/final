import { fireEvent, render } from '@testing-library/react';
import Button from './Button';

describe('Testing Button.js', () => {
  test('Smoke test of Button.js', () => {
    render(<Button />);
  });
  test('Testing Button onClick', () => {
    const buttonClickMock = jest.fn();
    const { getByTestId } = render(<Button onClick={buttonClickMock} />);
    const testButton = getByTestId(/testButton/i);
    expect(testButton).toBeInTheDocument();

    expect(buttonClickMock).not.toHaveBeenCalled();
    fireEvent.click(testButton);
    expect(buttonClickMock).toHaveBeenCalledTimes(1);
  });
  test('Testing Button children', () => {
    const testString = 'testButton'
    const { getByText } = render(<Button>{testString}</Button>);
    const testButton = getByText(testString);
    expect(testButton).toBeInTheDocument();
    expect(testButton).toHaveTextContent(testString)
  });
});