import React, {memo} from 'react';
import {Link, NavLink} from 'react-router-dom';
import {useLogin} from '../../hooks/useLogin';
import './CabinetMenu.scss';

const CabinetMenu = () => {
  const {handleLogout} = useLogin();

  return (
    <nav className='cabinet__nav'>
      <ul className='cabinet__menu menu'>
        <li className='menu__item'>
          <NavLink to='/cabinet/personal-info'
                   className='menu__link'
                   activeClassName='menu__link--active'
          >
            Редактировать профиль
          </NavLink>
        </li>
        <li className='menu__item'>
          <NavLink to='/cabinet/change-password'
                   className='menu__link'
                   activeClassName='menu__link--active'
          >
            Изменить пароль
          </NavLink>
        </li>
        <li className='menu__item'>
          <NavLink to='/cabinet/orders'
                   className='menu__link'
                   activeClassName='menu__link--active'
          >
            Мои заказы
          </NavLink>
        </li>
        <Link to='/'
              className='menu__link'
              onClick={() => handleLogout()}
        >
          Выйти
        </Link>
      </ul>
    </nav>
  );
}

export default memo(CabinetMenu);