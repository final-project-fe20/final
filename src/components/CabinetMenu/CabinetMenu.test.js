import React from 'react';
import {render} from '@testing-library/react';
import CabinetMenu from './CabinetMenu';

jest.mock('../../hooks/useLogin', () => ({
  useLogin: () => ({
    handleLogin: jest.fn(),
    handleLogout: jest.fn()
  })
}));

describe('CabinetMenu test', () => {
  test('smoke test', () => {
    render(<CabinetMenu/>);
  });

  test('all NavLinks presents', () => {
    const {getAllByTestId} = render(<CabinetMenu/>);
    getAllByTestId('testNavLink');
  });

  test('logout link is present', () => {
    const {getByTestId} = render(<CabinetMenu/>);
    const logoutLink = getByTestId('testLink');
    expect(logoutLink).toHaveAttribute('to', '/');
  });
})