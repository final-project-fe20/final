import React, {useEffect, useState, memo} from 'react';
import {Field} from 'formik';
import PropTypes from 'prop-types';

const months = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];

const BirthDaySelect = ({setFieldValue, birthArr}) => {
  const [dayValue, setDayValue] = useState('');
  const [monthValue, setMonthValue] = useState('');
  const [yearValue, setYearValue] = useState('');

  const setInitialValues = () => {
    if (birthArr.length) {
      const [day, month, year] = birthArr;
      setDayValue(day);
      setFieldValue('birthDay', day);
      setMonthValue(month)
      setFieldValue('birthMonth', month);
      setYearValue(year)
      setFieldValue('birthYear', year);
    }
  }

  const createDaysItems = () => {
    const daysInMonth = new Date(yearValue, monthValue, 0).getDate();
    const daysItems = [];
    for (let i = 1; i <= daysInMonth; i++) {
      const oneDayItem = <option key={i}>{i}</option>
      daysItems.push(oneDayItem);
    }
    return daysItems;
  }

  const createYearItems = () => {
    const currentYear = new Date().getFullYear()
    const yearsItems = [];
    for (let i = currentYear - 100; i <= currentYear; i++) {
      const oneYearItem = <option key={i}>{i}</option>
      yearsItems.push(oneYearItem);
    }
    return yearsItems;
  }

  const dayChangeHandler = (e) => {
    const {value} = e.target;
    setDayValue(value)
    setFieldValue('birthDay', value);
  }

  const yearChangeHandler = (e) => {
    const {value} = e.target;
    setYearValue(value)
    setFieldValue('birthYear', value);
  }

  const monthChangeHandler = (e) => {
    const {value} = e.target;
    setMonthValue(value)
    setFieldValue('birthMonth', value);
  }

  useEffect(setInitialValues, [birthArr, setFieldValue]);

  const daysItems = createDaysItems();
  const monthsItems = months.map((month, i) => <option key={i} value={i + 1}>{month}</option>);
  const yearsItems = createYearItems();

  return (
    <label className='cabinet__label'>
      День рождения:
      <ul className='cabinet__birthPicker birthPicker'>
        <li className='birthPicker__item'>
          <Field as='select'
                 name='birthDay'
                 className='cabinet__select'
                 value={dayValue}
                 onChange={dayChangeHandler}
                 style={{color: !dayValue ? '#9EA6B2' : ''}}
          >
            <option value='' disabled>День</option>
            {daysItems}
          </Field>
        </li>
        <li className='birthPicker__item'>
          <Field as='select'
                 name='birthMonth'
                 className='cabinet__select'
                 value={monthValue}
                 onChange={monthChangeHandler}
                 style={{color: !monthValue ? '#9EA6B2' : ''}}
          >
            <option value='' disabled>Месяц</option>
            {monthsItems}
          </Field>
        </li>
        <li className='birthPicker__item'>
          <Field as='select'
                 name='birthYear'
                 className='cabinet__select'
                 value={yearValue}
                 onChange={yearChangeHandler}
                 style={{color: !yearValue ? '#9EA6B2' : ''}}
          >
            <option value='' disabled>Год</option>
            {yearsItems}
          </Field>
        </li>
      </ul>
    </label>
  );
}

BirthDaySelect.propTypes = {
  setFieldValue: PropTypes.func.isRequired,
  birthArr: PropTypes.array.isRequired
}

export default memo(BirthDaySelect);