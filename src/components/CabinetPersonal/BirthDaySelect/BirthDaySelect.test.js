import React from 'react';
import {render} from '@testing-library/react';
import BirthDaySelect from './BirthDaySelect';

jest.mock('formik', () => ({Field: ({as, ...rest}) => <select {...rest} data-testid='testInput'/>}));

describe('Cabinet: BirthDaySelect test', () => {
  test('smoke test', () => {
    render(<BirthDaySelect birthArr={[]} setFieldValue={jest.fn()}/>);
  });

  test('date shows correct when birthArr sent', () => {
    const testBirthArr = ['1', '1', '2000'];
    const {getByText} = render(<BirthDaySelect birthArr={testBirthArr} setFieldValue={jest.fn()}/>);
    getByText(testBirthArr[0]);
    getByText(testBirthArr[1]);
    getByText(testBirthArr[2]);
  });

  test('setFieldValue calls properly', () => {
    const setFieldValueMock = jest.fn();
    render(<BirthDaySelect birthArr={[]} setFieldValue={setFieldValueMock}/>);
    expect(setFieldValueMock).not.toHaveBeenCalled();
    render(<BirthDaySelect birthArr={['1', '1', '2000']} setFieldValue={setFieldValueMock}/>);
    expect(setFieldValueMock).toHaveBeenCalledTimes(3);
  });
})