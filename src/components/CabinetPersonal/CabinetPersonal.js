import React, {useState, useMemo, memo} from 'react';
import {Formik, Form, Field} from 'formik';
import validationSchema from './CabinetPersonalSchema';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import API from '../../utils/API';
import {saveCustomerInfo} from '../../store/customer/actions';
import Input from '../Input/Input';
import Button from '../Button/Button';
import BirthDaySelect from './BirthDaySelect/BirthDaySelect';
import './CabinetPersonal.scss';
import { isNull } from 'lodash';

export const CabinetPersonal = (
  {
    customer,
    showOkMsg,
    saveCustomerInfo
  }) => {
  const [isDisabled, setIsDisabled] = useState(false)
  const {
    firstName,
    lastName,
    gender,
    telephone,
    email,
    birthdate,
    isSubscribed
  } = customer;
  const submitHandler = (values, {setSubmitting, errors, setErrors, setFieldValue}) => {
    const {birthDay, birthMonth, birthYear, telephone, checkbox, ...rest} = values;
    const birthdate = (birthDay && birthMonth && birthYear) ? `${birthDay}-${birthMonth}-${birthYear}` : '';
    const newData = {
      ...rest,
      birthdate,
      telephone: `+38${telephone}`,
    }
    API.request(
      'customers',
      'PUT',
      newData
    ).then(() => {
      saveCustomerInfo({...newData, isSubscribed: isSubscribed});
      showOkMsg();
    })
      .catch(err => {
        if (err.response.data.email) {
          setErrors(
            {
              ...errors,
              email: 'Такой email-адрес уже используется',
            }
          );
        }
      })
      .finally(() => setSubmitting(false))
  }

  const handleChange = async (setFieldValue, value) => {
    try {
      setFieldValue(!value);
      setIsDisabled(true);
      const unSubscribeData = {
        letterSubject: 'ABC-SHOP — Отказ от рассылки',
        letterHtml: `<p>Уважаемый, ${firstName}!</p>Уведомляем Вас про отказ от email-рассылки.<br>
                      Если это сделали не вы, то рекомендуем Вам сменить пароль Вашей учетной записи.<br> 
                      Благодарим, что были с нами!</p>`
      }
      const subscribeData = {
        letterSubject: 'ABC-SHOP — Подписка на рассылку',
        letterHtml: '<p>Благодарим за Вашу подписку!</p>'
      }
      if (isNull(isSubscribed)) {
        const res = await API.request(`subscribers`, 'POST', {...subscribeData, email: email});
        saveCustomerInfo({...customer, isSubscribed: res.data.subscriber.enabled});
        showOkMsg();
      } else {
        const res = await API.request(`subscribers/email/${email}`, 'PUT', (!value ? {...subscribeData, enabled: !value} : {...unSubscribeData, enabled: !value}));
        saveCustomerInfo({...customer, isSubscribed: res.data.subscriber.enabled});
        showOkMsg();
      }
    } catch (e) {
      throw new Error(e);
    } finally {
      setIsDisabled(false);
    }
  }

  const birthArr = useMemo(() => birthdate !== '' ? birthdate.split('-') : [], [birthdate]);

  return (
    <Formik
      initialValues={{
        firstName,
        lastName: lastName || '',
        gender: gender || '',
        telephone: telephone ? telephone.substring(3) : '',
        email,
        birthDay: '',
        birthMonth: '',
        birthYear: '',
        checkbox: !!isSubscribed,
      }}
      enableReinitialize={true}
      onSubmit={submitHandler}
      validationSchema={validationSchema}
    >
      {({isSubmitting, setFieldValue, values}) => {
        return (
          <section>
            <h2 className='cabinet__subtitle'>Профиль пользователя</h2>
            <Form
              className='cabinet__info-form'
              noValidate
            >
              <label className='cabinet__label'>
                Имя*:
                <Input
                  name='firstName'
                  type='text'
                  placeholder='Иван'
                  className='cabinet__input'
                />
              </label>
              <label className='cabinet__label'>
                Фамилия*:
                <Input
                  name='lastName'
                  type='text'
                  placeholder='Иванов'
                  className='cabinet__input'
                />
              </label>
              <label className='cabinet__label'>
                Пол:
                <Field as='select' name='gender' className='cabinet__select'>
                  <option value='male'>Мужчина</option>
                  <option value='female'>Женщина</option>
                  <option value=''>Не указано</option>
                </Field>
              </label>
              <label className='cabinet__label'>
                Телефон*:
                <Input
                  name='telephone'
                  type='tel'
                  placeholder='+38 (050) 355 55 55'
                  className='cabinet__input'
                  format='+38 (###) ### ## ##'
                  mask='_'
                />
              </label>
              <label className='cabinet__label'>
                E-mail*:
                <Input
                  name='email'
                  type='email'
                  placeholder='ivanov@gmail.com'
                  className='cabinet__input'
                />
              </label>
              <BirthDaySelect setFieldValue={setFieldValue} birthArr={birthArr}/>
              <label className='cabinet__label cabinet__checkbox-label'>
                <Input
                  type='checkbox'
                  name='checkbox'
                  className='cabinet__checkbox'
                  checked={values.checkbox}
                  onChange={() => handleChange(setFieldValue, values.checkbox)}
                  disabled={isDisabled}
                />
                <span className='cabinet__checkbox-title'>
                  Подписка на рассылку
                </span>
              </label>
              <Button type='submit'
                      className='cabinet__submit-btn'
                      disabled={isSubmitting}
              >
                Готово
              </Button>
            </Form>
          </section>
        )
      }}
    </Formik>
  );
}

const mapStateToProps = (state) => {
  const customer = state.customer.data;
  return {
    customer
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    saveCustomerInfo: (info) => dispatch(saveCustomerInfo(info))
  }
}

CabinetPersonal.propTypes = {
  showOkMsg: PropTypes.func.isRequired
}

export default connect(mapStateToProps, mapDispatchToProps)(memo(CabinetPersonal));