import React from 'react';
import {render, waitFor} from '@testing-library/react';
import {CabinetPersonal} from './CabinetPersonal';

jest.mock('./BirthDaySelect/BirthDaySelect', () => () => <div data-testid='componentTest'>test</div>);
jest.mock('../../utils/API', () => ({request: async () => {}}));

describe('Cabinet: CabinetPersonal test', () => {
  test('smoke test', () => {
    render(<CabinetPersonal showOkMsg={jest.fn()} customer={{
      firstName: '',
      birthdate: '',
      telephone: ''
    }}/>);
  });

  test('testData is shown in form', () => {
    const testFirstName = 'testFirstName';
    const testLastName = 'testLastName';
    const testEmail = 'testEmail@email.com';
    const {getByDisplayValue} = render(<CabinetPersonal showOkMsg={jest.fn()} customer={{
      firstName: testFirstName,
      lastName: testLastName,
      email: testEmail,
      telephone: '',
      birthdate: '',
      checkbox: false
    }}/>);
    getByDisplayValue(testFirstName);
    getByDisplayValue(testLastName);
    getByDisplayValue(testEmail);
  });

  test('saveCustomerData and showOkMsg are called when success response', async () => {
    const showOkMsgMock = jest.fn();
    const saveCustomerMock = jest.fn();
    const testCustomerInfo = {
      firstName: 'testFirstName',
      lastName: 'testLastName',
      email: 'testEmail@email.com',
      telephone: '+380123456789',
      birthdate: '',
      gender: 'male',
      isSubscribed: null,
    }
    const {getByTestId} = render(<CabinetPersonal saveCustomerInfo={saveCustomerMock} showOkMsg={showOkMsgMock} customer={testCustomerInfo}/>);
    const submitBtn = getByTestId('testButton');
    expect(showOkMsgMock).not.toHaveBeenCalled();
    expect(saveCustomerMock).not.toHaveBeenCalled();
    submitBtn.click();
    await waitFor(() => {
      expect(showOkMsgMock).toHaveBeenCalledTimes(1);
      expect(saveCustomerMock).toHaveBeenCalledTimes(1);
      expect(showOkMsgMock).toHaveBeenCalledWith();
      expect(saveCustomerMock).toHaveBeenCalledWith(testCustomerInfo);
    });
  });
})