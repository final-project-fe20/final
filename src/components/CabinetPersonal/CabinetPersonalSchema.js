import * as yup from 'yup';

const FIELD_REQUIRED = 'Поле обязательное для заполнения'

const schema = yup.object().shape({
  firstName: yup
    .string()
    .required(FIELD_REQUIRED)
    .min(2, 'Имя должно содержать не менее 2 символов')
    .max(25, 'Имя должно содержать не более 25 символов')
    .matches(
      /^[a-zA-Zа-яА-Я]+$/,
      'Допустимые символы для имени: a-z, A-Z, а-я, А-Я.'
    ),
  lastName: yup
    .string()
    .required(FIELD_REQUIRED)
    .min(2, 'Фамилия должна содержать не менее 2 символов')
    .max(25, 'Фамилия должна содержать не более 25 символов')
    .matches(
      /^[a-zA-Zа-яА-Я]+$/,
      'Допустимые символы для имени: a-z, A-Z, а-я, А-Я.'
    ),
  telephone: yup
    .string()
    .required(FIELD_REQUIRED)
    .min(10, 'Номер телефона должен содержать минимум 10 символов')
    .matches(
      /^0\d{3}\d{2}\d{2}\d{2}$/,
      'Неверный формат телефона'
    ),
  email: yup
    .string()
    .required(FIELD_REQUIRED)
    .email('Неверный формат email-адреса'),
})

export default schema;