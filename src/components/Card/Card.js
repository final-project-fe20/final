import React, {memo} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {toggleCartProduct} from '../../store/cart/actions';
import {CHARS_MATCHES} from '../../utils/CHARS_MATCHES';
import Button from '../Button/Button';
import CartIcon from './icons/CartIcon';
import './Card.scss';

const IN_STOCK_MSG = 'в наличии';
const EXPECTED_MSG = 'ожидается';
const BESTSELLER_MSG = 'хит продаж';

export const Card = ({
  data: {
    _id,
    itemNo,
    name,
    currentPrice,
    previousPrice,
    imageUrls,
    bestseller = false,
    quantity,
    ...restChars
  },
  carted,
  toggleCartProduct
}) => {
  const createCharsString = () => {
    let charsString = '';
    Object.keys(restChars).forEach(i => {
      if (CHARS_MATCHES[i]) charsString += `${CHARS_MATCHES[i]}: ${restChars[i]} / `;
    })
    return charsString;
  }

  const charsDesc = createCharsString();

  return (
    <article className='card'>
      {bestseller && <div
        className='card__bestseller-badge'>
        <span>{BESTSELLER_MSG}</span>
      </div>}
      <header className='card__header'>
        <Link to={`/product/${itemNo}`} className='card__link'>
          <div className='card__img-wrapper'>
            <img className='card__img' src={imageUrls[0]} alt={`Купить ${name}`} loading='lazy'/>
          </div>
          <h2 className='card__title'>{name}</h2>
        </Link>
      </header>
      <div className='card__body'>
        <h3 className={`card__price ${previousPrice ? 'card__price--discounted' : ''}`}>
          {previousPrice && <p className='card__original-price'>
            <del>
              {previousPrice} грн
            </del>
          </p>}
          {`${currentPrice} грн`}
        </h3>
        <p className={`card__availability card__availability--${quantity ? 'in-stock' : 'expected'}`}>
          {`${quantity ? IN_STOCK_MSG : EXPECTED_MSG}`}
        </p>
        <Button
          className={`card__cart-btn ${carted ? 'card__cart-btn--carted' : ''}`}
          disabled={!quantity}
          onClick={() => toggleCartProduct(_id)}
        >
          <CartIcon/>
        </Button>
      </div>
      <p className='card__desc'>
        {charsDesc}
      </p>
    </article>
  );
}

const mapStateToProps = (state, props) => {
  const carted = state.cart.some(({product}) => product === props.data._id);
  return {
    carted
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    toggleCartProduct: (productId) => dispatch(toggleCartProduct(productId))
  }
}

Card.propTypes = {
  data: PropTypes.shape({
    _id: PropTypes.string.isRequired,
    itemNo: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    currentPrice: PropTypes.number.isRequired,
    previousPrice: PropTypes.number,
    imageUrls: PropTypes.arrayOf(PropTypes.string).isRequired,
    bestseller: PropTypes.bool,
    quantity: PropTypes.number.isRequired
  })
}

export default connect(mapStateToProps, mapDispatchToProps)(memo(Card));