import React from 'react';
import {render} from '@testing-library/react';
import {Card} from './Card';

describe('Card test', () => {
  test('smoke test', () => {
    const testData = {
      _id: '1',
      itemNo: '11',
      name: 'Product',
      currentPrice: 100000,
      previousPrice: 200000,
      imageUrls: [''],
      quantity: 10000
    };
    render(<Card data={testData}/>);
  });

  test('card data shown properly', () => {
    const testData = {
      _id: '1',
      itemNo: '11',
      name: 'Product',
      currentPrice: 100000,
      imageUrls: [''],
      quantity: 10000
    };
    const {getByText, getByAltText} = render(<Card data={testData}/>);
    getByText(testData.name);
    getByText('в наличии');
    getByAltText(`Купить ${testData.name}`);
  });

  test('cartBtn is disabled when quantity is 0', () => {
    const testData = {
      _id: '1',
      itemNo: '11',
      name: 'Product',
      currentPrice: 100000,
      imageUrls: [''],
      quantity: 0
    };
    const {getByTestId} = render(<Card data={testData}/>);
    const cartBtn = getByTestId('testButton');
    expect(cartBtn).toHaveAttribute('disabled');
  });

  test('toggleCartProduct calls when cartBtn clicked', () => {
    const testData = {
      _id: '1',
      itemNo: '11',
      name: 'Product',
      currentPrice: 100000,
      imageUrls: [''],
      quantity: 100
    };
    const toggleCartProductMock = jest.fn();
    const {getByTestId} = render(<Card data={testData} toggleCartProduct={toggleCartProductMock}/>);
    const cartBtn = getByTestId('testButton');
    expect(cartBtn).not.toHaveAttribute('disabled');
    expect(toggleCartProductMock).not.toHaveBeenCalled();
    cartBtn.click();
    expect(toggleCartProductMock).toHaveBeenCalledTimes(1);
    expect(toggleCartProductMock).toHaveBeenCalledWith(testData._id);
  });
})