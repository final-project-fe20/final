const CartIcon = () => (
  <svg width='29' height='26' viewBox='0 0 29 26' fill='none' xmlns='http://www.w3.org/2000/svg'>
    <path
      d='M1 1C1.63819 1.0077 3.14623 1.0231 4.0728 1.0231C5.23102 1.0231 6.45695 1.35267 6.971 2.95084C7.38223 4.22936 9.60352 11.4287 10.6628 14.8685M7.81216 5.55355H28L25.7569 12.6768C20.9436 13.3922 11.2931 14.8836 10.5693 15.0055C9.21409 15.2338 8.65332 16.1927 8.79351 17.0603C8.9337 17.9279 9.54121 18.7041 11.1768 18.7041C11.775 18.7041 20.7268 18.7117 25.5401 18.7117'
      stroke='#51AD33' strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
    <circle cx='12.9031' cy='23.5188' r='1.71074' stroke='#51AD33' strokeWidth='1.5'/>
    <circle cx='22.9651' cy='23.5188' r='1.71074' stroke='#51AD33' strokeWidth='1.5'/>
  </svg>
);

export default CartIcon;
