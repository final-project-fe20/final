import React, {memo} from 'react';
import PropTypes from 'prop-types';
import Card from '../../components/Card/Card';
import './CardList.scss';

const CardList = ({products}) => {
  const cardItems = products.map(i => <Card key={i._id} data={i}/>);

  return (
    <div className='card-list'>
      {cardItems}
      {!products.length && <p data-testid='cardlist-empty-msg' className='card-list__empty-msg'>Товар отсутствует</p>}
    </div>
  );
}

CardList.propTypes = {
  products: PropTypes.arrayOf(PropTypes.object).isRequired
}

export default memo(CardList);