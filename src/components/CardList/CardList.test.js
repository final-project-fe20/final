import React from 'react';
import {render} from '@testing-library/react';
import CardList from './CardList';

jest.mock('../../components/Card/Card', () => () => <div data-testid='cardTest'>test</div>);

describe('CardList test', () => {
  test('smoke test', () => {
    render(<CardList products={[]}/>);
  });

  test('renders proper quantity of cardListItems', () => {
    const testProducts = [
      {_id: 1},
      {_id: 2},
      {_id: 3}
    ]
    const {getAllByTestId} = render(<CardList products={testProducts}/>);
    const cardListItems = getAllByTestId('cardTest');
    expect(cardListItems.length).toBe(testProducts.length);
  });

  test('empty msg shown whep products is empty', () => {
    const {getByTestId} = render(<CardList products={[]}/>);
    getByTestId('cardlist-empty-msg');
  });
})