import React, {memo} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {decCartProductQuantity, incCartProductQuantity, deleteCartProduct} from '../../store/cart/actions';
import Button from '../Button/Button';
import {CrossIcon} from '../../theme/icons/CrossIcon';
import './CartItem.scss';

export const CartItem = ({
  data: {
    _id,
    name,
    itemNo,
    imageUrls,
    currentPrice,
  },
  cartQuantity,
  incCartProductQuantity,
  decCartProductQuantity,
  deleteCartProduct
}) => {
  const getProductName = () => {
    const serialNo = name.match(/\s*\(.*\)\s*$/gm) ? name.match(/\s*\(.*\)\s*$/gm)[0] : '';
    return serialNo.length > 20 ? name.replace(serialNo, '') : name;
  }

  return (
    <li className='cart__item'>
      <div className='cart__name name'>
        <div className='name__img-wrapper'>
          <img className='name__img' src={imageUrls[0]} alt={name}/>
        </div>
        <p className='name__content'>
          <Link to={`product/${itemNo}`} className='name__link'>{getProductName()}</Link>
          <span className='name__subtitle'>Код товара: {itemNo}</span>
        </p>
      </div>
      <p className='cart__price'>{currentPrice} грн</p>
      <div className='cart__quantity quantity'>
        <div className='quantity__wrapper'>
          <Button className='quantity__btn' onClick={() => decCartProductQuantity(_id)}>—</Button>
          <p className='quantity__indicator'>{cartQuantity}</p>
          <Button className='quantity__btn' onClick={() => incCartProductQuantity(_id)}>+</Button>
        </div>
      </div>
      <p className='cart__price'>{cartQuantity * currentPrice} грн</p>
      <Button className='cart__del-btn' onClick={() => deleteCartProduct(_id)}><CrossIcon/></Button>
    </li>
  );
}

const mapStateToProps = (state, props) => {
  const item = state.cart.find(({product}) => product === props.data._id);
  return {
    cartQuantity: item ? item.cartQuantity : 0
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    incCartProductQuantity: (productId) => dispatch(incCartProductQuantity(productId)),
    decCartProductQuantity: (productId) => dispatch(decCartProductQuantity(productId)),
    deleteCartProduct: (productId) => dispatch(deleteCartProduct(productId))
  }
}

CartItem.propTypes = {
  data: PropTypes.object.isRequired
}

export default connect(mapStateToProps, mapDispatchToProps)(memo(CartItem));