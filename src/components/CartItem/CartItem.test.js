import React from 'react';
import {render} from '@testing-library/react';
import {CartItem} from './CartItem';

describe('Cart: CartItem test', () => {
  test('smoke test', () => {
    render(<CartItem data={{name: '', imageUrls: ['']}}/>);
  });

  test('data shown properly', () => {
    const testData = {
      _id: '1',
      itemNo: '11',
      name: 'Product',
      currentPrice: 100000,
      imageUrls: [''],
      quantity: 100
    };
    const testCartQuantity = 124;
    const {getByAltText, getByText} = render(<CartItem data={testData} cartQuantity={testCartQuantity}/>);
    getByAltText(testData.name);
    getByText(testData.name);
    getByText(testCartQuantity);
  });

  test('deleteCartProducts calls when delBtn clicked', () => {
    const testData = {
      _id: '1',
      itemNo: '11',
      name: 'Product',
      currentPrice: 100000,
      imageUrls: [''],
      quantity: 100
    };
    const deleteCartProductMock = jest.fn();
    const {getAllByTestId} = render(<CartItem data={testData} deleteCartProduct={deleteCartProductMock}/>);
    const delBtn = getAllByTestId('testButton')[2];
    expect(deleteCartProductMock).not.toHaveBeenCalled();
    delBtn.click();
    expect(deleteCartProductMock).toHaveBeenCalledTimes(1);
  });
})