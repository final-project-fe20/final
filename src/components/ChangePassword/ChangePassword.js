import React, {useState, memo} from 'react';
import {Form, Formik} from 'formik';
import PropTypes from 'prop-types';
import validationSchema from './ChangePasswordSchema';
import API from '../../utils/API';
import Input from '../Input/Input';
import ClosedEyeIcon from '../Login/icons/ClosedEyeIcon';
import OpenedEyeIcon from '../Login/icons/OpenedEyeIcon';
import Button from '../Button/Button';
import './ChangePassword.scss';

const ChangePassword = ({showOkMsg}) => {
  const [showCurPass, setShowCurPass] = useState(false);
  const [showNewPass, setShowNewPass] = useState(false);
  const [showRepeatPass, setShowRepeatPass] = useState(false);

  const submitHandler = async (values, {setSubmitting, errors, setErrors}) => {
    try {
      await API.request(
        'customers/password',
        'PUT',
        values);
      showOkMsg();
    } catch (err) {
      if (err.response.data.password) {
        setErrors(
          {
            ...errors,
            password: 'Неверный пароль',
          }
        );
      }
    } finally {
      setSubmitting(false)
    }
  }

  return (
    <Formik
      initialValues={{
        password: '',
        newPassword: '',
        repeatPassword: ''
      }}
      onSubmit={submitHandler}
      validationSchema={validationSchema}
    >
      {({isSubmitting}) => {
        return (
          <section className='cabinet__change-password change-password'>
            <h2 className='cabinet__subtitle'>Смена пароля</h2>
            <Form
              className='change-password__form'
              noValidate
            >
              <label className='cabinet__label'>
                Текущий пароль*:
                <Input
                  type={showCurPass ? 'text' : 'password'}
                  name='password'
                  placeholder='******'
                  className='cabinet__input'
                />
                <Button
                  type='button'
                  onClick={() => setShowCurPass(!showCurPass)}
                  className='change-password__show-btn'
                >
                  {showCurPass ? <ClosedEyeIcon/> : <OpenedEyeIcon/>}
                </Button>
              </label>
              <label className='cabinet__label'>
                Новый пароль*:
                <Input
                  type={showNewPass ? 'text' : 'password'}
                  name='newPassword'
                  placeholder='******'
                  className='cabinet__input'
                />
                <Button
                  type='button'
                  onClick={() => setShowNewPass(!showNewPass)}
                  className='change-password__show-btn'
                >
                  {showNewPass ? <ClosedEyeIcon/> : <OpenedEyeIcon/>}
                </Button>
              </label>
              <label className='cabinet__label'>
                Подтверждение пароля*:
                <Input
                  type={showRepeatPass ? 'text' : 'password'}
                  name='repeatPassword'
                  placeholder='******'
                  className='cabinet__input'
                />
                <Button
                  type='button'
                  onClick={() => setShowRepeatPass(!showRepeatPass)}
                  className='change-password__show-btn'
                >
                  {showRepeatPass ? <ClosedEyeIcon/> : <OpenedEyeIcon/>}
                </Button>
              </label>
              <Button type='submit'
                      className='cabinet__submit-btn'
                      disabled={isSubmitting}
              >
                Готово
              </Button>
            </Form>
          </section>
        )
      }
      }
    </Formik>
  )
}

ChangePassword.propTypes = {
  showOkMsg: PropTypes.func.isRequired
}

export default memo(ChangePassword);