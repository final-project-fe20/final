import React from 'react';
import {render, waitFor} from '@testing-library/react';
import ChangePassword from './ChangePassword';
import userEvent from '@testing-library/user-event';

jest.mock('../../utils/API', () => ({
  request: async () => {
  }
}));

describe('Cabinet: ChangePassword test', () => {
  test('smoke test', () => {
    render(<ChangePassword showOkMsg={jest.fn()}/>);
  });

  test('all buttons are present', () => {
    const {getAllByTestId} = render(<ChangePassword showOkMsg={jest.fn()}/>);
    const buttons = getAllByTestId('testButton');
    expect(buttons.length).toBe(4);
  });

  test('showOkMsg is called when success response', async () => {
    const showOkMsgMock = jest.fn();
    const {getAllByTestId} = render(<ChangePassword showOkMsg={showOkMsgMock}/>);
    const buttons = getAllByTestId('testButton');
    const submitBtn = buttons[3];
    userEvent.type(buttons[0], '12345678xx');
    userEvent.type(buttons[1], '12345678xx');
    userEvent.type(buttons[2], '12345678xx');
    expect(showOkMsgMock).not.toHaveBeenCalled();
    submitBtn.click();
    await waitFor(() => {
      expect(showOkMsgMock).toHaveBeenCalledTimes(1);
      expect(showOkMsgMock).toHaveBeenCalledWith();
    });
  });
})