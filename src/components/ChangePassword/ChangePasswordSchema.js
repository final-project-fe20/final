import * as yup from 'yup';

const FIELD_REQUIRED = 'Поле обязательно для заполнения';

const ChangePasswordSchema = yup.object().shape({
  password: yup
    .string()
    .required(FIELD_REQUIRED)
    .matches(
      /^[a-zA-Z0-9]+$/,
      'Пароль должен содержать только латинские символы и цифры'
    )
    .min(7, 'Пароль должен содержать минимум 7 символов')
    .max(30, 'Пароль должен содержать не более 30 символов'),
  newPassword: yup
    .string()
    .required(FIELD_REQUIRED)
    .matches(
      /^[a-zA-Z0-9]+$/,
      'Пароль должен содержать только латинские символы и цифры'
    )
    .min(7, 'Пароль должен содержать минимум 7 символов')
    .max(30, 'Пароль должен содержать не более 30 символов'),
  repeatPassword: yup
    .string()
    .required(FIELD_REQUIRED)
    .oneOf([yup.ref('newPassword')], 'Пароли не совпадают'),
});

export default ChangePasswordSchema;