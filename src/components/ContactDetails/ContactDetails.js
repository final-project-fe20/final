import React, { useState } from 'react'
import validationSchema from './validationSchema'
import Input from '../Input/Input'
import DeliveryMethods from '../DeliveryMethods/DeliveryMethods'
import PaymentMethods from '../PaymentMethods/PaymentMethods'
import './ContactDetails.scss'
import PropTypes from 'prop-types'
import { Formik, Form } from 'formik'
import SelectedProductsModal from '../SelectedProducts/SelectedProductsModal'
import API from '../../utils/API'
import { connect } from 'react-redux'
import { clearCart } from '../../store/cart/actions'
import Loader from '../Loader/Loader'

export const ContactDetails = ({ customer, cart, clearCart }) => {
  const isSignUpUser = !!customer.data.firstName
  const [liqpayData, setLiqpayData] = useState(null);
  const [showModal, setShowModal] = useState(false);
  const [deliveryInfo, setDeliveryInfo] = useState({
    method: '',
    region: '',
    city: '',
    address: ''
  });
  const [paymentMethod, setPaymentMethod] = useState('Наличными при получении')
  const [paymentMethodComment, setPaymentMethodComment] = useState('')
  const [orderNumber, setOrderNumber] = useState(0)
  const [isLoading, setIsLoading] = useState(false)

  const handleSubmit = (values, { errors, setErrors, resetForm }) => {
    setIsLoading(true)

    const { name, email, phone } = values

    const orderInfo = {
      email: email,
      mobile: `+38${phone}`,
      status: 'Принят',
      name: name,
      delivery: deliveryInfo,
      payment: paymentMethod,
      paymentComment: paymentMethodComment
    }

    Object.keys(orderInfo).forEach((key) => (orderInfo[key] === '') && delete orderInfo[key])

    if (isSignUpUser) {
      orderInfo.customerId = customer.data.customerId
    } else if (!isSignUpUser) {
      orderInfo.products = cart
    }

    API.request(
      'orders',
      'POST',
      orderInfo
    ).then(async response => {
      setOrderNumber(response.data.order.orderNo)
      clearCart()
      resetForm()
      if (response.data.order.payment === 'Картой на сайте') {
        const res = await API.request('payment', 'POST', {
          orderId: response.data.order._id,
          amount: response.data.order.totalSum,
          redirectUrl: 'https://final-project-fe20.gitlab.io/final'
        });
        setLiqpayData(res.data);
      }
      setShowModal(!showModal)
    })
      .catch(error => {
        if (error.response.status === 400) {
          setErrors({
            errors
          })
        }
      })
      .finally(() => {
        setIsLoading(false)
      })
  }

  return (
    <>
      {isLoading && <Loader className='contact-details__loader' />}
      <Formik
        initialValues={{
          name: customer.data.firstName,
          email: customer.data.email,
          phone: customer.data.telephone ? customer.data.telephone.substring(3) : ''
        }}
        onSubmit={handleSubmit}
        validationSchema={validationSchema}
        enableReinitialize
      >
        {({ errors, touched, dirty, isValid }) => {
          return (
            <Form
              data-testid='testing-form'
              id='checkout-form'
              className='contact__form'
              noValidate
            >
              <p className='contact__inner-subtitle'>Контактные данные</p>

              <label
                className='contact__form-label'
                htmlFor='name'
              >Имя получателя*
              </label>
              <Input
                data-testid='name-input'
                name='name'
                type='text'
                placeholder='Jason Statham'
                className='contact__form-input'
              />

              <label
                className='contact__form-label'
                htmlFor='phone'
              >Телефон*
              </label>
              <Input
                data-testid='phone-input'
                type='tel'
                name='phone'
                placeholder='+38 (050) 355 55 55'
                className='contact__form-input'
                format='+38 (###) ### ## ##'
                mask='_'
              />

              <label
                className='contact__form-label'
                htmlFor='email'
              >E-mail*
              </label>
              <Input
                data-testid='email-input'
                name='email'
                type='email'
                placeholder='jasonStatham@gmail.com'
                className='contact__form-input'
              />
              <DeliveryMethods
                deliveryInfo={deliveryInfo}
                setDeliveryInfo={setDeliveryInfo}
              />
              <PaymentMethods
                setPaymentMethod={setPaymentMethod}
                setPaymentMethodComment={setPaymentMethodComment}
              />
            </Form>
          )
        }}
      </Formik>
      {showModal && <SelectedProductsModal liqpayData={liqpayData} orderNumber={orderNumber} closeFunction={() => setShowModal(false)} />}
    </>
  )
}

const mapStateToProps = (state) => {
  const customer = state.customer
  const cart = state.cart
  return {
    customer,
    cart
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    clearCart: () => dispatch(clearCart())
  }
}

ContactDetails.propTypes = {
  customer: PropTypes.object,
  cart: PropTypes.array
}

export default connect(mapStateToProps, mapDispatchToProps)(ContactDetails)
