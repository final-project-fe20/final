import React from 'react'
import {
  render,
  screen,
  waitFor,
  fireEvent,
} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import { ContactDetails } from './ContactDetails.js'

jest.mock('../../components/DeliveryMethods/DeliveryMethods', () => () => <div data-testid='testDelivery'>test</div>);
jest.mock('../../components/PaymentMethods/PaymentMethods', () => () => <div data-testid='testPayment'>test</div>);
jest.mock('../../utils/API', () => ({
  request: async () => Promise.resolve({
    data: {
      order: {orderNo: '123'}
    }
  })
}));

describe('Contact details test', () => {
  const customer = {
    data: {
      firstName: 'John',
      telephone: '+380986141978',
      email: 'John@gmail.com'
    }
  }
  const clearCart = jest.fn()

  test('Smoke test', async () => {
    render(<ContactDetails
      customer={customer}
      cart={[]}
      clearCart={clearCart}
    />);
  });

  test('render email input', async () => {
    render(<ContactDetails
        customer={customer}
        cart={[]}
        clearCart={clearCart}
       />);
    await waitFor(() => {
      const emailInput = screen.getByTestId('email-input');
      expect(emailInput).toBeInTheDocument();
      expect(emailInput).toHaveAttribute('type', 'email');

      const phoneInput = screen.getByTestId('phone-input');
      expect(phoneInput).toBeInTheDocument();
      expect(phoneInput).toHaveAttribute('type', 'tel');

      const nameInput = screen.getByTestId('name-input');
      expect(nameInput).toBeInTheDocument();
      expect(nameInput).toHaveAttribute('type', 'text');
    })
  });

  test('check valid input value', async () => {
    render(<ContactDetails
      customer={customer}
      cart={[]}
      clearCart={clearCart}
    />)

    await waitFor(() => {
      const emailInput = screen.getByTestId('email-input');
      userEvent.type(emailInput, customer.email);
      expect(screen.getByTestId('email-input')).toHaveValue(customer.email);

      const phoneInput = screen.getByTestId('phone-input');
      userEvent.type(phoneInput, customer.telephone);
      expect(screen.getByTestId('phone-input')).toHaveValue(customer.telephone);

      const firstNameInput = screen.getByTestId('name-input');
      userEvent.type(firstNameInput, customer.firstNameInput);
      expect(screen.getByTestId('name-input')).toHaveValue(customer.firstNameInput);
    })
  })

  test('clearCart test', async () => {
    const {getByTestId} = render(
      <ContactDetails
        customer={customer}
        cart={[]}
        clearCart={clearCart}
      />
    );
    const form = getByTestId('testing-form');
    await waitFor(() => {
      fireEvent.submit(form)
    })
    expect(clearCart).toHaveBeenCalledTimes(1)
  })

  test('Delivery Methods component renders fine', async () => {
    await waitFor(() => {
      const { getByTestId } = render(<ContactDetails customer={customer}
                                                     cart={[]} clearCart={clearCart}/>);
      getByTestId('testDelivery');
      getByTestId('testPayment');
    })
  })
})