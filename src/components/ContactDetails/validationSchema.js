import * as yup from 'yup';

const FIELD_REQUIRED = 'Поле обязательное для заполнения'

const schema = yup.object().shape({
  name: yup
    .string()
    .required(FIELD_REQUIRED)
    .min(2, 'Имя должно содержать не менее 2 символов')
    .max(25, 'Имя должно содержать не более 25 символов')
    .matches(
      /^[a-zA-Zа-яА-Я]+$/,
      'Допустимые символы для имени: a-z, A-Z, а-я, А-Я.'),
  phone: yup
    .string()
    .required(FIELD_REQUIRED)
    .min(10, 'Номер телефона должен содержать минимум 10 символов')
    .matches(
      /^0\d{3}\d{2}\d{2}\d{2}$/,
      'Неверный формат телефона'
    ),
  email: yup
    .string()
    .email('Невалидный емейл')
    .required(FIELD_REQUIRED),
})

export default schema;
