import React, { memo, useCallback, useEffect, useMemo, useState } from 'react';
import SelectPickupOption from './SelectOptions/SelectPickupOption';
import SelectMailOption from './SelectOptions/SelectMailOption';
import './DeliveryMethods.scss';
import '../ContactDetails/ContactDetails.scss';
import PropTypes from 'prop-types';
import axios from 'axios';

const DeliveryMethods = ({ deliveryInfo, setDeliveryInfo }) => {
  const [activeSelect, setActiveSelect] = useState({ pickup: true });
  const [NPData, setNPData] = useState({
    areas: [],
    cities: [],
    warehouses: []
  });

  const getNPData = useCallback(async (requestMethod, requestMethodProps) => {
    const APIUrl = 'https://api.novaposhta.ua/v2.0/json/';
    const { data } = await axios.post(APIUrl, {
      modelName: 'AddressGeneral',
      calledMethod: `get${requestMethod}`,
      apiKey: 'd6f5552f80618c802dba6e896044383d',
      methodProperties: {
        Language: 'ru',
        ...requestMethodProps
      }
    });
    if (requestMethod === 'Warehouses') return data.data;
    setNPData({ ...NPData, [requestMethod.toLowerCase()]: data.data });
  }, [NPData]);

  const handleSetDeliveryInfo = useCallback((e, prop, value, rest) => {
    if (value === 'default') value = e.target.options[e.target.options.selectedIndex].label
    setDeliveryInfo({...deliveryInfo, [prop]: value, ...rest})
  }, [deliveryInfo, setDeliveryInfo]);

  const areasSelectHandleChange = useCallback(async (e) => {
    setActiveSelect({mailAreas: true, mailCities: false, mailWarehouses: false});
    await getNPData('Cities', {
      AreaRef: e.target.options[e.target.options.selectedIndex].value
    });
    setActiveSelect({mailAreas: true, mailCities: true, mailWarehouses: false});
    handleSetDeliveryInfo(e, 'region', 'default', { city: 'Нет данных', address: 'Нет данных'});
  }, [getNPData, handleSetDeliveryInfo]);

  const citiesSelectHandleChange = useCallback(async (e) => {
    setActiveSelect({mailAreas: true, mailCities: true, mailWarehouses: false});
    const warehousesStandard = await getNPData('Warehouses', {
      CityRef: e.target.options[e.target.options.selectedIndex].value,
      TypeOfWarehouseRef: '9a68df70-0267-42a8-bb5c-37f427e36ee4'
    });
    const warehousesLimitedWeight = await getNPData('Warehouses', {
      CityRef: e.target.options[e.target.options.selectedIndex].value,
      TypeOfWarehouseRef: '841339c7-591a-42e2-8233-7a0a00f0ed6f'
    });
    const allWarehouses = [...warehousesStandard, ...warehousesLimitedWeight].sort((a, b) => a.Number - b.Number);
    setNPData({ ...NPData, warehouses: allWarehouses });
    setActiveSelect({mailAreas: true, mailCities: true, mailWarehouses: true});
    handleSetDeliveryInfo(e, 'city', 'default', { address: 'Нет данных' });
  }, [NPData, getNPData, handleSetDeliveryInfo]);

  const warehousesSelectHandleChange = useCallback((e) => {
    handleSetDeliveryInfo(e, 'address', 'default');
  }, [handleSetDeliveryInfo]);

  const showAreasOptions = useMemo(() => NPData.areas.map(e => <option key={e.Ref} value={e.Ref}>{e.DescriptionRu}</option>), [NPData.areas]);
  const showCitiesOptions = useMemo(() => NPData.cities.map(e => <option key={e.Ref} value={e.Ref}>{e.DescriptionRu}</option>), [NPData.cities]);
  const showWarehousesOptions = useMemo(() => NPData.warehouses.map(e => <option key={e.Ref}>{e.DescriptionRu}</option>), [NPData.warehouses]);

  useEffect(() => {
    !showAreasOptions.length && getNPData('Areas');
  }, [getNPData, showAreasOptions.length]);

  useEffect(() => {
    activeSelect.pickup && setDeliveryInfo({method: 'Самовывоз', region: 'Киевская', city: 'Киев', address: 'ул. Даниила Щербаковского 12'});
    activeSelect.mailAreas && setDeliveryInfo({method: 'Новая Почта', region: 'Нет данных', city: 'Нет данных', address: 'Нет данных'});
    activeSelect.courier && setDeliveryInfo({method: 'Курьер', region: 'Киевская', city: 'Киев', address: 'Нет данных'});
  }, [activeSelect.pickup, activeSelect.mailAreas, activeSelect.courier, setDeliveryInfo]);

  return (
    <>
      <h2 className='delivery-subtitle'>Способы доставки</h2>
      <div className='delivary__form-inner'>
        <input
          type='radio'
          id='pickup'
          name='delivery'
          onClick={() => setActiveSelect({ pickup: true })}
          defaultChecked
        />
        <label className='delivary__form-label' htmlFor='pickup'>
          Самовывоз из пункта выдачи
        </label>
      </div>
      {activeSelect.pickup && (
        <select
          name='select'
          className='delivery-select'
          onChange={(e) => handleSetDeliveryInfo(e, 'address', e.target.options[e.target.options.selectedIndex].label.split(', ')[1])}
        >
          <SelectPickupOption />
        </select>
      )}

      <div className='delivary__form-inner'>
        <input
          type='radio'
          id='mail'
          name='delivery'
          value='mail'
          onClick={() => !activeSelect.mailAreas && setActiveSelect({mailAreas: true, mailCities: false, mailWarehouses: false})}
        />
        <label className='delivary__form-label' htmlFor='mail'>
          Новая почта (в отделение)
        </label>
      </div>
      {activeSelect.mailAreas &&
        <SelectMailOption
          options={showAreasOptions}
          onChange={e => areasSelectHandleChange(e)}
          label='область'
      />}
      {activeSelect.mailCities &&
        <SelectMailOption
          options={showCitiesOptions}
          onChange={e => citiesSelectHandleChange(e)}
          label='населенный пункт'
      />}
      {activeSelect.mailWarehouses &&
        <SelectMailOption
          options={showWarehousesOptions}
          onChange={e => warehousesSelectHandleChange(e)}
          label='отделение'
      />}

      <div className='delivary__form-inner'>
        <input
          type='radio'
          id='courier'
          name='delivery'
          value='courier'
          onClick={() => setActiveSelect({ courier: true })}
          className='delivery__form-input'
        />
        <label className='delivary__form-label' htmlFor='courier'>
          Курьерская доставка
        </label>
      </div>
      {activeSelect.courier && (
        <input
          className='delivery__form-input'
          type='text'
          id='name'
          name='name'
          placeholder='Введите адрес для курьерской доставки'
          onChange={(e) => handleSetDeliveryInfo(e, 'address', e.target.value)}
        />
      )}
    </>
  );
};

DeliveryMethods.propTypes = {
  deliveryInfo: PropTypes.objectOf(PropTypes.string.isRequired).isRequired,
  setDeliveryInfo: PropTypes.func.isRequired
};

export default memo(DeliveryMethods);
