import React from 'react';
import { render } from '@testing-library/react';
import DeliveryMethods from './DeliveryMethods';

const testDeliveryInfo = {};
const testSetDeliveryInfo = jest.fn();

describe('Testing DeliveryMethods.js', () => {
  test('Smoke test', () => {
    render(<DeliveryMethods deliveryInfo={testDeliveryInfo} setDeliveryInfo={testSetDeliveryInfo} />)
  });
});