import React, { memo } from 'react';
import PropTypes from 'prop-types';

const SelectMailOption = ({ options, onChange, label }) => {
  return (
    <select name='select' className='delivery-select' onChange={onChange} defaultValue='defaultSelect'>
      <option value='defaultSelect' disabled>
        {!options.length ? 'Не найдено' : `Выберите ${label}`}
      </option>
      {options}
    </select>
  )
}

SelectMailOption.propTypes = {
  options: PropTypes.array.isRequired,
  onChange: PropTypes.func.isRequired,
  label: PropTypes.string.isRequired
}

export default memo(SelectMailOption);