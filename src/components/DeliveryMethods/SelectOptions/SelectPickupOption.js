import React from 'react'
import '../DeliveryMethods.scss'

const SelectPickupOption = () => {
  return (<>
      <option value='defaultOption' disabled>Выберите пункт самовывоза</option>
      <option>г.Киев, ул. Даниила Щербаковского 12</option>
      <option>г.Киев, ул. Б. Васильковская (Красноармейская) 132</option>
      <option>г.Киев, ул. Золотоворотская 65</option>
    </>
  )
}

export default SelectPickupOption