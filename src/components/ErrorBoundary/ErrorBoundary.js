import React, {PureComponent} from 'react';
import {withRouter} from 'react-router-dom';
import ErrorPage from '../../pages/ErrorPage/ErrorPage';
import API from '../../utils/API';

class ErrorBoundary extends PureComponent {
  state = {
    errorPresent: false
  }

  static getDerivedStateFromError () {
    return {errorPresent: true}
  }

  componentDidCatch (error, errorInfo) {
    this.setState({errorPresent: true});
    API.request('errors', 'POST', {
      error: error.toString(),
      errorInfo: {...errorInfo, componentStack: errorInfo.componentStack.replace(/\n/g, '').trim()}
    });
  }

  componentDidUpdate (prevProps, prevState) {
    const {location} = this.props;
    const {errorPresent} = prevState;

    if (errorPresent && location.pathname !== prevProps.location.pathname) {
      this.setState({errorPresent: false});
    }
  }

  render () {
    const {errorPresent} = this.state;
    const {children} = this.props;

    if (errorPresent) {
      return <ErrorPage/>
    }

    return children
  }
}

export default withRouter(ErrorBoundary);