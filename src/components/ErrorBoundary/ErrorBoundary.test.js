import React from 'react';
import {render} from '@testing-library/react';
import ErrorBoundary from './ErrorBoundary';

jest.mock('react-router-dom', () => ({
  withRouter: (Component) => (props) => <Component {...props} />
}));
jest.mock('../../pages/ErrorPage/ErrorPage', () => () => <div data-testid='error-page'>error</div>);

describe('ErrorBoundary test', () => {
  test('smoke test', () => {
    render(
      <ErrorBoundary>
        <div>test</div>
      </ErrorBoundary>
    );
  });

  test('ErrorPage renders when error catched', () => {
    const ProblemComponent = () => {
      throw new Error('error')
    };
    console.log = jest.fn();
    console.error = jest.fn();
    const {getByTestId} = render(<ErrorBoundary><ProblemComponent/></ErrorBoundary>);
    getByTestId('error-page');
  });
})