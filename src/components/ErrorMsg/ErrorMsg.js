import React, {memo} from 'react';
import {connect} from 'react-redux';
import './ErrorMsg.scss';

export const ErrorMsg = ({isVisible, msgText}) => {
  return (
    <p className={`error-msg ${isVisible ? 'error-msg--show' : ''}`}>
      {msgText}
    </p>
  );
}

const mapStateToProps = (state) => {
  const isVisible = state.errorMsg.isVisible;
  const msgText = state.errorMsg.text;
  return {
    isVisible,
    msgText
  }
}

export default connect(mapStateToProps)(memo(ErrorMsg));