import React from 'react';
import {render} from '@testing-library/react';
import {ErrorMsg} from './ErrorMsg';

describe('ErrorMsg test', () => {
  test('smoke test', () => {
    render(<ErrorMsg/>);
  });

  test('sent error message shown properly', () => {
    const testErrorMsg = 'testErrorMsg';
    const {getByText} = render(<ErrorMsg msgText={testErrorMsg}/>);
    getByText(testErrorMsg);
  });
})