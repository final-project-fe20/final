import React, {useCallback, useEffect, useState, memo} from 'react';
import RangeSlider from 'react-slider';
import {useParams} from 'react-router-dom';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import API from '../../utils/API';
import {useUrlQuery} from '../../hooks/useUrlQuery';
import {setFilterQuery} from '../../store/catalog/actions';
import Button from '../Button/Button';
import FilterItem from './FilterItem/FilterItem';
import FilterPopup from './FilterPopup/FilterPopup';
import Loader from '../Loader/Loader';
import './Filter.scss';

const Filter = ({mobileFilterShow, popupShow, setFilterQuery, products}) => {
  const [isLoading, setIsLoading] = useState(true);
  const [categoryName, setCategoryName] = useState('Все товары');
  const [minPrice, setMinPrice] = useState(0);
  const [maxPrice, setMaxPrice] = useState(100000);
  const [filters, setFilters] = useState([]);
  const {getParsedQuery, getStringQuery, pushQuery, NON_FILTERS_PARAMS} = useUrlQuery();
  const [rangeBounds, setRangeBounds] = useState([0, 100000]);
  const params = useParams();

  const onMinPriceHandleChange = (e) => {
    setMinPrice(e.target.value.replace(/\D/g, ''));
  }

  const onMaxPriceHandleChange = (e) => {
    setMaxPrice(e.target.value.replace(/\D/g, ''));
  }

  const onRangeChange = (value) => {
    const [minPrice, maxPrice] = value;
    setMinPrice(minPrice);
    setMaxPrice(maxPrice);
  }

  const getOnlyMatchingFilters = useCallback((filters, products) => {
    const matchingFilters = filters.filter(({type}) => products.some(p => Object.keys(p).includes(type)));
    const brandType = matchingFilters.find(({type}) => type === 'brand');
    const matchingBrands = brandType.names;
    const updatedBrands = [...matchingBrands];
    matchingBrands.forEach(b => {
      if (!products.some(({brand}) => brand === b)) {
        const removeIndex = updatedBrands.indexOf(b);
        updatedBrands.splice(removeIndex, 1);
      }
    })
    if (updatedBrands.length) {
      brandType.names = updatedBrands
    } else {
      matchingFilters.splice(matchingFilters.indexOf(brandType), 1)
    }
    matchingFilters.forEach(({names}) => names.sort((a, b) => a.localeCompare(b)));
    return matchingFilters
  }, [])

  const getFilters = useCallback(async () => {
    const {data} = await API.request('filters');
    const filterTypes = [...new Set(data.map(i => i.type))];
    let filters = filterTypes.map(type => ({
      type,
      names: data.filter(i => i.type === type).map(i => i.name)
    }));
    if (params.category) {
      const {data: {name}} = await API.request(`catalog/${params.category}`);
      setCategoryName(name);
      filters = getOnlyMatchingFilters(filters, products);
    } else {
      setCategoryName('Все товары');
    }
    setFilters(filters);
    setIsLoading(false);
  }, [products, params.category, getOnlyMatchingFilters]);

  const setInitialFilterQuery = () => {
    const parsed = getParsedQuery();
    Object.keys(parsed).forEach(type => {
      if (NON_FILTERS_PARAMS.slice(2).includes(type)) delete parsed[type]
    })
    setFilterQuery(getStringQuery(parsed));
  }

  const setInitialPriceFilterParams = () => {
    const productPrices = products.map(p => p.currentPrice);
    const lowerBound = products.length ? Math.min(...productPrices) : 0;
    const upperBound = products.length ? Math.max(...productPrices) : 100000;
    setRangeBounds([lowerBound, upperBound]);
    const {minPrice, maxPrice} = getParsedQuery();
    if (minPrice) {
      setMinPrice(minPrice.replace(/\D/g, ''));
    } else {
      setMinPrice(lowerBound)
    }
    if (maxPrice) {
      setMaxPrice(maxPrice.replace(/\D/g, ''));
    } else {
      setMaxPrice(upperBound)
    }
  }

  const setPriceFilterUrlParams = (e) => {
    e.preventDefault();
    pushQuery({
      minPrice,
      maxPrice
    });
  }

  useEffect(() => getFilters(), [getFilters]);
  useEffect(setInitialFilterQuery, [getParsedQuery, getStringQuery, setFilterQuery, NON_FILTERS_PARAMS]);
  useEffect(setInitialPriceFilterParams, [getParsedQuery, products]);

  if (isLoading) return <Loader width='300px' alignItems='flex-start' className='filter__loader'/>

  const filterItems = filters.map((item, index) => <FilterItem data={item} key={index}/>);

  return (
    <div className={`catalog-page__filter filter ${mobileFilterShow ? 'filter--show' : ''}`}>
      <h2 className='filter__title'>{categoryName}</h2>
      <form className='filter__price-block' onSubmit={setPriceFilterUrlParams}>
        <p>По цене</p>
        <input type='text'
               name='minPrice'
               value={minPrice}
               onChange={onMinPriceHandleChange}
               className='filter__price-field'/>
        <input type='text'
               name='maxPrice'
               value={maxPrice}
               onChange={onMaxPriceHandleChange}
               className='filter__price-field'/>
        <Button type='submit' className='filter__price-btn'>
          ОК
        </Button>
        <RangeSlider
          value={[+minPrice, +maxPrice]}
          onChange={onRangeChange}
          min={rangeBounds[0]}
          max={rangeBounds[1]}
          className='range-slider'
          thumbClassName='range-slider__handle'
          trackClassName='range-slider__track'
        />
      </form>
      {filterItems}
      {popupShow && <FilterPopup/>}
    </div>
  );
}

const mapStateToProps = (state) => {
  const popupShow = state.catalog.popup.show;
  const products = state.catalog.products;
  return {
    popupShow,
    products
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setFilterQuery: (filterQuery) => dispatch(setFilterQuery(filterQuery))
  }
}

Filter.propTypes = {
  mobileFilterShow: PropTypes.bool.isRequired,
}

export default connect(mapStateToProps, mapDispatchToProps)(memo(Filter));