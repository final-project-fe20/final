import React, {useState, memo} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {CHARS_MATCHES} from '../../../utils/CHARS_MATCHES';
import {setPopupShow} from '../../../store/catalog/actions';
import FilterName from '../FilterName/FilterName';
import DownArrow from './icons/DownArrow';
import './FilterItem.scss';

export const FilterItem = ({data: {type, names}, popupTop, setPopupShow}) => {
  const [listOpened, setListOpened] = useState(true);

  const listClickHandler = (e) => {
    const filterItemTop = e.target.closest('.filter__item').offsetTop;
    if (listOpened && filterItemTop < popupTop) setPopupShow(false);
    setListOpened(!listOpened);
  }

  const filterNames = names.map((name, index) => <FilterName key={index} type={type} name={name}/>);

  return (
    <div className={`filter__item ${listOpened ? 'filter__item--opened' : ''}`}>
      <h3 className='filter__type' onClick={listClickHandler}>
        {`${CHARS_MATCHES[type]}`}
        <DownArrow/>
      </h3>
      <ul>
        {filterNames}
      </ul>
    </div>
  );
}

const mapStateToProps = (state) => {
  const popupTop = state.catalog.popup.top;
  return {
    popupTop
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setPopupShow: (show) => dispatch(setPopupShow(show))
  }
}

FilterItem.propTypes = {
  data: PropTypes.shape({
    type: PropTypes.string.isRequired,
    names: PropTypes.arrayOf(PropTypes.string).isRequired
  }).isRequired
}

export default connect(mapStateToProps, mapDispatchToProps)(memo(FilterItem));