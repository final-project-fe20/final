import React from 'react';
import {render} from '@testing-library/react';
import {FilterItem} from './FilterItem';

jest.mock('../FilterName/FilterName', () => () => <div data-testid='filterItemTest'>test</div>);

describe('Filter: FilterItem test', () => {
  test('smoke test', () => {
    render(<FilterItem data={{
      type: 'testType',
      names: ['']
    }}/>);
  });

  test('renders proper qunatity of items', () => {
    const testData = {
      type: 'testType',
      names: ['1', '2', '3', '4']
    }
    const {getAllByTestId} = render(<FilterItem data={testData}/>);
    const filterNames = getAllByTestId('filterItemTest');
    expect(filterNames.length).toBe(testData.names.length);
  });
})