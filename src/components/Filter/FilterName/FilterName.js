import React, {useEffect, useState, memo} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {useUrlQuery} from '../../../hooks/useUrlQuery';
import {setFilterQuery, setPopupTop, setPopupShow} from '../../../store/catalog/actions';
import './FilterName.scss';

export const FilterName = ({type, name, filterQuery, setFilterQuery, setPopupTop, setPopupShow}) => {
  const [checked, setChecked] = useState(false);
  const {getParsedQuery, getStringQuery} = useUrlQuery();

  const setInitialChecked = () => {
    const parsed = getParsedQuery();
    if (parsed[type] && parsed[type].includes(name)) {
      setChecked(true);
    } else setChecked(false);
  }

  const onCheckedChange = ({target: {checked}}) => {
    setChecked(checked);
    setPopupShow(prevState => !prevState);
  }

  const setQueryOption = (parsed) => {
    const newQuery = {...parsed};
    if (!newQuery[type]) {
      newQuery[type] = [name];
    } else if (!newQuery[type].includes(name)) {
      newQuery[type].push(name);
    } else if (newQuery[type].length === 1) {
      delete newQuery[type];
    } else {
      const index = newQuery[type].indexOf(name);
      newQuery[type].splice(index, 1);
    }
    return newQuery;
  }

  const onFilterNameClick = (e) => {
    setPopupTop(e.target.offsetParent.offsetTop);
    setPopupShow(true);
    const parsedQuery = getParsedQuery(filterQuery);
    const modifiedQuery = setQueryOption(parsedQuery);
    const query = getStringQuery(modifiedQuery);
    setFilterQuery(query);
  }

  useEffect(setInitialChecked, [getParsedQuery, name, type]);

  return (
    <li className='filter__name'>
      <input type='checkbox' checked={checked} onChange={onCheckedChange} id={name}/>
      <label htmlFor={name} onClick={onFilterNameClick}>{name}</label>
    </li>
  );
}

const mapStateToProps = (state) => {
  const filterQuery = state.catalog.filterQuery;
  return {
    filterQuery
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setFilterQuery: (filterQuery) => dispatch(setFilterQuery(filterQuery)),
    setPopupTop: (top) => dispatch(setPopupTop(top)),
    setPopupShow: (show) => dispatch(setPopupShow(show))
  }
}

FilterName.propTypes = {
  type: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired
}

export default connect(mapStateToProps, mapDispatchToProps)(memo(FilterName));