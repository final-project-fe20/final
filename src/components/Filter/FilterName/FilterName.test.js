import React from 'react';
import {render} from '@testing-library/react';
import {FilterName} from './FilterName';

jest.mock('../../../hooks/useUrlQuery', () => ({
  useUrlQuery: () => ({
    getParsedQuery: () => ({}),
    getStringQuery: () => '',
    pushQuery: jest.fn(),
    NON_FILTERS_PARAMS: ['']
  })
}));

describe('Filter: FilterName test', () => {
  test('smoke test', () => {
    render(<FilterName type='' name=''/>);
  });

  test('onFilterNameClick called when filterNameLabel clicked', () => {
    const testName = 'testName';
    const setFilterQueryMock = jest.fn();
    const setPopupTopMock = jest.fn();
    const setPopupShowMock = jest.fn();
    const {getByText} = render(
      <FilterName
        type=''
        name={testName}
        setFilterQuery={setFilterQueryMock}
        setPopupTop={setPopupTopMock}
        setPopupShow={setPopupShowMock}
      />
    );
    const filterNameLabel = getByText(testName);
    expect(setFilterQueryMock).not.toHaveBeenCalled();
    expect(setPopupTopMock).not.toHaveBeenCalled();
    expect(setPopupShowMock).not.toHaveBeenCalled();
    Object.defineProperty(filterNameLabel, 'offsetParent', {
      get () {
        return this.parentNode;
      },
    });
    filterNameLabel.click();
    expect(setFilterQueryMock).toHaveBeenCalledTimes(1);
    expect(setPopupTopMock).toHaveBeenCalledTimes(1);
    expect(setPopupShowMock).toHaveBeenCalledWith(true);
  });
})