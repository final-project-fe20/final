import React, {memo} from 'react';
import {connect} from 'react-redux';
import {setPopupShow} from '../../../store/catalog/actions';
import {useUrlQuery} from '../../../hooks/useUrlQuery';
import {useProductFilter} from '../../../hooks/useProductFilter';
import Button from '../../Button/Button';
import './FilterPopup.scss';

export const FilterPopup = ({filterQuery, products, top, setPopupShow}) => {
  const {pushQuery, getParsedQuery} = useUrlQuery();
  const {filterProducts} = useProductFilter();

  const getFilteredLength = () => {
    const parsed = getParsedQuery(filterQuery);
    const filtersTypes = Object.keys(parsed);
    if (!filtersTypes.length) return products.length;
    return filterProducts(products, parsed).length;
  }

  const onShowBtnClickHandler = () => {
    setPopupShow(false);
    window.scrollTo(0, 0);
    pushQuery(getParsedQuery(filterQuery += '&page=1'), true);
  }

  return (
    <div className='filter__popup' style={{top}}>
      <span>{`Найдено: ${getFilteredLength()}`}</span>
      <Button onClick={onShowBtnClickHandler}>Показать</Button>
    </div>);
}

const mapStateToProps = (state) => {
  const products = state.catalog.products;
  const filterQuery = state.catalog.filterQuery;
  const top = state.catalog.popup.top;
  return {
    products,
    filterQuery,
    top
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setPopupShow: (show) => dispatch(setPopupShow(show))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(memo(FilterPopup));