import React from 'react';
import {render} from '@testing-library/react';
import {FilterPopup} from './FilterPopup';

jest.mock('../../../hooks/useUrlQuery', () => ({
  useUrlQuery: () => ({
    getParsedQuery: () => ({testQuery: 'testQuery'}),
    getStringQuery: () => '',
    pushQuery: jest.fn(),
    NON_FILTERS_PARAMS: ['']
  })
}));

jest.mock('../../../hooks/useProductFilter', () => ({
  useProductFilter: () => ({
    filterProducts: () => jest.fn()
  })
}));

describe('Filter: FilterPopup test', () => {
  test('smoke test', () => {
    render(<FilterPopup/>);
  });

  test('popup closing when showBtnclicked', () => {
    const setPopupShowMock = jest.fn();
    const {getByTestId} = render(<FilterPopup setPopupShow={setPopupShowMock}/>);
    const showBtn = getByTestId('testButton');
    expect(setPopupShowMock).not.toHaveBeenCalled();
    showBtn.click();
    expect(setPopupShowMock).toHaveBeenCalledTimes(1);
    expect(setPopupShowMock).toHaveBeenCalledWith(false);
  });
})