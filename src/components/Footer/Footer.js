import React, { useState } from 'react';
import './Footer.scss';
import { Link } from 'react-router-dom';
import SubscriptionForm from './SubscriptionForm/SubscriptionForm.js';
import FooterFacebookIcon from './icons/FooterFacebookIcon';
import FooterInstagramIcon from './icons/FooterInstagramIcon';
import {LogoIcon} from '../../theme/icons/LogoIcon';
import Button from '../Button/Button';
import ButtonArrow from './icons/ButtonArrow';

const Footer = () => {
  const [isExpanded, setIsExpanded] = useState({
    information: false,
    customers: false,
    contacts: false,
  });

  return (
    <footer className='page-footer footer'>
      <div className='container'>
        <div className='footer__inner'>
          <div className='footer__menus menus'>
            <div className='menus__intro'>
              <LogoIcon baseColor='#B7B7B7' className='menus__intro__icon' />
              <p className='menus__intro__item'>интернет магазин</p>
              <p className='menus__intro__item'>
                <a href='tel:0 800 21 21 50' className='menus__intro__link'>
                  0 800 21 21 50
                </a>
              </p>
              <p className='menus__intro__item'>
                <a
                  href='mailto:info@abcphoto.com.ua'
                  className='menus__intro__link'
                  data-testid='footer-link'
                >
                  info@abcshop.com.ua
                </a>
              </p>
            </div>
            <nav className='menus__navbar'>
              <div className='menus__navbar__item'>
                <p className='menus__navbar__item__title menus--block-title'>
                  информация
                </p>
                <Button
                  className='menus__navbar__item__exp-btn'
                  onClick={() =>
                    setIsExpanded({
                      ...isExpanded,
                      information: !isExpanded.information,
                    })
                  }
                >
                  <ButtonArrow
                    className={`menus__navbar__item__exp-btn-icon ${
                      isExpanded.information ? 'rotate-btn-icon' : ''
                    }`}
                  />
                </Button>
                <ul
                  className={`menus__navbar__item__list ${
                    isExpanded.information ? 'show-list' : ''
                  }`}
                >
                  <li className='menus__navbar__item__link-wrap'>
                    <Link to='/about-us' className='menus__navbar__item__link' data-testid='footer-link'>
                      о компании
                    </Link>
                  </li>
                  <li className='menus__navbar__item__link-wrap'>
                    <Link to='/sitemap' className='menus__navbar__item__link' data-testid='footer-link'>
                      карта сайта
                    </Link>
                  </li>
                  <li className='menus__navbar__item__link-wrap'>
                    <Link
                      to='/policy'
                      className='menus__navbar__item__link'
                      data-testid='footer-link'
                    >
                      публичная оферта
                    </Link>
                  </li>
                </ul>
              </div>
              <div className='menus__navbar__item'>
                <p className='menus__navbar__item__title menus--block-title'>
                  покупателям
                </p>
                <Button
                  className='menus__navbar__item__exp-btn'
                  onClick={() =>
                    setIsExpanded({
                      ...isExpanded,
                      customers: !isExpanded.customers,
                    })
                  }
                >
                  <ButtonArrow
                    className={`menus__navbar__item__exp-btn-icon ${
                      isExpanded.customers ? 'rotate-btn-icon' : ''
                    }`}
                  />
                </Button>
                <ul
                  className={`menus__navbar__item__list ${
                    isExpanded.customers ? 'show-list' : ''
                  }`}
                >
                  <li className='menus__navbar__item__link-wrap'>
                    <Link to='/credit' className='menus__navbar__item__link' data-testid='footer-link'>
                      кредит
                    </Link>
                  </li>
                  <li className='menus__navbar__item__link-wrap'>
                    <Link
                      to='/delivery-and-payment'
                      className='menus__navbar__item__link'
                      data-testid='footer-link'
                    >
                      доставка и оплата
                    </Link>
                  </li>
                  <li className='menus__navbar__item__link-wrap'>
                    <Link
                      to='/service-and-warranty'
                      className='menus__navbar__item__link'
                      data-testid='footer-link'
                    >
                      сервис и гарантия
                    </Link>
                  </li>
                </ul>
              </div>
            </nav>
            <div className='menus__subscription'>
              <p className='menus__subscription__item menus--block-title'>
                подписаться на рассылку
              </p>
              <div className='menus__subscription__item'>
                <SubscriptionForm />
              </div>
              <div className='menus__subscription__item'>
                <span>Мы в социальных сетях</span>
                <a
                  href='https://instagram.com'
                  className='menus__subscription__item__link'
                >
                  <FooterInstagramIcon className='menus__subscription__item__link-icon' />
                </a>
                <a
                  href='https://facebook.com'
                  className='menus__subscription__item__link'
                >
                  <FooterFacebookIcon className='menus__subscription__item__link-icon' />
                </a>
              </div>
            </div>
          </div>
          <div className='footer__copyright'>
            &copy; abcphoto 2021 - Все права защищены.
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
