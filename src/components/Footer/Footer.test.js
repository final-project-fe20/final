import React from 'react';
import { render } from '@testing-library/react';
import Footer from './Footer';

describe('Testing Footer.js', () => {
  test('Smoke test', () => {
    render(<Footer/>);
  });

  test('Footer links are present', () => {
    const { getAllByTestId } = render(<Footer/>);
    getAllByTestId('footer-link');
  });
})