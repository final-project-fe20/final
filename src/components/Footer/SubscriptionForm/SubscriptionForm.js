import { withFormik } from 'formik';
import React, { memo } from 'react';
import Button from '../../Button/Button';
import Input from '../../Input/Input';
import * as yup from 'yup';
import ButtonArrow from '../icons/ButtonArrow';
import PropTypes from 'prop-types';
import './SubscriptionForm.scss';
import API from '../../../utils/API';

const SubscriptionForm = ({ handleSubmit, isSubmitting, isValid }) => {
  return (
    <form className='subscription-form' onSubmit={handleSubmit} noValidate>
      <Input
        type='email'
        name='subscriptionFormEmail'
        placeholder='E-mail'
        className='subscription-form__input'
      />
      <Button
        type='submit'
        disabled={isSubmitting}
        className={`subscription-form__submit-btn ${!isValid ? 'subscription-form__submit-btn--invalid-btn' : ''}`}
      >
        <ButtonArrow className='subscription-form__submit-btn-icon' />
      </Button>
    </form>
  );
};

const subscribeEmail = async (values, { setSubmitting, setFieldError }) => {
  try {
    if (!values.subscriptionFormEmail) {
      setTimeout(() => {
        setFieldError('subscriptionFormEmail', '');
      }, 3000);
      setFieldError('subscriptionFormEmail', 'Введите email!');
      return;
    };

    await API.request('subscribers', 'POST', {
      email: values.subscriptionFormEmail,
      letterSubject: 'ABC-SHOP — Подписка на рассылку',
      letterHtml: '<p>Благодарим за Вашу подписку!</p>'
    })
  } catch (err) {
    if (err.response.status === 400) {
      setFieldError('subscriptionFormEmail', 'Вы уже подписаны!')
    } else {
      throw new Error(err);
    }
  } finally {
    setSubmitting(false);
  }
};

const SubscriptionFormSchema = yup.object().shape({
  subscriptionFormEmail: yup.string().email('Неверный формат email-адреса'),
});

SubscriptionForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  isSubmitting: PropTypes.bool.isRequired,
  isValid: PropTypes.bool.isRequired
}

export default withFormik({
  mapPropsToValues: () => ({ subscriptionFormEmail: '' }),
  handleSubmit: subscribeEmail,
  validationSchema: SubscriptionFormSchema,
})(memo(SubscriptionForm));
