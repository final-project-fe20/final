import React from 'react';
import { render } from '@testing-library/react';
import SubscriptionForm from './SubscriptionForm';

describe('Testing SubscriptionForm.js', () => {
  test('Smoke test', () => {
    render(<SubscriptionForm />);
  });
  
  test('SubscriptionForm Input is present', () => {
    const { getByPlaceholderText } = render(<SubscriptionForm />);
    getByPlaceholderText(/e-mail/i);
  });

  test('SubscriptionForm Submit Button is present', () => {
    const { getByRole } = render(<SubscriptionForm />);
    getByRole('button', { type: /submit/i });
  });
})
