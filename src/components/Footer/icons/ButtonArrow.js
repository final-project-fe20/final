import React from 'react';

const ButtonArrow = ({ className, width, height, svgFill, pathFill }) => {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      viewBox='0 0 7 13'
      fill={svgFill}
      xmlns='http://www.w3.org/2000/svg'
    >
      <path
        d='M0.243491 11.3503C-0.11535 11.7681 -0.0675479 12.3977 0.350261 12.7565C0.768069 13.1154 1.39767 13.0675 1.75651 12.6497L0.243491 11.3503ZM6 6.17836L6.75651 6.8281C7.0919 6.4376 7.07495 5.85599 6.71739 5.48568L6 6.17836ZM1.71739 0.307315C1.33483 -0.0888911 0.703521 -0.099954 0.307315 0.282605C-0.0888916 0.665165 -0.0999545 1.29648 0.282605 1.69269L1.71739 0.307315ZM1.75651 12.6497L6.75651 6.8281L5.24349 5.52862L0.243491 11.3503L1.75651 12.6497ZM6.71739 5.48568L1.71739 0.307315L0.282605 1.69269L5.28261 6.87105L6.71739 5.48568Z'
        fill={pathFill}
      />
    </svg>
  );
};

ButtonArrow.defaultProps = {
  width: '7',
  height: '13',
  svgFill: 'none',
  pathFill: 'white',
};

export default ButtonArrow;
