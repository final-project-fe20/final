import React from 'react';

const FooterFacebookIcon = ({ className, width, height, svgFill, pathFill }) => {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      viewBox='0 0 25 25'
      fill={svgFill}
      xmlns='http://www.w3.org/2000/svg'
    >
      <rect
        opacity='0.2'
        x='0.933594'
        y='0.542725'
        width='23.9335'
        height='23.9335'
        rx='3.98892'
        fill='white'
      />
      <path
        d='M16.721 13.5352L17.1771 10.5632H14.3253V8.6346C14.3253 7.82152 14.7237 7.02897 16.0009 7.02897H17.2973V4.49864C17.2973 4.49864 16.1209 4.29785 14.996 4.29785C12.6475 4.29785 11.1125 5.7213 11.1125 8.29814V10.5632H8.50195V13.5352H11.1125V20.7198H14.3253V13.5352H16.721Z'
        fill={pathFill}
      />
    </svg>
  );
};

FooterFacebookIcon.defaultProps = {
  width: '25',
  height: '25',
  svgFill: 'none',
  pathFill: 'white',
};

export default FooterFacebookIcon;
