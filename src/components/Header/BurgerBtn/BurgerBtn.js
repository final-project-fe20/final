import React, {memo} from 'react';
import PropTypes from 'prop-types';
import Button from '../../Button/Button';
import './BurgerBtn.scss';

const BurgerBtn = ({setMenuVisible}) => (
    <Button className='burger-btn' onClick={() => setMenuVisible(true)}>
        <span className='burger-btn__bar burger-btn__bar--top'/>
        <span className='burger-btn__bar burger-btn__bar--mid'/>
        <span className='burger-btn__bar burger-btn__bar--bot'/>
    </Button>
);

BurgerBtn.propTypes = {
  setMenuVisible: PropTypes.func.isRequired
}

export default memo(BurgerBtn);