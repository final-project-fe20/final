import {render} from '@testing-library/react';
import BurgerBtn from './BurgerBtn';

describe('Header: BurgerBtn test', () => {
  test('smoke test', () => {
    render(<BurgerBtn setMenuVisible={jest.fn()}/>);
  });

  test('setMenuVisible function is called when button clicked ', () => {
    const setMenuVisibleMock = jest.fn();
    const {getByTestId} = render(<BurgerBtn setMenuVisible={setMenuVisibleMock}/>);
    const openMenuBtn = getByTestId('testButton');
    expect(setMenuVisibleMock).not.toHaveBeenCalled();
    openMenuBtn.click();
    expect(setMenuVisibleMock).toHaveBeenCalledTimes(1);
  });
})