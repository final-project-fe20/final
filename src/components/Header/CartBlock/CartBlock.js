import React, {memo} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import Button from '../../Button/Button';
import {CartIcon} from '../icons/CartIcon';
import {CartIndicatorIcon} from '../icons/CartIndicatorIcon';
import './CartBlock.scss';

export const CartBlock = ({cartSize}) => {
  return (
    <Link to='/cart' className='cart-block'>
      <Button className='cart-block__btn'>
        <CartIcon/>
        <span>Корзина</span>
        {!!cartSize && (
          <div
            className='cart-block__indicator'>
            <CartIndicatorIcon/>
            <span>{cartSize}</span>
          </div>
        )}
      </Button>
    </Link>
  );
}

const mapStateToProps = (state) => {
  const cartSize = state.cart.length;
  return {
    cartSize
  }
}

export default connect(mapStateToProps)(memo(CartBlock));