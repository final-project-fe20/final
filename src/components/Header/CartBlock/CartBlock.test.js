import {render} from '@testing-library/react';
import {CartBlock} from './CartBlock';
import React from 'react';

describe('Header: CartBlock test', () => {
  test('smoke test', () => {
    render(<CartBlock cartSize={10}/>);
  });

  test('cart size is shown in CartBlock', () => {
    const testCartSize = 54321;
    const {getByText} = render(<CartBlock cartSize={testCartSize}/>);
    getByText(testCartSize);
  });

  test('cart indicator is not shown when cart is empty', () => {
    const testCartSize = 0;
    const {queryByText} = render(<CartBlock cartSize={testCartSize}/>);
    expect(queryByText(testCartSize)).not.toBeInTheDocument();
  });
})