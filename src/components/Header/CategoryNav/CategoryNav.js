import React, {memo} from 'react';
import PropTypes from 'prop-types';
import CategoryNavItem from '../CategoryNavItem/CategoryNavItem';
import Loader from '../../Loader/Loader';
import './CategoryNav.scss';

const CategoryNav = ({categories, isLoading}) => {
  if (isLoading) return <Loader className='category-nav__loader'/>

  const categoriesItems = categories.map(cat => <CategoryNavItem key={cat._id} cat={cat}/>);

  return (
    <div className='header__category'>
      <div className='container'>
        <nav className='category-nav'>
          <ul className='category-nav__list'>
            {categoriesItems}
          </ul>
        </nav>
      </div>
    </div>
  );
}

CategoryNav.propTypes = {
  categories: PropTypes.array.isRequired,
  isLoading: PropTypes.bool.isRequired
}

export default memo(CategoryNav);