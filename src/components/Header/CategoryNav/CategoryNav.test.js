import React from 'react';
import {render} from '@testing-library/react';
import CategoryNav from './CategoryNav';

jest.mock('../CategoryNavItem/CategoryNavItem', () => () => <div data-testid='cat-nav-item-test'>test</div>);

describe('Header: CategoryNav test', () => {
  test('smoke test', () => {
    render(<CategoryNav categories={[]} isLoading={false}/>);
  });

  test('Loader is present when loading', () => {
    const {getByTestId} = render(<CategoryNav categories={[]} isLoading={true}/>);
    getByTestId('testLoader');
  })

  test('categories renders fine', () => {
    const testCats = [
      {
        _id: 1,
        id: 'laptops',
        name: 'ноутбуки'
      }
    ];
    const {getByTestId} = render(<CategoryNav categories={testCats} isLoading={false}/>);
    getByTestId('cat-nav-item-test');
  });
})