import React, {memo} from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import SubCategoryNav from '../SubCategoryNav/SubCategoryNav';

const CategoryNavItem = ({cat: {id, name, subCats}}) => {
  return (
    <li className='category-nav__item'>
      <Link to={`/catalog/${id}`} className='category-nav__link'>{name}</Link>
      <SubCategoryNav subCats={subCats}/>
    </li>
  );
}

CategoryNavItem.propTypes = {
  cat: PropTypes.object.isRequired
}

export default memo(CategoryNavItem);