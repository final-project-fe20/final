import React from 'react'
import {render} from '@testing-library/react';
import CategoryNavItem from './CategoryNavItem';

jest.mock('../SubCategoryNav/SubCategoryNav', () => () => <div data-testid='subcat-nav-item-test'>test</div>);
jest.mock('react-router-dom', () => ({Link: () => <a href='/' data-testid='testLink'>testLink</a>}));

describe('Header: CategoryNavItem test', () => {
  test('smoke test', () => {
    render(<CategoryNavItem cat={{}}/>);
  });

  test('Link is present', () => {
    const {getByTestId} = render(<CategoryNavItem cat={{}}/>);
    getByTestId('testLink');
  });
})