import React, {useCallback, useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import API from '../../utils/API';
import BurgerBtn from './BurgerBtn/BurgerBtn';
import SearchBlock from './SearchBlock/SearchBlock';
import TelBlock from './TelBlock/TelBlock';
import LoginBlock from './LoginBlock/LoginBlock';
import CategoryNav from './CategoryNav/CategoryNav';
import MobileNav from './MobileNav/MobileNav';
import CartBlock from './CartBlock/CartBlock';
import {LogoIcon} from '../../theme/icons/LogoIcon';
import './Header.scss';

const Header = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [categories, setCategories] = useState([]);
  const [isMenuVisible, setMenuVisible] = useState(false);

  const getCategories = useCallback(async () => {
    const {data} = await API.request('catalog');
    const parentCategories = data.filter(cat => cat.parentId === 'null');
    const categories = parentCategories.map(({_id, id, name}) => {
      return {
        _id,
        id,
        name,
        subCats: data.filter(cat => cat.parentId === id)
      }
    });
    setCategories(categories);
    setIsLoading(false);
  }, []);

  const preventScroll = () => {
    if (isMenuVisible) {
      document.body.classList.add('overflow-hidden');
    } else {
      document.body.classList.remove('overflow-hidden')
    }
  }

  useEffect(getCategories, [getCategories]);
  useEffect(preventScroll, [isMenuVisible]);

  return (
    <header className='header'>
      <div className='container'>
        <div className='header__content'>
          <BurgerBtn setMenuVisible={setMenuVisible}/>
          <Link to='/' className='header__logo'>
            <LogoIcon/>
          </Link>
          <SearchBlock/>
          <TelBlock/>
          <LoginBlock/>
          <CartBlock/>
        </div>
      </div>
      <CategoryNav categories={categories} isLoading={isLoading}/>
      <MobileNav
        isMenuVisible={isMenuVisible}
        setMenuVisible={setMenuVisible}
        categories={categories}
        isLoading={isLoading}/>
    </header>
  )
}

export default Header;