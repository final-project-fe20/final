import React from 'react';
import {render} from '@testing-library/react';
import Header from './Header';

jest.mock('./LoginBlock/LoginBlock', () => () => <div>test</div>);
jest.mock('./MobileNav/MobileNav', () => () => <div>test</div>);
jest.mock('./CartBlock/CartBlock', () => () => <div>test</div>);

describe('Header test', () => {
  test('smoke test', () => {
    render(<Header/>);
  });

  test('Buttons are present', () => {
    const {getAllByTestId} = render(<Header/>);
    getAllByTestId('testButton');
  });
})