import React, {memo} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {useLogin} from '../../../hooks/useLogin';
import {openLoginModal} from '../../../store/loginModal/actions';
import Button from '../../Button/Button';
import {LoginIcon} from '../icons/LoginIcon';
import './LoginBlock.scss';

export const LoginBlock = ({openLoginModal, userName}) => {
  const {handleLogout} = useLogin();
  const isAuth = !!userName;

  return (
    <div className='login-block'>
      <Link
        to={`${isAuth ? '/cabinet/personal-info' : '#'}`}
        className={`login-block__btn ${isAuth ? 'login-block__btn--logined' : ''}`}
        onClick={() => {
          if (!isAuth) openLoginModal('LogIn');
        }}
      >
        <LoginIcon fill={`${isAuth ? '#51AD33' : '#C2C8D0'}`}/>
        <span>{userName || 'Вход'}</span>
      </Link>
      <ul className='login-block__expandable'>
        {!isAuth && (
          <>
            <li>
              <Button onClick={() => openLoginModal('LogIn')}>Войти</Button>
            </li>
            <li>
              <Button onClick={() => openLoginModal('SignUp')}>Зарегистрироваться</Button>
            </li>
          </>
        )}
        {isAuth && <li>
          <Button onClick={() => handleLogout()}>Выйти</Button>
        </li>}
      </ul>
    </div>
  )
}

const mapStateToProps = (state) => {
  const userName = state.customer.data.firstName;
  return {
    userName
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    openLoginModal: (isActive) => dispatch(openLoginModal(isActive))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(memo(LoginBlock));