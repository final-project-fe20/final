import {render} from '@testing-library/react';
import {LoginBlock} from './LoginBlock';

jest.mock('../../../hooks/useLogin', () => ({
  useLogin: () => ({
    handleLogin: jest.fn(),
    handleLogout: jest.fn()
  })
}));

describe('Header: LoginBlock test', () => {
  test('smoke test', () => {
    render(<LoginBlock openLoginModal={jest.fn()} userName=''/>);
  });

  test('loginModal opens while user is not auth', () => {
    const openLoginModalMock = jest.fn();
    const {getAllByTestId} = render(<LoginBlock openLoginModal={openLoginModalMock} userName=''/>);
    const loginBtn = getAllByTestId('testButton')[0];
    expect(openLoginModalMock).not.toHaveBeenCalled();
    loginBtn.click();
    expect(openLoginModalMock).toHaveBeenCalledTimes(1);
    expect(openLoginModalMock).toHaveBeenCalledWith('LogIn');
  });
})