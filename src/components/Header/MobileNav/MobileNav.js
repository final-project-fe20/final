import React, {useState, memo} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {openLoginModal} from '../../../store/loginModal/actions';
import Button from '../../Button/Button';
import Loader from '../../Loader/Loader';
import {LogoIcon} from '../../../theme/icons/LogoIcon';
import TelIcon from '../icons/TelIcon';
import './MobileNav.scss';

export const MobileNav = (
  {
    isMenuVisible,
    setMenuVisible,
    categories,
    isLoading,
    firstName,
    lastName,
    email,
    openLoginModal
  }
) => {
  const [isSubVisible, setSubMenuVisible] = useState(false);
  const isAuth = !!firstName;

  const ordersBtnClickHandler = () => {
    setMenuVisible(false);
    if (!isAuth) openLoginModal('LogIn');
  }

  const subnavMenuItems = categories.map(cat => (
    <li key={cat._id} className='category-list__item'>
      <Link to={`/catalog/${cat.id}`}
            className='category-list__link'
            onClick={() => setMenuVisible(false)}
      >
        {cat.name}
      </Link>
    </li>
  ))

  return (
    <>
      <div className={`mobile-nav ${isMenuVisible ? 'mobile-nav--show' : ''}`}>
        <div className='mobile-nav__header'>
          <Link to='/'
                className='header__logo'
                onClick={() => setMenuVisible(false)}
          >
            <LogoIcon/>
          </Link>
          <div className='mobile-nav__login login'>
            {isAuth && (
              <Link to='/cabinet/personal-info'
                    className='login__user-block'
                    onClick={() => setMenuVisible(false)}
              >
                <div className='login__user-circle'>
                  {firstName[0].toUpperCase()}
                </div>
                <p className='login__user-info'>
                  <span>{firstName} {lastName}</span>
                  <span>{email}</span>
                </p>
              </Link>
            )}
            {!isAuth && (
              <>
                <Button onClick={() => {
                  setMenuVisible(false);
                  openLoginModal('LogIn');
                }}>
                  Вход
                </Button>
                <Button onClick={() => {
                  setMenuVisible(false);
                  openLoginModal('SignUp');
                }}>
                  Регистрация
                </Button>
              </>)
            }
          </div>
        </div>
        <div className='mobile-nav__body'>
          <ul className='mobile-nav__list'>
            <li className='mobile-nav__item'>
              <Link to='/' className='mobile-nav__link' onClick={() => setMenuVisible(false)}>
                Главная
              </Link>
            </li>
            <li className='mobile-nav__item'>
              <Button
                className={`mobile-nav__submenu-btn ${isSubVisible ? 'mobile-nav__submenu-btn--expanded' : ''}`}
                onClick={() => setSubMenuVisible(!isSubVisible)}>
                Каталог
              </Button>
              <ul className={`mobile-nav__category-list category-list ${isSubVisible ? 'category-list--show' : ''}`}>
                {!isLoading ? subnavMenuItems : <Loader/>}
              </ul>
            </li>
            <li className='mobile-nav__item'>
              <Link to='/cabinet/orders' className='mobile-nav__link' onClick={ordersBtnClickHandler}>
                Мои заказы
              </Link>
            </li>
            <li className='mobile-nav__item'>
              <Link to='/cart' className='mobile-nav__link' onClick={() => setMenuVisible(false)}>
                Корзина
              </Link>
            </li>
          </ul>
        </div>
        <div className='mobile-nav__footer'>
          <div className='mobile-nav__tel-block'>
            <TelIcon/>
            <a className='mobile-nav__tel' href='tel:+0800212150'>0 (800) 21 21 50</a>
          </div>
        </div>
      </div>
      <div className='mobile-nav-overlay' onClick={() => setMenuVisible(false)}/>
    </>
  )
}

const mapStateToProps = (state) => {
  const firstName = state.customer.data.firstName;
  const lastName = state.customer.data.lastName;
  const email = state.customer.data.email;
  return {
    firstName,
    lastName,
    email
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    openLoginModal: (isActive) => dispatch(openLoginModal(isActive)),
  }
}

MobileNav.propTypes = {
  isMenuVisible: PropTypes.bool.isRequired,
  setMenuVisible: PropTypes.func.isRequired,
  categories: PropTypes.array.isRequired,
  isLoading: PropTypes.bool.isRequired
}

export default connect(mapStateToProps, mapDispatchToProps)(memo(MobileNav));