import {render} from '@testing-library/react';
import {MobileNav} from './MobileNav';

describe('Header: MobileNav test', () => {
  test('smoke test', () => {
    render(<MobileNav
      isMenuVisible={false}
      setMenuVisible={jest.fn()}
      categories={[]}
      isLoading={false}
      firstName=''
      lastName=''
      email=''
      openLoginModal={jest.fn()}/>);
  });

  test('userName is shown in mobileMenu', () => {
    const testName = 'testName';

    const {getByText} = render(<MobileNav
      isMenuVisible={true}
      setMenuVisible={jest.fn()}
      categories={[]}
      isLoading={false}
      firstName={testName}
      lastName=''
      email=''
      openLoginModal={jest.fn()}/>);

    getByText(testName);
  });

  test('MobileNav is closing when closeBtn clicked', () => {
    const setMenuVisibleMock = jest.fn();

    const {getAllByTestId} = render(<MobileNav
      isMenuVisible={true}
      setMenuVisible={setMenuVisibleMock}
      categories={[]}
      isLoading={false}
      firstName=''
      lastName=''
      email=''
      openLoginModal={jest.fn()}/>);

    const logoBtn = getAllByTestId('testButton')[0];
    expect(setMenuVisibleMock).not.toHaveBeenCalled();
    logoBtn.click();
    expect(setMenuVisibleMock).toHaveBeenCalledTimes(1);
    expect(setMenuVisibleMock).toHaveBeenCalledWith(false);
  });
})