import React, {useCallback, useEffect, useState, memo} from 'react';
import API from '../../../utils/API';
import SearchResultItem from '../SearchResultItem/SearchResultItem';
import Button from '../../Button/Button';
import {CrossIcon} from '../../../theme/icons/CrossIcon';
import {SearchIcon} from '../icons/SearchIcon';
import './SearchBlock.scss';

const SearchBlock = () => {
  const [suggestShow, setSuggestShow] = useState(false);
  const [searchQuery, setSearchQuery] = useState('');
  const [resultData, setResultData] = useState([]);

  const onSearchFieldChangeHandler = (e) => {
    setSearchQuery(e.target.value);
  }

  const onFormSubmit = useCallback((e) => e.preventDefault(), []);

  const getSearchResults = useCallback(async () => {
    if (searchQuery.trim() !== '') {
      API.cancel();
      try {
        const {data} = await API.request(
          'products/search',
          'POST',
          {
            query: searchQuery
          });
        setResultData(data.slice(0, 7));
      } catch {}
    } else {
      setResultData([]);
    }
  }, [searchQuery]);

  const clearSearch = useCallback(() => {
    setSearchQuery('');
    setResultData([]);
    setSuggestShow(false);
  }, []);

  useEffect(getSearchResults, [getSearchResults]);

  const searchResultItems = resultData.map(product => (
    <SearchResultItem
      key={product._id}
      product={product}
      clearSearch={clearSearch}
    />
  ));

  return (
    <form className='search-block' onSubmit={onFormSubmit}>
      <label className={`search-block__search-field search-field ${suggestShow ? 'search-field--active' : ''}`}
             onFocus={() => setSuggestShow(true)}
             onBlur={(e) => {
               if (!e.relatedTarget || e.relatedTarget.className !== 'result-list__link') setSuggestShow(false);
             }}
      >
        <input
          type='search'
          className='search-field__input'
          placeholder='Например: Ноутбук Asus'
          value={searchQuery}
          onChange={onSearchFieldChangeHandler}
        />
        {!!searchQuery.length && <Button className='search-field__clear-btn' onClick={() => clearSearch()} data-testid='clearBtn'>
          <CrossIcon/>
        </Button>}
        <Button className='search-field__icon'>
          <SearchIcon/>
        </Button>
      </label>
      {suggestShow && (
        <ul className='search-block__result-list result-list'>
          {searchResultItems}
          {!searchResultItems.length && !!searchQuery.length && (
            <li className='result-list__link'>
              По вашему запросу ничего не найдено
            </li>
          )}
        </ul>
      )}
    </form>
  );
}

export default memo(SearchBlock);