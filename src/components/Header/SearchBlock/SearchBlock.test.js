import React from 'react';
import {render} from '@testing-library/react';
import SearchBlock from './SearchBlock';
import userEvent from '@testing-library/user-event';

describe('Header: SearchBlock test', () => {
  test('smoke test', () => {
    render(<SearchBlock/>);
  });

  test('clearBtn appears when typing in searchbox', () => {
    const {getByRole, getByTestId} = render(<SearchBlock/>);
    const searchBox = getByRole('searchbox');
    const testQuery = 'testQuery';
    userEvent.type(searchBox, testQuery);
    getByTestId('clearBtn');
  });
})