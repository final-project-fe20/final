import React, {memo} from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';

const SearchResultItem = ({product, clearSearch}) => {
  const onClickHandler = (e) => {
    e.target.focus();
    clearSearch();
  }

  return (
    <li className='result-list__item'>
      <Link
        to={`/product/${product.itemNo}`}
        className='result-list__link'
        onClick={onClickHandler}
        onMouseDown={e => e.preventDefault()}
      >
        {product.name && (product.name.charAt(0).toUpperCase() + product.name.substr(1))}
      </Link>
    </li>
  );
}

SearchResultItem.propTypes = {
  product: PropTypes.object.isRequired,
  clearSearch: PropTypes.func.isRequired
}

export default memo(SearchResultItem);