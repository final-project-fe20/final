import React from 'react';
import {render} from '@testing-library/react';
import SearchResultItem from './SearchResultItem';

describe('SearchResultItem test', () => {
  test('smoke test', () => {
    render(<SearchResultItem product={{}} clearSearch={jest.fn()}/>);
  });

  test('Product name is present', () => {
    const testName = 'TestName';
    const testProduct = {
      name: testName
    }
    const {getByText} = render(<SearchResultItem product={testProduct} clearSearch={jest.fn()}/>);
    getByText(testName);
  });

  test('clearing search works when item clicked', () => {
    const clearSearchMock = jest.fn();
    const {getByTestId} = render(<SearchResultItem product={{}} clearSearch={clearSearchMock}/>);
    const itemLink = getByTestId('testLink');
    expect(clearSearchMock).not.toHaveBeenCalled();
    itemLink.click();
    expect(clearSearchMock).toHaveBeenCalledTimes(1);
    expect(clearSearchMock).toHaveBeenCalledWith();
  });
})