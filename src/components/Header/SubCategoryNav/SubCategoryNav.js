import React, {memo} from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import './SubCategoryNav.scss';

const SubCategoryNav = ({subCats}) => {
  const categoriesItems = subCats.map(catName => (
      <li key={catName._id} className='subcategory-nav__item'>
          <Link to={`/catalog/${catName.id}`} className='subcategory-nav__link'>{catName.name}</Link>
      </li>
  ));

  return (
        <nav className={'subcategory-nav'}>
            <ul className='subcategory-nav__list'>
                {categoriesItems}
            </ul>
        </nav>
  );
}

SubCategoryNav.propTypes = {
  subCats: PropTypes.array.isRequired
}

export default memo(SubCategoryNav);