import React from 'react';
import {render} from '@testing-library/react';
import SubCategoryNav from './SubCategoryNav';

describe('SubCategoryNav test', () => {
  test('smoke test', () => {
    render(<SubCategoryNav subCats={[]}/>);
  });

  test('category name is present', () => {
    const testSub = 'testSub';
    const testSubCats = [{_id: 1, name: testSub}];
    const {getByText} = render(<SubCategoryNav subCats={testSubCats}/>);
    getByText(testSub);
  });
})