import React, {useState, memo} from 'react';
import Button from '../../Button/Button';
import {TelArrow} from '../icons/TelArrow';
import './TelBlock.scss';

const TelBlock = () => {
  const [expanded, setExpanded] = useState(false);

  return (
        <div className={`tel-block ${expanded ? 'tel-block--expanded' : ''}`}>
            <div>
                <a className='tel-block__tel' href='tel:+0800212150'>0 (800) 21 21 50</a>
                <Button onClick={() => setExpanded(!expanded)}><TelArrow/></Button>
            </div>
            <div className='tel-block__expandable'>
                <div>
                    <a className='tel-block__tel tel-block__tel--sm' href='tel:+0443777011'>
                        0 (44) 377 70 11
                    </a>
                    <span>Киев</span>
                </div>
                <div>
                    <a className='tel-block__tel tel-block__tel--sm' href='tel:+0563703653'>
                        0 (56) 370 36 53
                    </a>
                    <span>Днепр</span>
                </div>
            </div>
        </div>
  );
}

export default memo(TelBlock);