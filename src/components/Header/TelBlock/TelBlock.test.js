import React from 'react';
import {render} from '@testing-library/react';
import TelBlock from './TelBlock';

describe('TelBlock test', () => {
  test('smoke test', () => {
    render(<TelBlock/>);
  });

  test('expandBtn is present', () => {
    const {getByTestId} = render(<TelBlock/>);
    getByTestId('testButton');
  });
})