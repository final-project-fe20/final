export const CartIcon = () => (
    <svg width='41' height='41' viewBox='0 0 41 41' fill='none' xmlns='http://www.w3.org/2000/svg'>
        <mask id='mask0' mask-type='alpha' maskUnits='userSpaceOnUse' x='0' y='0' width='41' height='41'>
            <path d='M41 20.5C41 31.8218 31.8218 41 20.5 41C9.17816 41 0 31.8218 0 20.5C0 9.17816 9.17816 0 20.5 0C31.8218 0 41 9.17816 41 20.5Z' fill='white'/>
        </mask>
        <g mask='url(#mask0)'>
            <path d='M41 20.5C41 31.8218 31.8218 41 20.5 41C9.17816 41 0 31.8218 0 20.5C0 9.17816 9.17816 0 20.5 0C31.8218 0 41 9.17816 41 20.5Z' fill='#C2C8D0'/>
        </g>
        <svg x='8' y='8' width='25' height='24' viewBox='0 0 25 24' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path d='M1 1C1.57973 1.00699 1.50047 1.021 2.29178 1.021C3.34389 1.021 4.45752 1.32038 4.92448 2.77214C5.29804 3.93354 7.31583 10.4734 8.27804 13.5981M5.68858 5.13642H24.027L21.9894 11.6071C17.6171 12.2569 8.85065 13.6117 8.19314 13.7225C6.96208 13.9299 6.45268 14.801 6.58003 15.5891C6.70738 16.3772 7.25923 17.0823 8.74499 17.0823C9.28835 17.0823 17.4201 17.0892 21.7925 17.0892' stroke='white' strokeWidth='1.45044' strokeLinecap='round' strokeLinejoin='round'/>
            <circle cx='10.313' cy='20.8761' r='1.62613' stroke='white' strokeWidth='1.21837'/>
            <circle cx='19.4536' cy='20.8761' r='1.62613' stroke='white' strokeWidth='1.21837'/>
        </svg>
    </svg>
)