export const SearchIcon = () => (
    <svg width='32' height='32' viewBox='0 0 32 32' fill='none' xmlns='http://www.w3.org/2000/svg'>
        <circle cx='16' cy='16' r='16' fill='#C2C8D0'/>
        <svg x='9' y='8' width='16' height='16' viewBox='0 0 16 16' fill='none'>
            <circle cx='6.83333' cy='6.83333' r='5.83333' stroke='white' strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
            <path d='M11.1113 11.1111L15.0002 15' stroke='white' strokeWidth='2' strokeLinecap='round' strokeLinejoin='round'/>
        </svg>
    </svg>
)
