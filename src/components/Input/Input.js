import { useField } from 'formik';
import React from 'react';
import PropTypes from 'prop-types';
import NumberFormat from 'react-number-format';

const Input = ({ name, type, className, ...rest }) => {
  const [field, { error, touched }, { setValue }] = useField(name);
  const { name: fieldName, value: fieldValue, onBlur: fieldOnBlur } = field;
  return (
    <>
      {type === 'num' || type === 'tel'
        ? (
        <NumberFormat
          className={`${className} ${error && touched ? `${className}--invalid-field` : ''}`}
          name={fieldName}
          type={type}
          value={fieldValue}
          onBlur={fieldOnBlur}
          onValueChange={(value) => setValue(value.value)}
          {...rest}
        />
          )
        : (
        <input
          className={`${className} ${error && touched ? `${className}--invalid-field` : ''}`}
          type={type}
          {...field}
          {...rest}
        />
          )}
      {error && touched && (
        <span className={`${className}__error-mg`}>{error}</span>
      )}
    </>
  );
};

Input.propTypes = {
  name: PropTypes.string.isRequired,
  className: PropTypes.string.isRequired,
};

export default Input;
