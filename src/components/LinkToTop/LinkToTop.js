import React, {useEffect, useState, useCallback, memo} from 'react';
import Button from '../Button/Button';
import {TopArrow} from './icons/TopArrow';
import './LinkToTop.scss';

const LinkToTop = () => {
  const [show, setShow] = useState(false);

  const handleScroll = useCallback(() => {
    if (window.pageYOffset >= document.documentElement.clientHeight) {
      setShow(true)
    } else {
      setShow(false);
    }
  }, [])

  const btnClickHandler = () => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth'
    });
  }

  useEffect(() => {
    window.addEventListener('scroll', handleScroll);
    return () => window.removeEventListener('scroll', handleScroll);
  }, [handleScroll])

  return (
    <Button
      onClick={btnClickHandler}
      className={`link-to-top ${show ? 'link-to-top--show' : ''}`}>
      <TopArrow/>
    </Button>
  );
}

export default memo(LinkToTop);