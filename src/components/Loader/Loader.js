import React, {memo} from 'react';
import PropTypes from 'prop-types';
import './Loader.scss';

const Loader = ({width, height, justifyContent, alignItems, className = ''}) => {
  return (
    <div className={`loader-wrapper ${className}`} style={{
      width,
      height,
      justifyContent,
      alignItems,
    }}
         data-testid='testLoader'
    >
      <div className='spinner-border'/>
    </div>
  )
}

Loader.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
  justifyContent: PropTypes.string,
  alignItems: PropTypes.string,
  className: PropTypes.string
}

export default memo(Loader);