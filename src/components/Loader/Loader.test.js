import React from 'react';
import {render} from '@testing-library/react';
import Loader from './Loader';

describe('Loader test', () => {
  test('smoke test', () => {
    render(<Loader/>);
  });

  test('loader-wrapper is present', () => {
    const {getByTestId} = render(<Loader/>);
    getByTestId('testLoader');
  });
})