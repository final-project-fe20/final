import { withFormik } from 'formik';
import React, { memo } from 'react';
import Button from '../../Button/Button';
import Input from '../../Input/Input';
import * as yup from 'yup';
import './ForgotPassword.scss';
import API from '../../../utils/API';
import PropTypes from 'prop-types';

const ForgotPassword = ({ handleSubmit, isSubmitting, openLoginModal, status }) => {
  if (status) {
    return (
    <div className='forgot-password-form__success-msg'>
        <p className='forgot-password-form__success-msg__text'>Проверьте почту для получения временного пароля</p>
        <Button className='forgot-password-form__success-msg__btn' onClick={() => openLoginModal('LogIn')}>ОК</Button>
    </div>)
  }
  return (
    <form className='forgot-password-form' onSubmit={handleSubmit} noValidate>
      <p className='forgot-password-form__title'>
          Восстановление пароля
      </p>
      <label className='forgot-password-form__label'>
        <span className='forgot-password-form__input-title'>e-mail</span>
        <Input
          type='email'
          name='forgotPasswordEmail'
          placeholder='Ivanov@gmail.com'
          className='forgot-password-form__input'
        />
      </label>
      <Button type='submit' disabled={isSubmitting} className='forgot-password-form__submit-btn'>Получить временный пароль</Button>
      <Button type='button' onClick={() => openLoginModal('LogIn')} className='forgot-password-form__go-back-btn'>Вернуться назад</Button>
    </form>
  );
};

const submitForgotPasswordForm = (
  values,
  {setSubmitting, setStatus, setFieldError}
) => {
  const { forgotPasswordEmail: email } = values;
  API.request('customers/forgotPassword', 'POST', { email })
    .then((res) => {
      setStatus(res.data.success);
    })
    .catch((err) => {
      if (err.response.status === 404) {
        setFieldError('forgotPasswordEmail', 'Пользователя c таким e-mail адресом не существует');
      } else {
        throw err;
      }
    })
    .finally(() => setSubmitting(false))
};

const ForgotPasswordSchema = yup.object().shape({
  forgotPasswordEmail: yup
    .string()
    .required('Поле обязательно для заполнения')
    .email('Неверный формат email-адреса'),
});

ForgotPassword.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  isSubmitting: PropTypes.bool.isRequired,
  openLoginModal: PropTypes.func.isRequired,
  status: PropTypes.bool.isRequired,
};

ForgotPassword.defaultProps = {
  status: false,
}

export default withFormik({
  mapPropsToValues: () => ({
    forgotPasswordEmail: '',
  }),
  handleSubmit: submitForgotPasswordForm,
  validationSchema: ForgotPasswordSchema,
})(memo(ForgotPassword));
