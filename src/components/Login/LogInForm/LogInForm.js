import React, { useState, memo } from 'react';
import {withFormik} from 'formik';
import Button from '../../Button/Button';
import Input from '../../Input/Input';
import ClosedEyeIcon from '../icons/ClosedEyeIcon';
import OpenedEyeIcon from '../icons/OpenedEyeIcon';
import './LogInForm.scss';
import LogInFormSchema from './LogInFormSchema';
import PropTypes from 'prop-types';
import API from '../../../utils/API';

const LogInForm = ({
  handleSubmit,
  isSubmitting,
  openLoginModal,
  values
}) => {
  const [isVisiblePassword, setIsVisiblePassword] = useState(false);
  return (
    <form className='log-in-form' onSubmit={handleSubmit} noValidate>
      <label className='log-in-form__label'>
        <span className='log-in-form__input-title'>e-mail</span>
        <Input
          type='email'
          name='logInEmail'
          placeholder='Ivanov@gmail.com'
          className='log-in-form__input'
        />
      </label>
      <label className='log-in-form__label'>
        <span className='log-in-form__input-title'>пароль</span>
        <Input
          type={!isVisiblePassword ? 'password' : 'text'}
          name='logInPassword'
          placeholder='******'
          className='log-in-form__input'
        />
        <Button
          type='button'
          onClick={() => setIsVisiblePassword(!isVisiblePassword)}
          className='log-in-form__show-hide-password'
        >
          {!isVisiblePassword ? <ClosedEyeIcon/> : <OpenedEyeIcon/>}
        </Button>
      </label>
      <div className='log-in-form__auth-restore-block auth-restore-block'>
        <label className='auth-restore-block__remember-me-label'>
          <Input
            type='checkbox'
            name='logInCheckbox'
            className='auth-restore-block__remember-me-checkbox'
            checked={values.logInCheckbox}
          />
          <span className='auth-restore-block__remember-me-checkbox-title'>
            Запомнить меня
          </span>
        </label>
        <Button
          onClick={() => openLoginModal('ForgotPassword')}
          type='button'
          className='auth-restore-block__forgot-password-btn'
        >
          Забыли пароль?
        </Button>
      </div>
      <Button
        type='submit'
        disabled={isSubmitting}
        className='log-in-form__submit-btn'
      >
        войти
      </Button>
    </form>
  );
};

const submitLogInForm = (
  values,
  {setSubmitting, errors, setErrors}
) => {
  const {logInEmail: loginOrEmail, logInPassword: password, closeLoginModal, handleLogin, logInCheckbox } = values;
  API.request('customers/login', 'POST', {
    loginOrEmail,
    password,
  })
    .then((res) => {
      closeLoginModal();
      handleLogin(res.data.token, logInCheckbox);
    })
    .catch((err) => {
      if (err.response.status === 404) {
        setErrors({
          ...errors,
          logInEmail: 'Пользователя c таким e-mail адресом не существует',
        });
      } else if (err.response.status === 400) {
        setErrors({
          ...errors,
          logInPassword: 'Неверный пароль',
        });
      } else {
        throw err;
      }
    })
    .finally(() => setSubmitting(false))
};

LogInForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  isSubmitting: PropTypes.bool.isRequired,
};

export default withFormik({
  mapPropsToValues: ({closeLoginModal, handleLogin}) => ({
    logInEmail: '',
    logInPassword: '',
    logInCheckbox: true,
    closeLoginModal,
    handleLogin
  }),
  handleSubmit: submitLogInForm,
  validationSchema: LogInFormSchema,
})(memo(LogInForm));