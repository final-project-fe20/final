import * as yup from 'yup';

const FIELD_REQUIRED = 'Поле обязательно для заполнения';

const LogInFormSchema = yup.object().shape({
  logInEmail: yup
    .string()
    .required(FIELD_REQUIRED)
    .email('Неверный формат email-адреса'),
  logInPassword: yup.string().required(FIELD_REQUIRED),
});

export default LogInFormSchema;
