import React, { memo } from 'react';
import Button from '../Button/Button';
import Modal from '../Modal/Modal';
import './Login.scss';
import LogInForm from './LogInForm/LogInForm';
import SignUpForm from './SignUpForm/SignUpForm';
import { connect } from 'react-redux';
import {
  closeLoginModal,
  openLoginModal,
} from '../../store/loginModal/actions';
import PropTypes from 'prop-types';
import { useLogin } from '../../hooks/useLogin';
import ForgotPassword from './ForgotPassword/ForgotPassword';
import GoogleLogin from 'react-google-login';
import API from '../../utils/API';

export const Login = ({ isActiveLoginModal, openLoginModal, closeLoginModal }) => {
  const { handleLogin } = useLogin();
  if (isActiveLoginModal) {
    return (
      <Modal
        className='login-modal'
        closeFunction={closeLoginModal}
        closeButton
      >
        <div className='login-modal__inner'>
          {isActiveLoginModal !== 'ForgotPassword'
            ? (
            <>
              <div className='login-modal__switch-btns-block'>
                <Button
                  className={`login-modal__btn ${
                    isActiveLoginModal === 'LogIn'
                      ? 'login-modal--active-btn'
                      : ''
                  }`}
                  onClick={() => openLoginModal('LogIn')}
                >
                  <span className='login-modal__btn-title-wrap'>войти</span>
                </Button>

                <Button
                  className={`login-modal__btn ${
                    isActiveLoginModal === 'SignUp'
                      ? 'login-modal--active-btn'
                      : ''
                  }`}
                  onClick={() => openLoginModal('SignUp')}
                >
                  <span className='login-modal__btn-title-wrap btn-pd'>
                    зарегистрироваться
                  </span>
                </Button>
              </div>
              {isActiveLoginModal === 'LogIn'
                ? (
                <LogInForm
                  openLoginModal={openLoginModal}
                  closeLoginModal={closeLoginModal}
                  handleLogin={handleLogin}
                />
                  )
                : (
                <SignUpForm
                  closeLoginModal={closeLoginModal}
                  handleLogin={handleLogin}
                />
                  )}
              <div className='login-modal__social-block'>
                <GoogleLogin
                  className='login-modal__social-block__btn'
                  clientId='368924533594-qp4o8ummtlbi98g850oounef4g00m018.apps.googleusercontent.com'
                  buttonText='Войти с Google'
                  onSuccess={async (user) => {
                    try {
                      const {
                        email,
                        familyName: lastName,
                        givenName: firstName,
                      } = user.profileObj;
                      const { data } = await API.request(
                        'customers/googleAuth',
                        'POST',
                        { email, firstName, lastName }
                      );
                      handleLogin(data.token);
                      closeLoginModal();
                    } catch (err) {
                      throw new Error(err);
                    }
                  }}
                  cookiePolicy={'single_host_origin'}
                />
              </div>
            </>
              )
            : (
            <ForgotPassword openLoginModal={openLoginModal} />
              )}
        </div>
      </Modal>
    );
  }
  return null;
};

Login.propTypes = {
  isActiveLoginModal: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
  openLoginModal: PropTypes.func.isRequired,
  closeLoginModal: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => {
  return {
    isActiveLoginModal: state.loginModal.isActiveLoginModal,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    openLoginModal: (isActive) => dispatch(openLoginModal(isActive)),
    closeLoginModal: () => dispatch(closeLoginModal()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(memo(Login));
