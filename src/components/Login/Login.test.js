import React from 'react';
import { render } from '@testing-library/react';
import { Login } from './Login';

jest.mock('../../hooks/useLogin', () => ({
  useLogin: () => ({
    handleLogin: jest.fn(),
  })
}));

describe('Testing Login', () => {
  test('Smoke test', () => {
    render(<Login isActiveLoginModal={'LogIn'} openLoginModal={jest.fn()} closeLoginModal={jest.fn()}/>);
  });
})