import React, { useState, memo } from 'react';
import { withFormik } from 'formik';
import Button from '../../Button/Button';
import Input from '../../Input/Input';
import SignUpFormSchema from './SignUpFormSchema';
import ClosedEyeIcon from '../icons/ClosedEyeIcon';
import OpenedEyeIcon from '../icons/OpenedEyeIcon';
import './SignUpForm.scss';
import PropTypes from 'prop-types';
import API from '../../../utils/API';

const SignUpForm = ({ handleSubmit, isSubmitting }) => {
  const [isVisiblePassword, setIsVisiblePassword] = useState({
    password: false,
    repeatPassword: false,
  });

  return (
    <form className='sign-up-form' onSubmit={handleSubmit} noValidate>
      <label className='sign-up-form__label'>
        <span className='sign-up-form__input-title'>ваше имя*</span>
        <Input
          type='text'
          name='signUpFirstName'
          placeholder='Иван'
          className='sign-up-form__input'
        />
      </label>
      <label className='sign-up-form__label'>
        <span className='sign-up-form__input-title'>e-mail*</span>
        <Input
          type='email'
          name='signUpEmail'
          placeholder='Ivanov@gmail.com'
          className='sign-up-form__input'
        />
      </label>
      <label className='sign-up-form__label'>
        <span className='sign-up-form__input-title'>номер телефона*</span>
        <Input
          type='tel'
          name='signUpTel'
          placeholder='+38 (050) 355 55 55'
          className='sign-up-form__input'
          format='+38 (###) ### ## ##'
          mask='_'
        />
      </label>
      <label className='sign-up-form__label'>
        <span className='sign-up-form__input-title'>ваш пароль*</span>
        <Input
          type={!isVisiblePassword.password ? 'password' : 'text'}
          name='signUpPassword'
          placeholder='******'
          className='sign-up-form__input'
        />
        <Button
          type='button'
          onClick={() =>
            setIsVisiblePassword({
              ...isVisiblePassword,
              password: !isVisiblePassword.password,
            })
          }
          className='sign-up-form__show-hide-password'
        >
          {!isVisiblePassword.password ? <ClosedEyeIcon /> : <OpenedEyeIcon />}
        </Button>
      </label>
      <label className='sign-up-form__label'>
        <span className='sign-up-form__input-title'>подтверждение пароля*</span>
        <Input
          type={!isVisiblePassword.repeatPassword ? 'password' : 'text'}
          name='signUpRepeatPassword'
          placeholder='******'
          className='sign-up-form__input'
        />
        <Button
          type='button'
          onClick={() =>
            setIsVisiblePassword({
              ...isVisiblePassword,
              repeatPassword: !isVisiblePassword.repeatPassword,
            })
          }
          className='sign-up-form__show-hide-password'
        >
          {!isVisiblePassword.repeatPassword
            ? (
            <ClosedEyeIcon />
              )
            : (
            <OpenedEyeIcon />
              )}
        </Button>
      </label>
      <Button
        type='submit'
        disabled={isSubmitting}
        className='sign-up-form__submit-btn'
      >
        зарегистрироваться
      </Button>
    </form>
  );
};

const submitSignUpForm = (values, { setSubmitting, errors, setErrors }) => {
  const {
    signUpFirstName,
    signUpEmail,
    signUpTel,
    signUpPassword,
    closeLoginModal,
    handleLogin,
  } = values;
  const newCustomer = {
    firstName: signUpFirstName,
    email: signUpEmail,
    password: signUpPassword,
    telephone: `+38${signUpTel}`,
  };
  API.request('customers', 'POST', newCustomer)
    .then((res) => {
      closeLoginModal();
      handleLogin(res.data.token);
    })
    .catch(err => {
      if (err.response.status === 400) {
        const { firstName, password, telephone, email } = err.response.data;
        setErrors({
          signUpFirstName: !!firstName && 'Допустимые символы для имени: a-z, A-Z, а-я, А-Я',
          signUpPassword: !!password && 'Неверный пароль',
          signUpTel: !!telephone && 'Некорректный номер телефона',
          signUpEmail: !!email && 'Данный e-mail уже зарегистрирован',
        })
      } else {
        throw err;
      }
    })
    .finally(() => setSubmitting(false));
};

SignUpForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  isSubmitting: PropTypes.bool.isRequired,
};

export default withFormik({
  mapPropsToValues: ({ closeLoginModal, handleLogin }) => ({
    signUpFirstName: '',
    signUpEmail: '',
    signUpTel: '',
    signUpPassword: '',
    signUpRepeatPassword: '',
    closeLoginModal,
    handleLogin,
  }),
  handleSubmit: submitSignUpForm,
  validationSchema: SignUpFormSchema,
})(memo(SignUpForm));
