import * as yup from 'yup';

const FIELD_REQUIRED = 'Поле обязательно для заполнения';

const SignUpFormSchema = yup.object().shape({
  signUpFirstName: yup
    .string()
    .required(FIELD_REQUIRED)
    .min(2, 'Имя должно содержать не менее 2 символов')
    .max(25, 'Имя должно содержать не более 25 символов')
    .matches(
      /^[a-zA-Zа-яА-Я]+$/,
      'Допустимые символы для имени: a-z, A-Z, а-я, А-Я.'
    ),
  signUpEmail: yup
    .string()
    .required(FIELD_REQUIRED)
    .email('Неверный формат email-адреса'),
  signUpTel: yup
    .string()
    .required(FIELD_REQUIRED)
    .min(10, 'Номер телефона должен содержать минимум 10 символов')
    .matches(
      /^0\d{3}\d{2}\d{2}\d{2}$/,
      'Неверный формат телефона'
    ),
  signUpPassword: yup
    .string()
    .required(FIELD_REQUIRED)
    .matches(
      /^[a-zA-Z0-9]+$/,
      'Пароль должен содержать только латинские символы и цифры'
    )
    .min(7, 'Пароль должен содержать минимум 7 символов')
    .max(30, 'Пароль должен содержать не более 30 символов'),
  signUpRepeatPassword: yup
    .string()
    .required(FIELD_REQUIRED)
    .oneOf([yup.ref('signUpPassword')], 'Пароли не совпадают'),
});

export default SignUpFormSchema;
