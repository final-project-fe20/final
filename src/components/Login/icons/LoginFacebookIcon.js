const LoginFacebookIcon = ({ className, width, height, svgFill }) => {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      viewBox='0 0 55 54'
      fill={svgFill}
      xmlns='http://www.w3.org/2000/svg'
    >
      <rect x='0.125' width='54' height='54' fill='#3B5998' />
      <path
        d='M27.508 10.125C37.0399 10.125 44.7661 17.8523 44.7661 27.3831C44.7661 36.915 37.0399 44.6411 27.508 44.6411C17.9761 44.6411 10.25 36.9149 10.25 27.3831C10.25 17.8523 17.9762 10.125 27.508 10.125Z'
        fill='white'
      />
      <path
        d='M30.1474 20.9443H32.75V16.875H29.6906V16.8897C25.9836 17.0286 25.2238 19.2343 25.1569 21.5508H25.1492V23.5828H22.625V27.5679H25.1492V38.25H28.9534V27.5679H32.0697L32.6717 23.5828H28.9547V22.3551C28.9547 21.5722 29.4469 20.9443 30.1474 20.9443Z'
        fill='#3B5998'
      />
    </svg>
  );
};

LoginFacebookIcon.defaultProps = {
  width: '55',
  height: '54',
  svgFill: 'none',
};

export default LoginFacebookIcon;
