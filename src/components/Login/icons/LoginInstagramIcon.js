const LoginInstagramIcon = ({ className, width, height, svgFill }) => {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      viewBox='0 0 54 54'
      fill={svgFill}
      xmlns='http://www.w3.org/2000/svg'
    >
      <g clipPath='url(#clip0)'>
        <g filter='url(#filter0_d)'>
          <path d='M54.0527 0H0.0527344V54H54.0527V0Z' fill='#E11B7E' />
          <path
            d='M54.0527 0H0.0527344V54H54.0527V0Z'
            fill='url(#paint0_linear)'
          />
          <path
            d='M54.0527 0H0.0527344V54H54.0527V0Z'
            fill='url(#paint1_radial)'
          />
          <path
            d='M54.0527 0H0.0527344V54H54.0527V0Z'
            fill='url(#paint2_radial)'
          />
        </g>
        <path
          d='M27.3889 18.4095C22.491 18.4095 18.5403 22.361 18.5403 27.26C18.5403 32.159 22.491 36.1105 27.3889 36.1105C32.2867 36.1105 36.2374 32.159 36.2374 27.26C36.2374 22.361 32.2867 18.4095 27.3889 18.4095ZM27.3889 33.014C24.2237 33.014 21.6362 30.4335 21.6362 27.26C21.6362 24.0865 24.216 21.506 27.3889 21.506C30.5617 21.506 33.1415 24.0865 33.1415 27.26C33.1415 30.4335 30.554 33.014 27.3889 33.014ZM38.6632 18.0475C38.6632 19.1952 37.7391 20.1118 36.5993 20.1118C35.4519 20.1118 34.5354 19.1875 34.5354 18.0475C34.5354 16.9075 35.4596 15.9831 36.5993 15.9831C37.7391 15.9831 38.6632 16.9075 38.6632 18.0475ZM44.5237 20.1426C44.3928 17.3773 43.7613 14.9279 41.7359 12.9097C39.7182 10.8916 37.2693 10.26 34.5046 10.1213C31.6552 9.95956 23.1148 9.95956 20.2654 10.1213C17.5084 10.2523 15.0595 10.8839 13.0341 12.902C11.0087 14.9202 10.3849 17.3696 10.2463 20.1349C10.0846 22.985 10.0846 31.5273 10.2463 34.3774C10.3772 37.1427 11.0087 39.5921 13.0341 41.6103C15.0595 43.6284 17.5007 44.26 20.2654 44.3987C23.1148 44.5604 31.6552 44.5604 34.5046 44.3987C37.2693 44.2677 39.7182 43.6361 41.7359 41.6103C43.7536 39.5921 44.3851 37.1427 44.5237 34.3774C44.6854 31.5273 44.6854 22.9927 44.5237 20.1426ZM40.8426 37.4354C40.2419 38.9451 39.0791 40.1082 37.562 40.7168C35.2901 41.618 29.8994 41.41 27.3889 41.41C24.8783 41.41 19.4799 41.6103 17.2158 40.7168C15.7063 40.1159 14.5435 38.9528 13.9351 37.4354C13.0341 35.1631 13.242 29.7711 13.242 27.26C13.242 24.7489 13.0418 19.3492 13.9351 17.0846C14.5358 15.5749 15.6986 14.4118 17.2158 13.8032C19.4876 12.902 24.8783 13.11 27.3889 13.11C29.8994 13.11 35.2978 12.9097 37.562 13.8032C39.0714 14.4041 40.2342 15.5672 40.8426 17.0846C41.7436 19.357 41.5357 24.7489 41.5357 27.26C41.5357 29.7711 41.7436 35.1708 40.8426 37.4354Z'
          fill='white'
        />
      </g>
      <defs>
        <filter
          id='filter0_d'
          x='-3.94727'
          y='0'
          width='62'
          height='62'
          filterUnits='userSpaceOnUse'
          colorInterpolationFilters='sRGB'
        >
          <feFlood floodOpacity='0' result='BackgroundImageFix' />
          <feColorMatrix
            in='SourceAlpha'
            type='matrix'
            values='0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0'
          />
          <feOffset dy='4' />
          <feGaussianBlur stdDeviation='2' />
          <feColorMatrix
            type='matrix'
            values='0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0'
          />
          <feBlend
            mode='normal'
            in2='BackgroundImageFix'
            result='effect1_dropShadow'
          />
          <feBlend
            mode='normal'
            in='SourceGraphic'
            in2='effect1_dropShadow'
            result='shape'
          />
        </filter>
        <linearGradient
          id='paint0_linear'
          x1='5.98535'
          y1='-6.25525e-07'
          x2='23.7305'
          y2='48.3838'
          gradientUnits='userSpaceOnUse'
        >
          <stop offset='0.04' stopColor='#4263DF' />
          <stop offset='1' stopColor='#D53585' stopOpacity='0' />
        </linearGradient>
        <radialGradient
          id='paint1_radial'
          cx='0'
          cy='0'
          r='1'
          gradientUnits='userSpaceOnUse'
          gradientTransform='translate(11.9443 54) rotate(-36.1187) scale(49.8752)'
        >
          <stop offset='0.13' stopColor='#FFAD05' />
          <stop offset='1' stopColor='#FF3F00' stopOpacity='0' />
        </radialGradient>
        <radialGradient
          id='paint2_radial'
          cx='0'
          cy='0'
          r='1'
          gradientUnits='userSpaceOnUse'
          gradientTransform='translate(15.4775 54) rotate(-40.6013) scale(33.79)'
        >
          <stop offset='0.01' stopColor='#FDDB86' />
          <stop offset='1' stopColor='#F06942' stopOpacity='0' />
        </radialGradient>
        <clipPath id='clip0'>
          <rect width='54' height='54' fill='white' />
        </clipPath>
      </defs>
    </svg>
  );
};

LoginInstagramIcon.defaultProps = {
  width: '54',
  height: '54',
  svgFill: 'none',
};

export default LoginInstagramIcon;
