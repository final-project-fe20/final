import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import './Modal.scss';
import Button from '../Button/Button';
import ModalCloseIcon from './icons/ModalCloseIcon';

const Modal = ({ children, className, closeFunction, closeButton }) => {
  const [isClicked, setIsClicked] = useState(false);
  const handleClick = () => {
    if (isClicked) {
      closeFunction();
      setIsClicked(false);
    }
  }

  useEffect(() => {
    document.body.classList.add('overflow-hidden');
    return () => document.body.classList.remove('overflow-hidden');
  }, []);

  return (
    <div className={`modal ${className}`} onMouseDown={() => setIsClicked(true)} onClick={handleClick}>
      <div className='modal__content' onMouseDown={e => e.stopPropagation()}>
        {closeButton && (
          <Button className='modal__close-btn' onClick={() => closeFunction()}>
            <ModalCloseIcon />
          </Button>
        )}
        {children}
      </div>
    </div>
  );
};

Modal.propTypes = {
  className: PropTypes.string.isRequired,
  closeFunction: PropTypes.func.isRequired,
  closeButton: PropTypes.bool,
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ])
};

export default Modal;
