import React from 'react';

const ModalCloseIcon = ({ className, width, height, svgFill, rectFill }) => {
  return (
    <svg
      className={className}
      width={width}
      height={height}
      viewBox='0 0 25 24'
      fill={svgFill}
      xmlns='http://www.w3.org/2000/svg'
    >
      <rect
        x='20.8693'
        y='0.000366211'
        width='4.42704'
        height='29.5136'
        rx='2.21352'
        transform='rotate(45 20.8693 0.000366211)'
        fill={rectFill}
      />
      <rect
        x='0.00219727'
        y='3.13037'
        width='4.42704'
        height='29.5136'
        rx='2.21352'
        transform='rotate(-45 0.00219727 3.13037)'
        fill={rectFill}
      />
    </svg>
  );
};

ModalCloseIcon.defaultProps = {
  width: '25',
  height: '24',
  svgFill: 'none',
  rectFill: 'white',
};

export default ModalCloseIcon;
