import React, {useEffect, useState, memo} from 'react';
import API from '../../utils/API';
import OrdersItem from './OrdersItem/OrdersItem';
import Loader from '../Loader/Loader';
import './Orders.scss';

const Orders = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [ordersData, setOrdersData] = useState([]);

  const getOrdersData = async () => {
    const {data} = await API.request('orders');
    const orders = [...data].reverse();
    setOrdersData(orders);
    setIsLoading(false);
  }

  useEffect(() => getOrdersData(), []);

  if (isLoading) return <Loader width='100%' height='300px'/>

  const ordersItems = ordersData.map(orderData => <OrdersItem key={orderData._id} data={orderData}/>)

  return (
    <section className='cabinet__orders orders'>
      <h2 className='cabinet__subtitle'>Мои заказы</h2>
      <div className='orders__header'>
        <span>№ Заказа</span>
        <span>К-во товаров</span>
        <span>Дата</span>
        <span>Сумма</span>
        <span>Статус</span>
      </div>
      <ul className='orders__list'>
        {ordersItems}
        {!ordersItems.length && <li className='orders__empty-msg'>Заказы отсутствуют</li>}
      </ul>
    </section>
  );
}

export default memo(Orders);