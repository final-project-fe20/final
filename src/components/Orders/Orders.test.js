import React from 'react';
import {render} from '@testing-library/react';
import Orders from './Orders';

jest.mock('./OrdersItem/OrdersItem', () => () => <div data-testid='ordersItemTest'>test</div>);

describe('Cabinet: Orders test', () => {
  test('smoke test', () => {
    render(<Orders/>);
  });

  test('loader is present while loading process', () => {
    const {getByTestId, queryByTestId} = render(<Orders/>);
    expect(queryByTestId('ordersItemTest')).not.toBeInTheDocument();
    getByTestId('testLoader');
  });
})