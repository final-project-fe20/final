import React, {memo} from 'react';
import PropTypes from 'prop-types';
import './OrdersItem.scss';

const OrdersItem = ({
  data: {
    products,
    orderNo,
    date,
    totalSum,
    status,
    canceled
  }
}) => {
  const getQuantityTotal = () => {
    return products.length ? products.map(({cartQuantity}) => cartQuantity).reduce((a, b) => a + b) : 0;
  }
  const orderDate = new Date(date);
  const quantityTotal = getQuantityTotal();
  return (
    <li className='orders__item'>
      <p>{orderNo}</p>
      <p>{quantityTotal}</p>
      <p>{`${orderDate.getDate()}.${('0' + (orderDate.getMonth() + 1)).slice(-2)}.${orderDate.getFullYear().toString().substr(-2)}`}</p>
      <p>{totalSum}<span>грн</span></p>
      {!canceled && <p className={`orders__status orders__status--${status === 'Выполнен' ? 'done' : 'accepted'}`}>{status}</p>}
      {canceled && <p className='orders__status orders__status--canceled'>Отменен</p>}
    </li>
  );
}

OrdersItem.propTypes = {
  data: PropTypes.object.isRequired
}

export default memo(OrdersItem);