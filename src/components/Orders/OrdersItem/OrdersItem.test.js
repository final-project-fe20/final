import React from 'react';
import {render} from '@testing-library/react';
import OrdersItem from './OrdersItem';

describe('Cabinet: OrdersItem test', () => {
  test('smoke test', () => {
    render(<OrdersItem data={
      {
        products: [{
          cartQuantity: 1
        }]
      }
    }/>);
  });

  test('received data shown properly', () => {
    const testData = {
      products: [{
        cartQuantity: 1
      }],
      orderNo: 'testOrderNo',
      totalSum: 100000
    }
    const {getByText} = render(<OrdersItem data={testData}/>);
    getByText(testData.orderNo);
    getByText(testData.totalSum);
  });
})