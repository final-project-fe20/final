import React, {useCallback, memo} from 'react';
import ReactPaginate from 'react-paginate';
import PropTypes from 'prop-types';
import './Pagination.scss';

const Pagination = ({page, changePage, pagesTotal}) => {
  const handlePageClick = useCallback((e) => {
    changePage(e.selected);
    window.scrollTo(0, 0);
  }, [changePage]);

  if (!pagesTotal) return null;

  return (
    <ReactPaginate
      previousLabel={''}
      nextLabel={''}
      breakLabel={'...'}
      containerClassName={'pagination'}
      previousLinkClassName={'pagination__arrow-btn pagination__arrow-btn--prev'}
      nextLinkClassName={'pagination__arrow-btn pagination__arrow-btn--next'}
      pageClassName={'pagination__btn'}
      activeClassName={'pagination__btn--active'}
      breakClassName={'pagination__btn'}
      forcePage={page}
      onPageChange={handlePageClick}
      pageCount={pagesTotal}
      marginPagesDisplayed={1}
      pageRangeDisplayed={3}
    />
  )
}

Pagination.propTypes = {
  page: PropTypes.number.isRequired,
  changePage: PropTypes.func.isRequired,
  pagesTotal: PropTypes.number.isRequired
}

export default memo(Pagination);