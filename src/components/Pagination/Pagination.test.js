import React from 'react';
import {render} from '@testing-library/react';
import Pagination from './Pagination';

describe('Pagination test', () => {
  test('smoke test', () => {
    render(<Pagination page={1} changePage={jest.fn()} pagesTotal={1}/>);
  });

  test('proper quantity of buttons is present', () => {
    const testPagesTotal = 3;
    const {getAllByRole} = render(<Pagination page={1} changePage={jest.fn()} pagesTotal={testPagesTotal}/>);
    const buttons = getAllByRole('button');
    expect(buttons.length).toBe(testPagesTotal + 2);
  });

  test('when no pages - renders nothing', () => {
    const {queryAllByRole} = render(<Pagination page={1} changePage={jest.fn()} pagesTotal={0}/>);
    const buttons = queryAllByRole('button');
    expect(buttons.length).toBe(0);
  });

  test('page changes when arrowBtn clicked', () => {
    const testPage = 5;
    const changePageMock = jest.fn();
    const {getByRole} = render(<Pagination page={testPage} changePage={changePageMock} pagesTotal={10}/>);
    const arrowBtn = getByRole('button', {name: 'Next page'});
    expect(changePageMock).not.toHaveBeenCalled();
    arrowBtn.click();
    expect(changePageMock).toHaveBeenCalledTimes(1);
    expect(changePageMock).toHaveBeenCalledWith(testPage + 1);
  });
})