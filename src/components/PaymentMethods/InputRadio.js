import React, {memo} from 'react';
import './InputRadio.scss'
import PropTypes from 'prop-types';

const InputRadio = ({text, id, setPaymentMethod, defaultChecked}) => {
  return (
    <div className='input-radio__wrapper'>
      <input type='radio'
             id={id}
             name='payment'
             className='input-radio__input'
             defaultChecked={defaultChecked}
      ></input>
      <label className='input-radio__label' htmlFor={id} onClick={e => setPaymentMethod(e.target.textContent)}>{text}</label>
    </div>
  )
}

InputRadio.propTypes = {
  text: PropTypes.string,
  id: PropTypes.string,
  setPaymentMethod: PropTypes.func
}

export default memo(InputRadio)