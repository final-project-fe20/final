import React, {memo} from 'react'
import './PaymentMethods.scss'
import InputRadio from './InputRadio'
import PropTypes from 'prop-types';

const PaymentMethods = ({setPaymentMethod, setPaymentMethodComment}) => {
  return (
    <div>
      <h2 className='payment-subtitle'>Способы оплаты</h2>
      <div className='payment__wrapper'>
        <InputRadio text='Наличными при получении' id='cash' setPaymentMethod={setPaymentMethod} defaultChecked />
        <InputRadio text='Кредит Альфа Банк ' id='alfa- bank' setPaymentMethod={setPaymentMethod} />
        <InputRadio text='Картой на сайте' id='upon-receipt' setPaymentMethod={setPaymentMethod} />
        <InputRadio text='Кредит Приват Банк ' id='privat-bank' setPaymentMethod={setPaymentMethod} />
        <InputRadio text='Безналичный расчет' id='cashless-payment' setPaymentMethod={setPaymentMethod} />
        <InputRadio text='Кредит УКРСИББАНК' id='ukrsibbank' setPaymentMethod={setPaymentMethod} />
      </div>
      <textarea onChange={e => setPaymentMethodComment(e.target.value)}
        className='payment-textinput'
        name='text'
        placeholder='Комментарий'>
      </textarea>
    </div>
  )
}

PaymentMethods.propTypes = {
  setPaymentMethod: PropTypes.func,
  setPaymentMethodComment: PropTypes.func,
}

export default memo(PaymentMethods)