import React from 'react';
import { render } from '@testing-library/react';
import PaymentMethods from '../PaymentMethods/PaymentMethods';

jest.mock('../PaymentMethods/InputRadio', () => () =>
  <div data-testid='testInput'>test</div>);

describe('PaymentMethods test', () => {
  test('smoke test', () => {
    render(<PaymentMethods
      setPaymentMethod={jest.fn()}
      setPaymentMethodComment={jest.fn()}/>);
  })

  test('Payment Methods children component renders fine', () => {
    const { getAllByTestId } = render(
      <PaymentMethods
        setPaymentMethod={jest.fn()}
        setPaymentMethodComment={jest.fn()}/>);
    getAllByTestId('testInput');
  })
})