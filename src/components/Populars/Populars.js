import React, {useCallback, useEffect, useState} from 'react';
import Card from '../Card/Card';
import Sliders from '../Slider/Slider';
import API from '../../utils/API';
import Loader from '../Loader/Loader';
import './Populars.scss';

const Populars = () => {
  const [popularsData, setPopularsData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  const getPopularsData = useCallback(async () => {
    const {data: {products}} = await API.request('products/filter?categories=gaming-laptops&perPage=12');
    setIsLoading(false);
    setPopularsData(products);
  }, []);

  useEffect(() => getPopularsData(), [getPopularsData]);

  if (isLoading) return <Loader height='600px'/>

  const cardItems = popularsData.map(i => <Card key={i._id} data={i}/>);

  return (
    <section className='populars'>
      <div className='container'>
        <h1 className='populars__title'>популярные модели</h1>
      </div>
      <div className='popular-slider--wrapper'>
      <Sliders
        quantity={3}
        dots={false}>
        {cardItems}
      </Sliders>
      </div>
    </section>
  )
}

export default Populars