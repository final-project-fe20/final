import React, {useCallback, useState} from 'react';
import API from '../../../../utils/API';
import PropTypes from 'prop-types';
import Modal from '../../../Modal/Modal';
import Button from '../../../Button/Button';
import './CommentModal.scss';

const CommentModal = (
  {
    _id: productId,
    closeFunction,
    editComment,
    updateCommentsData
  }) => {
  const [commentValue, setCommentValue] = useState(editComment.content);

  const onCommentChange = useCallback((e) => setCommentValue(e.target.value), []);

  const onSubmitHandler = useCallback((e) => {
    e.preventDefault();
    const newComment = {
      product: productId,
      content: commentValue
    }
    if (editComment.id) newComment.edited = true;
    API.request(
      `comments/${editComment.id || ''}`,
      `${editComment.id ? 'PUT' : 'POST'}`,
      newComment)
      .then(({data}) => {
        closeFunction();
        updateCommentsData(data, editComment.id);
      })
  }, [productId, commentValue, closeFunction, updateCommentsData, editComment]);

  return (
    <Modal
      className='comment-add'
      closeFunction={closeFunction}
      closeButton
    >
      <div className='comment-add__inner'>
        <header className='comment-add__header'>{editComment.id ? 'Редактировать отзыв' : 'Написать отзыв'}</header>
        <form className='comment-add__form' onSubmit={onSubmitHandler}>
          <textarea
            required
            rows='8'
            className='comment-add__textarea'
            placeholder='Ваш Комментарий'
            value={commentValue}
            onChange={onCommentChange}
            data-testid='testTextArea'
          />
          <Button
            type='submit'
            className='comments__btn'
          >
            {editComment.id ? 'Сохранить' : 'Оставить отзыв'}
          </Button>
        </form>
      </div>
    </Modal>
  );
}

CommentModal.propTypes = {
  _id: PropTypes.string.isRequired,
  closeFunction: PropTypes.func.isRequired,
  editComment: PropTypes.object.isRequired,
  updateCommentsData: PropTypes.func.isRequired
}

export default CommentModal;