import React from 'react';
import {render, waitFor} from '@testing-library/react';
import CommentModal from './CommentModal';
import userEvent from '@testing-library/user-event';

jest.mock('../../../../utils/API', () => ({
  request: async () => Promise.resolve({data: []})
}));

describe('Comments: AddCommentModal test', () => {
  test('smoke test', () => {
    render(<CommentModal _id='1' closeFunction={jest.fn()} updateCommentsData={jest.fn()} editComment={{
      id: '',
      content: ''
    }}/>);
  });

  test('commentArea filled properly when typing on textArea', () => {
    const {getByTestId, getByDisplayValue} = render(
      <CommentModal
        _id='1'
        closeFunction={jest.fn()}
        updateCommentsData={jest.fn()}
        editComment={{
          id: '',
          content: ''
        }}/>
    );
    const testComment = 'testComment';
    const commentArea = getByTestId('testTextArea');
    userEvent.type(commentArea, testComment);
    getByDisplayValue(testComment);
  });

  test('closeFunction and addCommentsData calles when submitBtn clicked', async () => {
    const closeFunctionMock = jest.fn();
    const updateCommentsDataMock = jest.fn();
    const {getByTestId, getAllByTestId} = render(
      <CommentModal
        _id='1'
        closeFunction={closeFunctionMock}
        updateCommentsData={updateCommentsDataMock}
        editComment={{
          id: '',
          content: ''
        }}/>
    );
    const testComment = 'testComment';
    const commentArea = getByTestId('testTextArea');
    userEvent.type(commentArea, testComment);
    const buttons = getAllByTestId('testButton');
    const submitBtn = buttons[buttons.length - 1];
    expect(closeFunctionMock).not.toHaveBeenCalled();
    expect(updateCommentsDataMock).not.toHaveBeenCalled();
    submitBtn.click();
    await waitFor(() => {
      expect(closeFunctionMock).toHaveBeenCalledTimes(1);
      expect(updateCommentsDataMock).toHaveBeenCalledTimes(1);
      expect(closeFunctionMock).toHaveBeenCalledWith();
    });
  });
})