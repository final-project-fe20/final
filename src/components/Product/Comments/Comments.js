import React, {useCallback, useEffect, useState, memo} from 'react';
import {connect} from 'react-redux';
import API from '../../../utils/API';
import PropTypes from 'prop-types';
import Button from '../../Button/Button';
import {openLoginModal} from '../../../store/loginModal/actions';
import CommentsItem from './CommentsItem/CommentsItem';
import CommentModal from './CommentModal/CommentModal';
import Loader from '../../Loader/Loader';
import './Comments.scss';

export const Comments = ({_id, isAuth, openLoginModal, customerId}) => {
  const [isLoading, setIsLoading] = useState(true);
  const [commentsData, setCommentsData] = useState([]);
  const [modalShow, setModalShow] = useState(false);
  const [editComment, setEditComment] = useState({
    id: '',
    content: ''
  });

  const getComments = useCallback(async () => {
    const {data} = await API.request(`comments/product/${_id}`);
    setCommentsData(data);
    setIsLoading(false);
  }, [_id]);

  const updateCommentsData = useCallback((newComment, commentId) => {
    const updatedData = [...commentsData];
    if (commentId) {
      const index = updatedData.findIndex(({_id}) => _id === commentId);
      if (index !== -1) updatedData.splice(index, 1, newComment);
    } else {
      updatedData.push(newComment);
    }
    setCommentsData(updatedData);
  }, [commentsData]);

  const modalCloseFunc = useCallback(() => setModalShow(false), []);

  const onCommentsBtnClickHandler = useCallback(() => {
    if (isAuth) {
      setEditComment({
        id: '',
        content: ''
      });
      setModalShow(true);
    } else {
      openLoginModal('LogIn')
    }
  }, [isAuth, openLoginModal]);

  useEffect(getComments, [getComments]);

  if (isLoading) return <Loader height='400px'/>

  const commentsItems = commentsData.map(i => (
    <CommentsItem
      key={i._id}
      data={i}
      customerId={customerId}
      setModalShow={setModalShow}
      setEditComment={setEditComment}
    />
  ));

  return (
    <>
      <div className='container'>
        <section className='comments'>
          <Button
            className='comments__btn'
            onClick={onCommentsBtnClickHandler}
          >
            Оставить отзыв
          </Button>
          <ul className='comments__list'>
            {commentsItems}
            {!commentsItems.length && <li className='comments__empty-msg'>Отзывов о данном товаре пока нет</li>}
          </ul>
        </section>
      </div>
      {modalShow && (
        <CommentModal
          _id={_id}
          closeFunction={modalCloseFunc}
          updateCommentsData={updateCommentsData}
          editComment={editComment}
        />
      )}
    </>
  );
}

const mapStateToProps = (state) => {
  const isAuth = !!state.customer.data.firstName;
  const customerId = state.customer.data.customerId;
  return {
    isAuth,
    customerId
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    openLoginModal: (isActive) => dispatch(openLoginModal(isActive))
  }
}

Comments.propTypes = {
  _id: PropTypes.string.isRequired
}

export default connect(mapStateToProps, mapDispatchToProps)(memo(Comments));