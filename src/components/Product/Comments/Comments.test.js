import React from 'react';
import {render, waitForElementToBeRemoved} from '@testing-library/react';
import {Comments} from './Comments';

jest.mock('./CommentModal/CommentModal', () => () => <div data-testid='CommentModalTest'>test</div>);
jest.mock('../../../utils/API', () => ({
  request: async () => Promise.resolve({data: []})
}));

describe('Comments test', () => {
  test('smoke test', async () => {
    const {getByTestId} = render(<Comments _id='1'/>);
    await waitForElementToBeRemoved(getByTestId('testLoader'));
  });

  test('test', async () => {
    const openLoginModalMock = jest.fn();
    const {queryByTestId, getByTestId} = render(<Comments _id='1' isAuth={false} openLoginModal={openLoginModalMock}/>);
    await waitForElementToBeRemoved(queryByTestId('testLoader'));
    const submitBtn = getByTestId('testButton');
    expect(openLoginModalMock).not.toHaveBeenCalled();
    submitBtn.click();
    expect(openLoginModalMock).toHaveBeenCalledTimes(1);
    expect(openLoginModalMock).toHaveBeenCalledWith('LogIn');
  });
})