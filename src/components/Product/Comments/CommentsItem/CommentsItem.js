import React from 'react';
import PropTypes from 'prop-types';
import Button from '../../../Button/Button';
import {EditIcon} from '../../icons/EditIcon';
import './CommentsItem.scss';

const CommentsItem = ({
  data: {
    _id: commentId,
    content,
    customer,
    date,
    edited = false
  },
  customerId: currentCustomerId,
  setModalShow,
  setEditComment
}) => {
  const onEditBtnClickHandler = () => {
    setEditComment({
      id: commentId,
      content
    });
    setModalShow(true)
  }

  return (
    <li className='comments__item item'>
      <div className='item__header'>
        <p className='item__author'>
          {customer.firstName} {customer.lastName} {edited && <span className='item__edited-msg'>(изменено)</span>}
        </p>
        <time dateTime={date} className='item__date'>{new Date(date).toLocaleString()}</time>
      </div>
      <div className='item__body'>
        {content}
        {customer._id === currentCustomerId && (
          <Button className='comments__edit-btn' onClick={onEditBtnClickHandler}>
            <EditIcon/>
          </Button>
        )}
      </div>
    </li>
  );
}

CommentsItem.propTypes = {
  data: PropTypes.shape({
    _id: PropTypes.string.isRequired,
    content: PropTypes.string.isRequired,
    customer: PropTypes.object.isRequired,
    date: PropTypes.string.isRequired,
    edited: PropTypes.bool
  }).isRequired,
  setModalShow: PropTypes.func.isRequired,
  setEditComment: PropTypes.func.isRequired
}

export default CommentsItem;