import React from 'react';
import {render} from '@testing-library/react';
import CommentsItem from './CommentsItem';

describe('Comments: CommentsItem test', () => {
  test('smoke test', () => {
    const testData = {
      _id: '',
      content: '',
      customer: {},
      date: ''
    }
    render(<CommentsItem data={testData} setEditComment={jest.fn()} setModalShow={jest.fn()}/>);
  });

  test('comment renders properly', () => {
    const testData = {
      _id: '',
      content: 'testContent',
      customer: {},
      date: ''
    }
    const {getByText} = render(<CommentsItem data={testData} setEditComment={jest.fn()} setModalShow={jest.fn()}/>);
    getByText(testData.content);
  });
})