import React, {memo, useCallback} from 'react';
import PropTypes from 'prop-types'
import Modal from '../../Modal/Modal';
import Sliders from '../../Slider/Slider';
import './PhotoGallery.scss';

const PhotoGallery = ({imageUrls, name, photoGalleryOptions, setPhotoGalleryOptions}) => {
  const closeFunction = useCallback(() => {
    setPhotoGalleryOptions(prevState => ({...prevState, show: false}));
  }, [setPhotoGalleryOptions]);

  const galleryItems = imageUrls.map((imgUrl, i) => (
    <div key={i} className='slider__wrapper-item'>
      <img src={imgUrl}
           alt={`Купить ${name}`}
           className='slider__wrapper-photo'>
      </img>
    </div>
  ))

  return (
    <Modal
      className='photo-gallery'
      closeFunction={closeFunction}
      closeButton
    >
      <div className='photo-gallery__inner'>
        <header className='photo-gallery__header'>
          <p>
            <span>Фотографии </span>
            {name}
          </p>
        </header>
        <div className='photo-gallery__content'>
          <Sliders
            dots={true}
            quantity={1}
            autoplay={false}
            initialSlide={photoGalleryOptions.startItem}
            accessibility={true}
          >
            {galleryItems}
          </Sliders>
        </div>
      </div>
    </Modal>
  );
}

PhotoGallery.propTypes = {
  imageUrls: PropTypes.arrayOf(PropTypes.string).isRequired,
  name: PropTypes.string.isRequired,
  photoGalleryOptions: PropTypes.object.isRequired,
  setPhotoGalleryOptions: PropTypes.func.isRequired
}

export default memo(PhotoGallery);