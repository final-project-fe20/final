import React from 'react';
import {render} from '@testing-library/react';
import PhotoGallery from './PhotoGallery';

jest.mock('react-slick', () => () => <div data-testid='slickTest'>test</div>);

describe('PhotoGallery test', () => {
  test('smoke test', () => {
    render(<PhotoGallery imageUrls={['']} name='' photoGalleryOptions={{}} setPhotoGalleryOptions={jest.fn()}/>);
  });

  test('sent productName is present', () => {
    const testName = 'testName';
    const {getByText} = render(<PhotoGallery imageUrls={['']} name={testName} photoGalleryOptions={{}} setPhotoGalleryOptions={jest.fn()}/>);
    getByText(testName);
  });

  test('slider is present', async () => {
    const {getByTestId} = render(<PhotoGallery imageUrls={['']} name='' photoGalleryOptions={{}} setPhotoGalleryOptions={jest.fn()}/>);
    getByTestId('slickTest');
  });
})