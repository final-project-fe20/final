import React, { memo, useCallback, useEffect, useMemo, useState } from 'react'
import './Product.scss';
import ProductBuy from './ProductBuy/ProductBuy';
import ProductOptions from './ProductOptions/ProductOptions';
import ProductName from './ProductName/ProductName';
import ProductPhotoBlock from './ProductPhotoBlock/ProductPhotoBlock';
import API from '../../utils/API'
import Loader from '../Loader/Loader'
import BreadCrumbs from '../BreadCrumbs/BreadCrumbs'
import { useParams, useHistory } from 'react-router-dom'
import SimilarModels from '../SimilarModels/SimilarModels';
import PhotoGallery from './PhotoGallery/PhotoGallery';

const Product = () => {
  const [productData, setProductData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [photoGalleryOptions, setPhotoGalleryOptions] = useState({
    show: false,
    startItem: 0
  });
  const params = useParams();
  const history = useHistory();

  const getProductData = useCallback(async () => {
    const {data} = await API.request(`products/filter?itemNo=${params.itemNo}`);
    if (!data.products[0]) {
      history.push('/');
      return;
    }
    setProductData(data.products[0]);
    setIsLoading(false);
  }, [params.itemNo, history]);

  useEffect(() => {
    getProductData()
  }, [getProductData]);

  const imageUrls = useMemo(() => {
    if (!isLoading) return productData.imageUrls.slice(1)
  }, [productData.imageUrls, isLoading])

  if (isLoading) return <Loader height='800px'/>

  return (
    <>
      <section className='product container'>
        <div className='product__bread-crumbs'>
          <BreadCrumbs/>
        </div>
        <div className='product__main-block'>
          <ProductName name={productData.name} itemNo={productData.itemNo}/>
          <div className='product__main-block_buy-block'>
            <ProductPhotoBlock
              imageUrls={imageUrls}
              name={productData.name}
              id={productData._id}
              setPhotoGalleryOptions={setPhotoGalleryOptions}
            />
            <ProductBuy options={productData}/>
          </div>
        </div>
        <div className='product__descriptions'>
          <ProductOptions options={productData}/>
        </div>
      </section>
      {photoGalleryOptions.show && (
        <PhotoGallery
          imageUrls={imageUrls}
          name={productData.name}
          photoGalleryOptions={photoGalleryOptions}
          setPhotoGalleryOptions={setPhotoGalleryOptions}/>
      )}
      <SimilarModels category={productData.categories}/>
    </>
  );
}

export default memo(Product);