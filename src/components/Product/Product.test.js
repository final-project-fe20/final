import React from 'react';
import {render} from '@testing-library/react';
import Product from './Product';

jest.mock('../BreadCrumbs/BreadCrumbs', () => () => <div data-testid='breadCrumbsTest'>test</div>);
jest.mock('./ProductName/ProductName', () => () => <div data-testid='productNameTest'>test</div>);
jest.mock('./ProductPhotoBlock/ProductPhotoBlock', () => () => <div data-testid='productPhotoBlockTest'>test</div>);
jest.mock('./ProductBuy/ProductBuy', () => () => <div data-testid='productBuyTest'>test</div>);
jest.mock('./ProductOptions/ProductOptions', () => () => <div data-testid='productOptionsTest'>test</div>);
jest.mock('./PhotoGallery/PhotoGallery', () => () => <div data-testid='photoGalleryTest'>test</div>);
jest.mock('../SimilarModels/SimilarModels', () => () => <div data-testid='similarModelsTest'>test</div>);

describe('Product test', () => {
  test('smoke test', () => {
    render(<Product/>);
  });
})