import React, { memo } from 'react'
import './ProductBuy.scss';
import {Link} from 'react-router-dom';
import Button from '../../Button/Button'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { toggleCartProduct } from '../../../store/cart/actions'
import CartIcon from './icons/CartIcon'

const IN_STOCK_MSG = 'в наличии';
const EXPECTED_MSG = 'ожидается';

const ProductBuy = ({
  options: {
    quantity,
    currentPrice,
    _id
  },
  carted,
  toggleCartProduct
}) => {
  return (
    <div className='product__buy'>
      <div className='product__price'>
        <p className={`product__price-availability product__price-availability--${quantity ? 'in-stock' : 'expected'}`}>
          {`${quantity ? IN_STOCK_MSG : EXPECTED_MSG}`}
        </p>
        <span className='product__buy_price'>
          {currentPrice}
          <span className='product__buy_price-currency'> грн</span>
        </span>
      </div>
      <Button
        onClick={() => toggleCartProduct(_id)}
        className={`product__buy_button ${carted ? 'product__buy_button--center' : ''}`}
        data-testid='buyBtn'>
        <span className='product__buy_button-title'>{carted ? 'В корзине' : 'Купить'}</span>
        <span className={carted ? 'product__buy_button-icon--hidden' : ''} >
          <CartIcon/>
        </span>
      </Button>
      <div className='product__buy_delivery'>
        <h3 className='product__buy_delivery-title'>ДОСТАВКА</h3>
        <ul className='product__buy_delivery-list'>
          <li className='product__buy_delivery-item'>• Доставка по всей Украине</li>
          <li className='product__buy_delivery-item'>• Оплата товара при получении</li>
          <li className='product__buy_delivery-item'>• Возможен самовывоз</li>
        </ul>
        <h3 className='product__buy_guarantee-title'>ГАРАНТИЯ</h3>
        <p className='product__buy_guarantee-description'>Официальная 24 месяца от производителя</p>
        <Link to='/delivery-and-payment' className='product__buy_guarantee-details'>Условия доставки и оплаты</Link>
      </div>
    </div>
  );
};

const mapStateToProps = (state, props) => {
  const carted = state.cart.some(({product}) => product === props.options._id);
  return {
    carted
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    toggleCartProduct: (productId) => dispatch(toggleCartProduct(productId))
  }
}

ProductBuy.propTypes = {
  options: PropTypes.shape({
    _id: PropTypes.string.isRequired,
    currentPrice: PropTypes.number.isRequired,
    quantity: PropTypes.number.isRequired
  })
}

export default connect(mapStateToProps, mapDispatchToProps)(memo(ProductBuy));