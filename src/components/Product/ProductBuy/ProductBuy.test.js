import React from 'react';
import { render } from '@testing-library/react'
import ProductBuy from './ProductBuy';
import store from '../../../store/store'
import { Provider } from 'react-redux'

describe('ProductBuy test', () => {
  test('smoke test', () => {
    const testData = {
      _id: '13',
      currentPrice: 999,
      quantity: 5
    };
    render(<Provider store={store}><ProductBuy options={testData}/></Provider>);
  });
})