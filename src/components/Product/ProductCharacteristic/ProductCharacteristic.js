import React, { memo } from 'react'
import PropTypes from 'prop-types';
import { CHARS_MATCHES } from '../../../utils/CHARS_MATCHES'

function ProductCharacteristic ({characteristics}) {
  const createCharsMas = () => {
    const charsMas = [];
    Object.keys(characteristics).forEach(i => {
      if (CHARS_MATCHES[i]) {
        charsMas.push(
          <li key={i} className={'product__descriptions_characteristics-item'}>
            <span className={'product__descriptions_characteristics-title'}>{CHARS_MATCHES[i]} :</span>
            <span className={'product__descriptions_characteristics-desc'}>{characteristics[i]}</span>
          </li>);
      }
    })
    return charsMas;
  }

  const charsList = createCharsMas();

  return (
    <div className='product__descriptions_characteristics' id='characteristics'>
      <ul className='product__descriptions_characteristics-list' id='characteristics'>
        {charsList}
      </ul>
    </div>
  )
}

ProductCharacteristic.propTypes = {
  characteristics: PropTypes.object.isRequired
}

export default memo(ProductCharacteristic);