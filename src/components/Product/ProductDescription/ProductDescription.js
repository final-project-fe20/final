import React, { memo } from 'react'
import './ProductDescription.scss';
import PropTypes from 'prop-types'

function ProductDescription ({ description = [] }) {
  const content = description.map((p, i) => <p key={i} dangerouslySetInnerHTML={{__html: p}} />);
  return (
    <div className='product__descriptions_description' id='descriptions'>
      <div className='product__options'>
        <div className='product__options_description'>
          {content}
        </div>
      </div>
    </div>
  )
}
ProductDescription.propTypes = {
  description: PropTypes.arrayOf(PropTypes.string)
}

export default memo(ProductDescription);