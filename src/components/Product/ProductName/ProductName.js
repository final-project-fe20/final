import React, { memo } from 'react'
import './ProductName.scss';
import PropTypes from 'prop-types'

const ProductName = ({name, itemNo}) => {
  return (
    <h1 className='product__info'>
      <span className='product__name'>{name}</span>
      <span className='product__code'>Код товара : {itemNo}</span>
    </h1>
  );
};

ProductName.propTypes = {
  name: PropTypes.string.isRequired,
  itemNo: PropTypes.string.isRequired
}

export default memo(ProductName);