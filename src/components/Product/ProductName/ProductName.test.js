import React from 'react';
import {render} from '@testing-library/react';
import ProductName from './ProductName';

describe('ProductName test', () => {
  test('smoke test', () => {
    render(<ProductName name='Notebook' itemNo='112233'/>);
  });

  test('Name and VendorCode rendered correctly', () => {
    const {getByText} = render(<ProductName name='Notebook' itemNo='112233'/>);
    getByText('Notebook');
    getByText('Код товара : 112233');
  });
})