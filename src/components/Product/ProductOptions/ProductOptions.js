import React, { memo, useCallback, useState } from 'react'
import './ProductOptions.scss';
import ProductCharacteristic from '../ProductCharacteristic/ProductCharacteristic';
import Button from '../../Button/Button';
import Comments from '../Comments/Comments';
import ProductDescription from '../ProductDescription/ProductDescription';
import PropTypes from 'prop-types'

const ProductOptions = ({options}) => {
  const [activeSection, setActiveSection] = useState(1);

  const setSection = useCallback((section) => () => setActiveSection(section), []);

  return (
    <>
      <div className='product__descriptions_options'>
        <Button className={`product__descriptions-select product__descriptions-select${activeSection === 1 ? '--active' : ''}`} onClick={setSection(1)}>описание</Button>
        <Button className={`product__descriptions-select product__descriptions-select${activeSection === 2 ? '--active' : ''}`} onClick={setSection(2)}>характеристики</Button>
        <Button className={`product__descriptions-select product__descriptions-select${activeSection === 3 ? '--active' : ''}`} onClick={setSection(3)}>отзывы</Button>
      </div>
      {activeSection === 1 && <ProductDescription description={options.description}/>}
      {activeSection === 2 && <ProductCharacteristic characteristics={options}/>}
      {activeSection === 3 && <Comments _id={options._id}/>}
    </>
  );
}

ProductOptions.propTypes = {
  options: PropTypes.object.isRequired,
}

export default memo(ProductOptions);