import React from 'react';
import {render} from '@testing-library/react';
import ProductOptions from './ProductOptions';

jest.mock('../ProductDescription/ProductDescription', () => () => <div data-testid='productDescriptionTest'>test</div>);
jest.mock('../ProductCharacteristic/ProductCharacteristic', () => () => <div data-testid='productCharacteristicsTest'>test</div>);
jest.mock('../Comments/Comments', () => () => <div data-testid='CommentsTest'>test</div>);

describe('ProductOptions test', () => {
  test('smoke test', () => {
    render(<ProductOptions options={{description: ['']}}/>);
  });

  test('section is present', () => {
    const {getByTestId, queryByTestId} = render(<ProductOptions options={{description: ['']}}/>);
    getByTestId('productDescriptionTest');
    expect(queryByTestId('productCharacteristicsTest')).not.toBeInTheDocument();
    expect(queryByTestId('CommentsTest')).not.toBeInTheDocument();
  });
})