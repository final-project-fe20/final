import './ProductPhotoBlock.scss';
import React, { memo, useState } from 'react'
import PropTypes from 'prop-types'
import ButtonArrow from '../../Footer/icons/ButtonArrow';
import Button from '../../Button/Button';

const ProductPhotoBlock = ({imageUrls, name, setPhotoGalleryOptions}) => {
  const [curIndex, setCurIndex] = useState(0);

  const openPhotoGallery = (startItem) => {
    setPhotoGalleryOptions({
      show: true,
      startItem
    });
  }

  const nextPhoto = () => {
    if (curIndex < imageUrls.length - 1) {
      setCurIndex(curIndex + 1);
    } else if (curIndex === imageUrls.length - 1) {
      setCurIndex(0);
    }
  }
  const prevPhoto = () => {
    if (curIndex > 0) {
      setCurIndex(curIndex - 1);
    } else if (curIndex === 0) {
      setCurIndex(imageUrls.length - 1);
    }
  }

  const makeMainPhoto = (event) => {
    openPhotoGallery(+event.target.dataset.current);
    setCurIndex(+event.target.dataset.current);
  }

  const miniPhotos = imageUrls.map((url, i) => (
    <img
      key={i}
      className={`product__photo_mini ${i === curIndex ? 'product__photo_mini--active' : ''}`}
      src={url.replace('big', 'preview')}
      alt={`купить ${name}`}
      data-current={i}
      onClick={makeMainPhoto}
    />
  ))

  return (
    <div className='product__photo'>
      <div className='product__photo_set'>
        <div className='product__photo_set-photos'>
          {miniPhotos}
        </div>
        <div className='product__photo_arrows'>
          <div className='product__photo_arrow' onClick={prevPhoto}>
            <Button className='arrow-up'>
              <ButtonArrow/>
            </Button>
          </div>
          <div className='product__photo_arrow' onClick={nextPhoto}>
            <Button className='arrow-down'>
              <ButtonArrow/>
            </Button>
          </div>
        </div>
      </div>
      <div className='product__main-photos'>
        <img className='product__main-photos_main-photo'
             src={imageUrls[curIndex]}
             alt={`купить ${name}`}
             onClick={() => openPhotoGallery(curIndex)}
        />
      </div>
    </div>
  );
};

ProductPhotoBlock.propTypes = {
  imageUrls: PropTypes.arrayOf(PropTypes.string).isRequired,
  name: PropTypes.string.isRequired,
  setPhotoGalleryOptions: PropTypes.func.isRequired
}

export default memo(ProductPhotoBlock);
