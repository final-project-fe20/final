import React, { memo, useCallback, useEffect, useState } from 'react'
import './Promotions.scss';
import {Link} from 'react-router-dom';
import API from '../../utils/API'
import Loader from '../Loader/Loader'

const Promotions = () => {
  const [promotionsData, setPromotionsData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  const getPromotionsData = useCallback(async () => {
    const {data} = await API.request('slides');
    const filteredSlides = data.filter(({section}) => section === 'promotions');
    setPromotionsData(filteredSlides);
    setIsLoading(false);
  }, []);

  useEffect(() => getPromotionsData(), [getPromotionsData]);

  const cardList = promotionsData.map(i => <Link className='promotions__photo_wrapper' to={i.linkTo}>
    <img src={i.imageUrl}
         key={i._id}
         alt={`Изображение для ${i.customId}`}
         className='promotions__photo'>
    </img>
  </Link>)

  if (isLoading) return <Loader height='800px'/>

  return (
    <section className='promotions'>
      <div className='container'>
        <div className='promotions__title'>Акции и спецпредложения</div>
        <div className='promotions__container'>
          <div className='promotions__container_grid-item1 promotions__container_grid-item-all'>{cardList[0]}</div>
          <div className='promotions__container_grid-item2 promotions__container_grid-item-all'>{cardList[1]}</div>
          <div className='promotions__container_grid-item3 promotions__container_grid-item-all'>{cardList[2]}</div>
          <div className='promotions__container_grid-item4 promotions__container_grid-item-all'>{cardList[3]}</div>
          <div className='promotions__container_grid-item5 promotions__container_grid-item-all'>{cardList[4]}</div>
          <div className='promotions__container_grid-item6 promotions__container_grid-item-all'>{cardList[5]}</div>
        </div>
      </div>
    </section>
  )
}

export default memo(Promotions);