import React from 'react';
import {render, waitForElementToBeRemoved} from '@testing-library/react'
import Promotions from './Promotions';

jest.mock('../../utils/API', () => ({
  request: async () => Promise.resolve({
    data: [
      {
        _id: 1,
        imageUrl: 'https://res.cloudinary.com/tablet1.jpg',
        linkTo: '/catalog/tablet',
        customId: 11
      },
      {
        _id: 2,
        imageUrl: 'https://res.cloudinary.com/notebook1.jpg',
        linkTo: '/catalog/notebook',
        customId: 12
      },
      {
        _id: 3,
        imageUrl: 'https://res.cloudinary.com/laptop1.jpg',
        linkTo: '/catalog/laptops',
        customId: 13
      }
    ]
  })
}));

describe('Promotions test', () => {
  test('smoke test', async () => {
    const {getByTestId} = render(<Promotions/>);
    await waitForElementToBeRemoved(getByTestId('testLoader'));
  })

  test('Loader is present while loading process', async () => {
    const {getByTestId} = render(<Promotions/>);
    getByTestId('testLoader');
    await waitForElementToBeRemoved(getByTestId('testLoader'));
  });
})