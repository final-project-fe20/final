import React, { useEffect, useState, memo } from 'react'
import './SelectedProducts.scss'
import { connect } from 'react-redux'
import API from '../../utils/API'
import Button from '../Button/Button';

export const SelectedProducts = ({cart}) => {
  const [cartProducts, setCartProducts] = useState([]);

  const getCartProducts = () => {
    const filterRequest = cart.map(({product}) => product).join();
    const filterUrl = filterRequest.length ? `_id=${filterRequest}` : 'brand=nobrand';
    API.request(`products/filter?${filterUrl}`)
      .then(({data: {products}}) => {
        setCartProducts(products);
      })
  }

  const getPriceTotal = () => {
    return cart.reduce((sum, {product, cartQuantity}) => {
      const foundItem = cartProducts.find(({_id}) => _id === product);
      const cartItemPrice = foundItem ? foundItem.currentPrice : 0;
      return sum + cartItemPrice * cartQuantity
    }, 0);
  }

  const getProductName = (name) => {
    const serialNo = name.match(/\s*\(.*\)\s*$/gm) ? name.match(/\s*\(.*\)\s*$/gm)[0] : '';
    return serialNo.length > 20 ? name.replace(serialNo, '') : name;
  }

  useEffect(getCartProducts, [cart])

  const priceTotal = getPriceTotal();

  const getCurrentPrice = (currentPrice, id) => {
    const foundItem = cart.find(({product}) => product === id)
    return foundItem ? foundItem.cartQuantity * currentPrice : 0;
  }

  const selectedProductsCartItems = cartProducts.map(el => {
    return (
        <div className='selected-product__image-item' key={el._id}>
          <img src={el.imageUrls[0]}
               alt={el.name}
               className='selected-products__image' />
          <div>
            <p className='selected-products__desc'>{getProductName(el.name)}</p>
          </div>
          <p className='selected-products__image-price'>{getCurrentPrice(el.currentPrice, el._id)} <span>грн</span></p>
        </div>
    )
  })

  return (
        <section className='selected-products selected-products__container'>
          <div className='selected-products__inner'>
            <h2 className='selected-products__inner-title'>Товары в корзине</h2>
          </div>

          <div className='selected-products__images-wrapper'>
            {selectedProductsCartItems}
          </div>

          <p className='selected-products__delivery-cost'>Доставка - 150 грн</p>
          <p className='selected-products__order-cost'>Сумма заказа: {priceTotal}  <span className='smal-text'>грн</span></p>
          <div className='selected-products__confirm-inner'>
            <Button
              data-testid='submit-button'
              className='selected-products__confirm'
              form='checkout-form'
              type='submit'
              disabled={!cart.length}
            >
              Подтвердить
            </Button>
          </div>
        </section>
  )
}

const mapStateToProps = (state) => {
  const cart = state.cart;
  return {
    cart
  }
}

export default connect(mapStateToProps)(memo(SelectedProducts));