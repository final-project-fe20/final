import React from 'react'
import Modal from '../Modal/Modal'
import './SelectedProductsModal.scss'
import { Link } from 'react-router-dom'
import { Smile } from './icons/Smile'
import PropTypes from 'prop-types'
import Button from '../Button/Button';

const SelectedProductsModal = ({ closeFunction, orderNumber, liqpayData }) => {
  return (<Modal
          className='checkout-done'
          closeFunction={closeFunction}
          closeButton
        >
          <div className='checkout-done__inner'>
            <div className='checkout-done__img'>
              <Smile />
            </div>
            <h2 className='checkout-done__title'>Спасибо, что выбрали нас!</h2>
            <h3 className='checkout-done__info'><p>Ваш заказ №{orderNumber} успешно оформлен.</p>Мы свяжемся с вами ближайшее время.</h3>
            <div className='checkout-done__buttons-wrapper'>
              {!!liqpayData && <div className='checkout-done__liqpay' dangerouslySetInnerHTML={{__html: liqpayData}} />}
              <Button
                className='checkout-done__button'
                onClick={closeFunction}>
                <Link
                  to='/catalog'
                  className='checkout-done__redirect-button'>
                  продолжить покупки
                </Link>
              </Button>
            </div>
          </div>
        </Modal>
  )
}

SelectedProductsModal.propTypes = {
  closeFunction: PropTypes.func.isRequired,
  orderNumber: PropTypes.string.isRequired,
}

export default SelectedProductsModal;