import React, {useCallback, useEffect, useMemo, useState, memo} from 'react';
import PropTypes from 'prop-types';
import API from '../../utils/API';
import Sliders from '../Slider/Slider';
import Card from '../Card/Card';
import Loader from '../Loader/Loader';
import './SimilarModels.scss';

const SimilarModels = ({category}) => {
  const [similarData, setSimilarData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  const getSimilarData = useCallback(async () => {
    const {data: {products: similarProducts}} = await API.request(`products/filter?categories=${category}&perPage=10`);
    setIsLoading(false);
    setSimilarData(similarProducts);
  }, [category]);

  useEffect(getSimilarData, [getSimilarData]);

  const similarItems = useMemo(() => similarData.map(i => <Card key={i._id} data={i}/>), [similarData]);

  if (isLoading) return <Loader height='600px'/>

  if (similarData.length < 4) return null;

  return (
    <section className='similar-models'>
      <div className='container'>
        <h1 className='similar-models__title'>Похожие модели</h1>
      </div>
      <Sliders
        quantity={3}
        dots={false}>
        {similarItems}
      </Sliders>
    </section>
  )
}

SimilarModels.propTypes = {
  category: PropTypes.string.isRequired
}

export default memo(SimilarModels);