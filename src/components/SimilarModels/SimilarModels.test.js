import React from 'react';
import {render, waitForElementToBeRemoved} from '@testing-library/react';
import SimilarModels from './SimilarModels';

jest.mock('react-slick', () => () => <div data-testid='slickTest'>test</div>);
jest.mock('../../utils/API', () => ({
  request: async () => Promise.resolve({
    data: {
      products: [
        {
          _id: '1234'
        },
        {
          _id: '1235'
        },
        {
          _id: '1236'
        },
        {
          _id: '1237',
        }
      ]
    }
  })
}));

describe('SimilarModels test', () => {
  test('smoke test', async () => {
    const {getByTestId} = render(<SimilarModels category='testCategory'/>);
    await waitForElementToBeRemoved(getByTestId('testLoader'));
  });

  test('Slider is present when there is enough similar models', async () => {
    const {getByTestId} = render(<SimilarModels category='testCategory'/>);
    await waitForElementToBeRemoved(getByTestId('testLoader'));
    getByTestId('slickTest');
  });
})