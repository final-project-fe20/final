import React, {memo} from 'react'
import Button from '../../components/Button/Button'

const SampleNextArrow = ({onClick}) => {
  return (
    <Button className='arrow-right arrow'
            onClick={onClick}>
      <div className='custom-arrow-right'></div>
    </Button>
  )
}

export default memo(SampleNextArrow)