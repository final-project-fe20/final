import React, {memo} from 'react'
import Button from '../../components/Button/Button'

const SamplePrevArrow = ({onClick}) => {
  return (
    <Button className='arrow-left arrow'
            onClick={onClick}>
      <div className='custom-arrow-left'></div>
    </Button>
  )
}

export default memo(SamplePrevArrow)