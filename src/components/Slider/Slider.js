import React, {memo} from 'react'
import Slider from 'react-slick'
import './Slider.scss'
import SampleNextArrow from './SampleNextArrow'
import SamplePrevArrow from './SamplePrevArrow'
import { PropTypes } from 'prop-types'

const Sliders = ({ quantity, dots, children, ...restSettings }) => {
  let settings = {
    className: 'center',
    dots: dots,
    infinite: true,
    speed: 500,
    slidesToShow: quantity,
    swipeToSlide: true,
    slidesToScroll: 1,
    centerMode: true,
    centerPadding: '-10px',
    draggable: false,
    responsive: [
      {
        breakpoint: 500,
        settings: {
          arrows: true,
          infinite: true,
          slidesToScroll: 1,
          swipeToSlide: true,
        }
      },
      {
        breakpoint: 768,
        settings: {
          arrows: true,
          infinite: true,
          slidesToScroll: 1,
          swipeToSlide: true,
          slideToShow: 3,
        }
      },
      {
        breakpoint: 360,
        settings: {
          infinite: true,
          slidesToScroll: 1,
          swipeToSlide: true,
          slidesToShow: 1
        }
      }
    ]
  }

  if (quantity > 1) {
    settings.dots = false
    settings = {
      ...settings,
      nextArrow: <SampleNextArrow/>,
      prevArrow: <SamplePrevArrow/>
    }
  } else {
    settings.dots = true
    settings = {
      ...settings,
      autoplay: true,
      nextArrow: null,
      prevArrow: null,
      ...restSettings
    }
  }

  return (
    <div className='slider__wrapper'>
      <div className='container'>
        <Slider {...settings}>
          {children}
        </Slider>
      </div>
    </div>
  )
}

Sliders.propTypes = {
  quantity: PropTypes.number.isRequired,
  dots: PropTypes.bool.isRequired,
  children: PropTypes.array.isRequired
}

export default memo(Sliders)