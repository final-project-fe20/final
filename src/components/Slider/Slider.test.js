import React from 'react';
import {render} from '@testing-library/react';
import Sliders from './Slider';
import SamplePrevArrow from './SamplePrevArrow';
import SampleNextArrow from './SampleNextArrow'

describe('Slider test', () => {
  test('smoke test', () => {
    const children = []
    render(<Sliders dots={true} quantity={1}>{children}</Sliders>);
  })

  test('show slider prev arrow', () => {
    render(<SamplePrevArrow onClick={jest.fn()}/>);
  })

  test('show slider next arrow', () => {
    render(<SampleNextArrow onClick={jest.fn()}/>);
  })
})