import {useCallback} from 'react';
import {useDispatch} from 'react-redux';
import API from '../utils/API';
import {clearCart, setCart} from '../store/cart/actions'
import {clearCustomerInfo, setCustomerLoading} from '../store/customer/actions';
import {getCustomerInfo} from '../store/customer/operations';
import jwt_decode from 'jwt-decode';

export const useLogin = () => {
  const dispatch = useDispatch();

  const checkToken = (token) => {
    const {exp: expTimeStamp} = jwt_decode(token);
    const currentTimeStamp = ((new Date().getTime()) / 1000);
    return (expTimeStamp >= currentTimeStamp);
  }

  const buildCart = useCallback(async () => {
    try {
      const localCart = JSON.parse(localStorage.getItem('cart'));
      const {data: serverCart} = await API.request('cart');
      if (!serverCart) {
        dispatch(setCart(localCart || []));
        return;
      }
      const cartProducts = serverCart.products.map(({cartQuantity, product: {_id}}) => ({
        product: _id,
        cartQuantity
      }));
      const resultData = localCart ? mergeCartData(cartProducts, localCart) : cartProducts;
      dispatch(setCart(resultData));
    } catch (e) {
      setCart([])
    }
  }, [dispatch]);

  const buildCustomer = useCallback(() => dispatch(getCustomerInfo()), [dispatch])

  const mergeCartData = (serverData, localData) => {
    const resultData = [...localData];
    serverData.forEach(serverItem => {
      if (!localData.some(localItem => localItem.product === serverItem.product)) resultData.push(serverItem);
    })
    return resultData;
  }

  const handleLogin = useCallback((token, isRemembered = !!localStorage.getItem('token')) => {
    if (!checkToken(token)) {
      localStorage.removeItem('token');
      sessionStorage.removeItem('token');
    }
    if (isRemembered) {
      localStorage.setItem('token', token);
    } else {
      sessionStorage.setItem('token', token);
    }
    API.token = token;
    buildCart();
    buildCustomer();
  }, [buildCart, buildCustomer]);

  const handleLogout = useCallback(() => {
    localStorage.removeItem('token');
    sessionStorage.removeItem('token');
    API.token = '';
    dispatch(clearCart());
    dispatch(clearCustomerInfo());
  }, [dispatch]);

  const handleNoAuth = useCallback(() => dispatch(setCustomerLoading(false)), [dispatch]);

  return {
    handleLogin,
    handleLogout,
    handleNoAuth
  }
}