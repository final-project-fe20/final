import {useCallback} from 'react';

import {NON_FILTERS_PARAMS} from './useUrlQuery';

export const useProductFilter = () => {
  const filterProducts = useCallback((products, parsedParams) => {
    NON_FILTERS_PARAMS.forEach(p => Object.defineProperty(parsedParams, p, {enumerable: false}));
    let filteredProducts = [...products];
    if (parsedParams.minPrice && parsedParams.maxPrice) filteredProducts = filteredProducts.filter(({currentPrice}) => currentPrice >= parsedParams.minPrice && currentPrice <= parsedParams.maxPrice);
    filteredProducts = filteredProducts.filter(p => Object.keys(parsedParams).every(t => parsedParams[t].includes(p[t])));
    return filteredProducts;
  }, [])

  return {
    filterProducts
  }
}