import React from 'react';
import {render} from '@testing-library/react';
import {useProductFilter} from './useProductFilter';

const UseProductFilterTest = ({products, parsedParams}) => {
  const {filterProducts} = useProductFilter();
  const filteredProducts = filterProducts(products, parsedParams);
  const filteredItems = filteredProducts.map(({brand}, index) => <div key={index} data-testid='filterResult'>{brand}</div>)
  return (
    <div>
      {filteredItems}
    </div>
  );
}

describe('useProductFilter test', () => {
  test('filterProducts returns properly filtered products', () => {
    const testProducts = [
      {
        enabled: true,
        imageUrls: [
          'https://content2.rozetka.com.ua/goods/images/big_tile/144249849.jpg',
          'https://content2.rozetka.com.ua/goods/images/original/144249849.jpg',
          'https://content1.rozetka.com.ua/goods/images/original/144249682.jpg',
          'https://content1.rozetka.com.ua/goods/images/original/144249836.jpg'
        ],
        quantity: 0,
        _id: '60a0c5820a3fd8000467e0cd',
        name: 'Ноутбук Apple MacBook Air 13" M1 256GB 2020 (MGN93) Silver',
        currentPrice: 33999,
        categories: 'slim-laptops',
        description: 'Он быстро справляется с вашими задачами, задействуя потрясающую скорость 8‑ядерного процессора. Открывает возможности нового уровня в приложениях и играх со сложной графикой, используя всю мощь 8‑ядерного графического процессора. И ускоряет операции машинного обучения, применяя 16‑ядерную систему Neural Engine. Всё происходит бесшумно, потому что это ноутбук без вентилятора. И он работает без подзарядки до 18 часов напролёт.',
        bestseller: false,
        brand: 'Apple',
        screenSize: '13"',
        screenType: 'Retina',
        cpu: 'Apple M1',
        ram: '8 GB',
        graphicsCard: 'Интегрированная',
        diskType: 'SSD',
        diskSize: '512 GB',
        itemNo: '265143',
        date: '2021-05-16T07:10:58.487Z',
        __v: 0
      },
      {
        enabled: true,
        imageUrls: [
          'https://content1.rozetka.com.ua/goods/images/big_tile/89657405.jpg',
          'https://content1.rozetka.com.ua/goods/images/original/89657405.jpg',
          'https://content1.rozetka.com.ua/goods/images/big/89657405.jpg',
          'https://content1.rozetka.com.ua/goods/images/big/89657462.jpg'
        ],
        quantity: 124,
        _id: '60a12b9da46af30004fb3a15',
        name: 'Ноутбук Lenovo V145-15 (81MT002CRA) Black',
        currentPrice: 8999,
        previousPrice: 11199,
        categories: 'slim-laptops',
        description: 'ИТ-специалисты будут вам благодарны Производительность, безопасность и надежность ноутбука Lenovo V145 — просто подарок для ИТ-отдела. Сотрудников наверняка порадует производительность AMD и наличие множества портов и разъемов, они будут меньше обращаться к ИТ-специалистам. А вы сможете не сомневаться в качестве шифрования данных и паролей, а также в высокой надежности и долговечности этого ноутбука.',
        bestseller: true,
        brand: 'Lenovo',
        screenSize: '15.6"',
        screenType: 'IPS',
        cpu: 'Intel Core i5-1135G7',
        ram: '8 GB',
        graphicsCard: 'Интегрированная',
        diskType: 'SSD',
        diskSize: '128 GB',
        itemNo: '111926',
        date: '2021-05-16T14:26:37.123Z',
        __v: 0
      }
    ];
    const testParsedParams = {
      brand: ['Lenovo', 'Asus', 'Dell']
    }
    const {getByText, getAllByTestId, queryByText} = render(<UseProductFilterTest products={testProducts} parsedParams={testParsedParams}/>);
    const filterResults = getAllByTestId('filterResult');
    expect(filterResults.length).toBe(1);
    getByText(testParsedParams.brand[0]);
    expect(queryByText(testParsedParams.brand[1])).not.toBeInTheDocument();
  });
})