import {useLocation, useHistory} from 'react-router-dom';
import queryString from 'query-string';
import {useCallback} from 'react';

export const NON_FILTERS_PARAMS = ['minPrice', 'maxPrice', 'page', 'perPage', 'sort'];

export const useUrlQuery = () => {
  const location = useLocation();
  const history = useHistory();

  const getParsedQuery = useCallback((query = location.search) => {
    return queryString.parse(query, {arrayFormat: 'bracket-separator'});
  }, [location.search]);

  const getStringQuery = useCallback((parsedQuery) => {
    return queryString.stringify(parsedQuery, {arrayFormat: 'bracket-separator'})
  }, []);

  const pushQuery = useCallback((parsedParams, clearFilters = false) => {
    const parsed = getParsedQuery();
    if (clearFilters) {
      Object.keys(parsed).forEach(type => {
        if (!NON_FILTERS_PARAMS.includes(type)) delete parsed[type]
      })
    }
    const resultQuery = {...parsed, ...parsedParams};
    history.push(`?${getStringQuery(resultQuery)}`);
  }, [getParsedQuery, getStringQuery, history]);

  return {
    getParsedQuery,
    getStringQuery,
    pushQuery,
    urlParam: location.search,
    NON_FILTERS_PARAMS
  }
}