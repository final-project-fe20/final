import React, {useCallback, useState} from 'react';
import BreadCrumbs from '../../components/BreadCrumbs/BreadCrumbs';
import CabinetMenu from '../../components/CabinetMenu/CabinetMenu';
import CabinetRoutes from '../../routes/CabinetRoutes';
import './Cabinet.scss';

const Cabinet = () => {
  const [okMsgShow, setOkMsgShow] = useState(false);

  const showOkMsg = useCallback(() => {
    setOkMsgShow(true);
    setTimeout(() => setOkMsgShow(false), 3000);
  }, [])

  return (
    <main className='cabinet'>
      <p className={`cabinet__ok-msg ${okMsgShow ? 'cabinet__ok-msg--show' : ''}`}>
        Изменения успешно внесены!
      </p>
      <div className='container'>
        <BreadCrumbs/>
        <h1 className='cabinet__title'>Личный Кабинет</h1>
        <section className='cabinet__content'>
          <CabinetMenu/>
          <CabinetRoutes showOkMsg={showOkMsg}/>
        </section>
      </div>
    </main>
  );
}

export default Cabinet;