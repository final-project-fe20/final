import React from 'react';
import {render} from '@testing-library/react';
import Cabinet from './Cabinet';

jest.mock('../../routes/CabinetRoutes', () => () => <div data-testid='testRoutes'>test</div>);
jest.mock('../../components/CabinetMenu/CabinetMenu', () => () => <div data-testid='testMenu'>test</div>);

describe('Cabinet test', () => {
  test('smoke test', () => {
    render(<Cabinet/>);
  });

  test('Menu and Routes renders fine', () => {
    const {getByTestId} = render(<Cabinet/>);
    getByTestId('testRoutes');
    getByTestId('testMenu');
  });
})