import React, {useCallback, useEffect, useState} from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import API from '../../utils/API';
import BreadCrumbs from '../../components/BreadCrumbs/BreadCrumbs';
import CartItem from '../../components/CartItem/CartItem';
import Loader from '../../components/Loader/Loader';
import './Cart.scss';

export const Cart = ({cart}) => {
  const [cartProducts, setCartProducts] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  const getCartProducts = useCallback(async () => {
    const filterRequest = cart.map(({product}) => product).join();
    const filterUrl = filterRequest.length ? `_id=${filterRequest}` : 'brand=nobrand';
    const {data: {products}} = await API.request(`products/filter?${filterUrl}`);
    setCartProducts(products || []);
    setIsLoading(false);
  }, [cart]);

  const getPriceTotal = () => {
    return cart.reduce((sum, {product, cartQuantity}) => {
      const foundItem = cartProducts.find(({_id}) => _id === product);
      const cartItemPrice = foundItem ? foundItem.currentPrice : 0;
      return sum + cartItemPrice * cartQuantity
    }, 0);
  }

  const getQuantityTotal = () => {
    if (cartProducts.length !== cart.length) return 0;
    const itemQuantities = cart.map(({product, cartQuantity}) => cartProducts.some(({_id}) => _id === product) ? cartQuantity : 0);
    return itemQuantities.length ? itemQuantities.reduce((a, b) => a + b) : 0;
  }

  const getQuantityWordEnding = (quantityTotal) => {
    const n = Math.abs(quantityTotal) % 100;
    if (n >= 5 && n <= 20) return 'ов';
    if (n % 10 === 1) return '';
    if (n % 10 >= 2 && n % 10 <= 4) return 'а';
    return 'ов';
  }

  useEffect(() => getCartProducts(), [getCartProducts]);

  if (isLoading) return <Loader height='800px'/>

  const cartItems = cartProducts.map(i => <CartItem key={i._id} data={i}/>);
  const priceTotal = getPriceTotal();
  const quantityTotal = getQuantityTotal();

  return (
    <main>
      <div className='container'>
        <BreadCrumbs/>
        <section className='cart'>
          <h1 className='cart__title'>Моя корзина</h1>
          <div className='cart__header'>
            <span>Товар</span>
            <span>Цена</span>
            <span>Количество</span>
            <span>Всего</span>
          </div>
          <ul className='cart__list'>
            {cartItems}
            {!cartItems.length && <li className='cart__empty-msg'>Корзина пуста</li>}
          </ul>
          <div className='cart__checkout-block'>
            <Link to='/catalog' className='cart__btn cart__btn--continue'>ПРОДОЛЖИТЬ ПОКУПКИ</Link>
            {!!cart.length && (
              <p className='cart__summary'>
                Итого: <strong>{quantityTotal} товар{getQuantityWordEnding(quantityTotal)}</strong> на общую сумму
              </p>
            )}
            {!!cart.length && <p className='cart__total-price'>{priceTotal} грн</p>}
            <Link to='/checkout' disabled={!cart.length} className='cart__btn cart__btn--checkout'>ОФОРМИТЬ</Link>
          </div>
        </section>
      </div>
    </main>
  );
}

const mapStateToProps = (state) => {
  const cart = state.cart;
  return {
    cart
  }
}

export default connect(mapStateToProps)(Cart);