import React from 'react';
import {render, waitForElementToBeRemoved} from '@testing-library/react';
import {Cart} from './Cart';

jest.mock('../../components/CartItem/CartItem', () => () => <div data-testid='cartItemTest'>test</div>);
jest.mock('../../components/BreadCrumbs/BreadCrumbs', () => () => <div data-testid='componentTest'>test</div>);
jest.mock('../../utils/API', () => ({
  request: async () => Promise.resolve({
    data: {
      products: [
        {
          _id: '1234',
          currentPrice: 1111
        }
      ]
    }
  })
}));

describe('Cart test', () => {
  test('smoke test', async () => {
    const {getByTestId} = render(<Cart cart={[]}/>);
    await waitForElementToBeRemoved(getByTestId('testLoader'));
  });

  test('renders proper number of CartItems', async () => {
    const testCart = [
      {
        product: '1234',
        cartQuantity: 2
      }
    ]
    const {getByTestId, getAllByTestId} = render(<Cart cart={testCart}/>);
    await waitForElementToBeRemoved(getByTestId('testLoader'));
    const cartItems = getAllByTestId('cartItemTest');
    expect(cartItems.length).toBe(1);
  });

  test('priceTotal counting correct', async () => {
    const testCart = [
      {
        product: '1234',
        cartQuantity: 2
      }
    ]
    const {getByTestId, getByText} = render(<Cart cart={testCart}/>);
    await waitForElementToBeRemoved(getByTestId('testLoader'));
    getByText('2222 грн');
  });
})