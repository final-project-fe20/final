import React, {useState, useEffect, useCallback, useMemo} from 'react';
import {connect} from 'react-redux';
import {useParams, useHistory} from 'react-router-dom';
import API from '../../utils/API';
import {useUrlQuery} from '../../hooks/useUrlQuery';
import {useProductFilter} from '../../hooks/useProductFilter';
import {setProducts} from '../../store/catalog/actions';
import BreadCrumbs from '../../components/BreadCrumbs/BreadCrumbs';
import CardList from '../../components/CardList/CardList';
import Filter from '../../components/Filter/Filter';
import Pagination from '../../components/Pagination/Pagination';
import Button from '../../components/Button/Button';
import FilterIcon from './icons/FilterIcon';
import Loader from '../../components/Loader/Loader';
import './Catalog.scss';

export const Catalog = ({products, setProducts}) => {
  const [processedProducts, setProcessedProducts] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [perPage, setPerPage] = useState(9);
  const [sortMethod, setSortMethod] = useState('default');
  const [page, setPage] = useState(0);
  const [mobileFilterShow, setMobileFilterShow] = useState(false);
  const {getParsedQuery, pushQuery, urlParam} = useUrlQuery();
  const {filterProducts} = useProductFilter();
  const params = useParams();
  const history = useHistory();

  const getProducts = useCallback(async () => {
    let request = '';
    if (params.category) {
      const {data: categories} = await API.request('catalog');
      const categoryInfo = categories.find(cat => cat.id === params.category);
      if (!categoryInfo) {
        history.push('/catalog');
        return;
      }
      if (categoryInfo.parentId === 'null') {
        const subCats = categories.filter(cat => cat.parentId === params.category);
        const subCatsQuery = subCats.map(({id}) => id).join();
        request = `categories=${subCatsQuery}`;
      } else {
        request = `categories=${params.category}`;
      }
    }
    const {data: {products}} = await API.request(`products/filter?${request}`);
    setPage(0);
    setProducts(products);
    setProcessedProducts(products);
    setIsLoading(false);
  }, [params.category, history, setProducts]);

  const sortProducts = (products, sortMethod) => {
    let compareFn;
    if (sortMethod === '+price') {
      compareFn = (a, b) => a.currentPrice - b.currentPrice;
    } else if (sortMethod === '-price') {
      compareFn = (a, b) => b.currentPrice - a.currentPrice;
    } else if (sortMethod === '+name') {
      compareFn = (a, b) => a.name.localeCompare(b.name);
    } else if (sortMethod === '-date') {
      compareFn = (a, b) => new Date(b.date) - new Date(a.date);
    } else {
      compareFn = (a, b) => a._id - b._id;
    }
    products.sort(compareFn);
  }

  const onPerPageHandleChange = (e) => {
    pushQuery({
      perPage: e.target.value,
      page: 1
    })
  }

  const onSortHandleChange = (e) => {
    pushQuery({
      sort: e.target.value,
      page: 1
    })
  }

  const changePage = useCallback((targetPage) => {
    if (targetPage >= 0 && targetPage <= (Math.ceil(processedProducts.length / perPage) - 1)) {
      pushQuery({
        page: +targetPage + 1
      })
      setPage(targetPage);
    }
  }, [perPage, processedProducts.length, pushQuery]);

  const ProcessByUrlParam = () => {
    const parsed = getParsedQuery();
    if (!Object.keys(parsed).length) {
      setMobileFilterShow(false);
      setProcessedProducts(products);
      return;
    }
    const filteredProducts = filterProducts(products, parsed);
    if (parsed.page) changePage(parsed.page - 1);
    if (parsed.perPage) setPerPage(parsed.perPage);
    if (parsed.sort) {
      setSortMethod(parsed.sort);
      sortProducts(filteredProducts, parsed.sort);
    }
    setMobileFilterShow(false);
    setProcessedProducts(filteredProducts);
  }

  const preventScroll = () => {
    if (mobileFilterShow) {
      document.body.classList.add('overflow-hidden');
    } else {
      document.body.classList.remove('overflow-hidden')
    }
  }

  useEffect(getProducts, [getProducts]);
  useEffect(ProcessByUrlParam, [urlParam, products, changePage, getParsedQuery, filterProducts]);
  useEffect(preventScroll, [mobileFilterShow]);

  const shownProducts = useMemo(() => processedProducts.slice(page * perPage, (page + 1) * perPage), [page, perPage, processedProducts]);
  const pagesTotal = useMemo(() => Math.ceil(processedProducts.length / perPage), [perPage, processedProducts.length]);

  if (isLoading) return <Loader height='800px'/>

  return (
    <main className='main'>
      <div className='container'>
        <section className='catalog-page'>
          <BreadCrumbs/>
          <div className='catalog-page__wrapper'>
            <Filter mobileFilterShow={mobileFilterShow}/>
            {mobileFilterShow && <div className='mobile-nav-overlay' onClick={() => {
              setMobileFilterShow(false);
            }}/>}
            <div className='catalog-page__catalog'>
              <div className='catalog-page__option-panel option-panel'>
                <p className='option-panel__quantity-indicator'>
                  {processedProducts.length ? page * perPage + 1 : '0'} -&nbsp;
                  {(page + 1) * perPage <= processedProducts.length ? (page + 1) * perPage : processedProducts.length} из&nbsp;
                  {processedProducts.length}
                </p>
                <p className='option-panel__shown-quantity'>
                  Показывать
                  <select value={perPage}
                          onChange={onPerPageHandleChange}
                          className='option-panel__dropdown'>
                    <option value='9'>9 товаров</option>
                    <option value='18'>18 товаров</option>
                    <option value='27'>27 товаров</option>
                  </select>
                </p>
                <p className='option-panel__sort'>
                  <span>Сортировать по:</span>
                  <select value={sortMethod}
                          onChange={onSortHandleChange}
                          className='option-panel__dropdown'>
                    <option value='default'>По умолчанию</option>
                    <option value='+price'>По возрастанию цены</option>
                    <option value='-price'>По убыванию цены</option>
                    <option value='-date'>От новых к старым</option>
                    <option value='+name'>По названию</option>
                  </select>
                </p>
                <Button
                  className='option-panel__show-filter-btn'
                  onClick={() => setMobileFilterShow(true)}>
                  <FilterIcon/>
                  Фильтр
                </Button>
              </div>
              <CardList products={shownProducts}/>
              <Pagination page={page} changePage={changePage} pagesTotal={pagesTotal}/>
            </div>
          </div>
        </section>
      </div>
    </main>
  );
}

const mapStateToProps = (state) => {
  const products = state.catalog.products;
  return {
    products
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setProducts: (products) => dispatch(setProducts(products))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Catalog);