import React from 'react';
import {render} from '@testing-library/react';
import {Catalog} from './Catalog';

jest.mock('../../components/BreadCrumbs/BreadCrumbs', () => () => <div data-testid='componentTest'>test</div>);
jest.mock('../../hooks/useUrlQuery', () => ({
  useUrlQuery: () => ({
    getParsedQuery: () => ({}),
    getStringQuery: () => '',
    pushQuery: jest.fn(),
    urlParam: '',
    NON_FILTERS_PARAMS: ['']
  })
}));
jest.mock('../../hooks/useProductFilter', () => ({
  useProductFilter: () => ({
    filterProducts: () => jest.fn()
  })
}));

describe('Catalog test', () => {
  test('smoke test', () => {
    render(<Catalog products={[]}/>);
  });

  test('Loader is present while loading process', () => {
    const {getByTestId} = render(<Catalog products={[]}/>);
    getByTestId('testLoader')
  });
})