import React from 'react'
import './Checkout.scss'
import ContactDetails from '../../components/ContactDetails/ContactDetails'
import SelectedProducts from '../../components/SelectedProducts/SelectedProducts'
import BreadCrumbs from '../../components/BreadCrumbs/BreadCrumbs'

const Checkout = () => {
  return (
    <main className='oredering-container container'>
      <BreadCrumbs/>
      <h1 className='ordering__title'>оформление заказа</h1>
      <div className='ordering__inner'>
        <div className='contact-details__wrapper'>
          <ContactDetails />
        </div>
        <div>
          <SelectedProducts />
        </div>
      </div>
    </main>
  )
}

export default Checkout