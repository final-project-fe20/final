import React, {useEffect} from 'react';
import {useHistory, useLocation} from 'react-router-dom';
import setPageTitle from '../../utils/setPageTitle';
import Button from '../../components/Button/Button';
import {IceCreamIcon} from './icons/IceCreamIcon';
import './ErrorPage.scss';

const ErrorPage = () => {
  const history = useHistory();
  const location = useLocation();

  const goToHomePage = () => {
    if (location.pathname !== '/') {
      history.push('/')
    } else history.go();
  }

  useEffect(() => setPageTitle('Упс! Что-то пошло не так!'), []);

  return (
    <div className='error-page'>
      <div className='container'>
        <IceCreamIcon/>
        <h1 className='error-page__title'>
          Ой, что-то пошло не так...
        </h1>
        <Button
          className='error-page__btn'
          onClick={() => goToHomePage()}
        >
          На главную
        </Button>
      </div>
    </div>
  );
}

export default ErrorPage;