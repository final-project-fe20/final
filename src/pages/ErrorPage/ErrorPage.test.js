import React from 'react';
import {render} from '@testing-library/react';
import ErrorPage from './ErrorPage';

describe('ErrorPage test', () => {
  test('smoke test', () => {
    render(<ErrorPage/>);
  });

  test('goToHome btn is present', () => {
    const {getByTestId} = render(<ErrorPage/>);
    getByTestId('testButton');
  });
})