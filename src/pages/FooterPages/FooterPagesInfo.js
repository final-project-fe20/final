import React, { useCallback, useEffect, useMemo, useState, memo } from 'react';
import { useLocation } from 'react-router';
import Loader from '../../components/Loader/Loader';
import Pagination from '../../components/Pagination/Pagination';
import { useUrlQuery } from '../../hooks/useUrlQuery';
import API from '../../utils/API';
import setPageTitle from '../../utils/setPageTitle';
import FooterPagesLayout from './FooterPagesLayout';

const FooterPagesInfo = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [htmlData, setHtmlData] = useState({__html: ''});
  const [siteMapData, setSiteMapData] = useState({
    currentPage: 1,
    itemsPerPageCount: 50,
    allData: [],
  });
  const { pushQuery } = useUrlQuery();
  const { pathname } = useLocation();

  const changePage = useCallback(
    (targetPage) => {
      if (
        targetPage >= 0 &&
        targetPage <= Math.ceil(siteMapData.allData.length / siteMapData.itemsPerPageCount) - 1
      ) {
        pushQuery({
          page: +targetPage + 1,
        });
        setSiteMapData(siteMapData => ({ ...siteMapData, currentPage: targetPage + 1, currentData: [] }));
      }
    },
    [pushQuery, siteMapData.allData.length, siteMapData.itemsPerPageCount]
  );

  useEffect(() => {
    if (pathname === '/sitemap') {
      API.request('products')
        .then((res) => {
          setSiteMapData((siteMapData) => ({
            ...siteMapData,
            allData: res.data,
          }));
          setPageTitle('Карта Сайта');
          setIsLoading(true);
        })
        .catch((e) => {
          throw new Error(e);
        });
    } else {
      API.request(`pages${pathname}`)
        .then((res) => {
          setHtmlData({ __html: res.data.htmlContent }); // temporarily
          setPageTitle(res.data.title);
          setIsLoading(true);
        })
        .catch((err) => {
          if (err.response.status === 400) {
            setIsLoading(true);
          }
        });
    }
  }, [pathname]);

  const itemsPerPage = useMemo(() => siteMapData.allData.slice((siteMapData.currentPage - 1) * siteMapData.itemsPerPageCount, siteMapData.currentPage * siteMapData.itemsPerPageCount), [siteMapData.allData, siteMapData.currentPage, siteMapData.itemsPerPageCount]);
  const totalPagesCount = useMemo(() => Math.ceil(siteMapData.allData.length / siteMapData.itemsPerPageCount), [siteMapData.itemsPerPageCount, siteMapData.allData.length]);
  const pageToShow = useMemo(() => itemsPerPage.map(e => <p className='footer-page__text' key={`${pathname.slice(1)}-${e.itemNo}`}><a className={`${pathname.slice(1)}__item__link`} key={`${pathname.slice(1)}-${e.itemNo}-link`} href={`/product/${e.itemNo}`}>{e.name}</a></p>), [itemsPerPage, pathname]);
  if (!isLoading) return <Loader height='800px' />;

  if (pathname === '/sitemap') {
    return (
      <FooterPagesLayout className={pathname.slice(1)}>
        <h1 className='footer-page__title'>Карта Сайта</h1>
        {pageToShow}
        <Pagination
          page={siteMapData.currentPage - 1}
          changePage={changePage}
          pagesTotal={totalPagesCount}
        />
      </FooterPagesLayout>
    );
  } else {
    return (
      <FooterPagesLayout htmlContent={htmlData} className={pathname.slice(1)} />
    );
  }
};

export default memo(FooterPagesInfo);
