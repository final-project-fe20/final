import React from 'react';
import { render } from '@testing-library/react';
import FooterPagesInfo from './FooterPagesInfo';
import { MemoryRouter } from 'react-router';

const FOOTER_PATHS_ARRAY = [
  '/about-us',
  '/sitemap',
  '/policy',
  '/credit',
  '/delivery-and-payment',
  '/service-and-warranty'
]

describe('Testing FooterPagesInfo', () => {
  test('Smoke test', () => {
    render(<MemoryRouter initialEntries={FOOTER_PATHS_ARRAY}><FooterPagesInfo /></MemoryRouter>)
  })
})