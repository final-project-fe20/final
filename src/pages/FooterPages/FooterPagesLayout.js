import React, { memo } from 'react';
import './FooterPagesLayout.scss';
import PropTypes from 'prop-types';

const FooterPagesLayout = ({ className, htmlContent, children }) => {
  return (
    <section className={`footer-page ${className}`}>
      <div className='container'>
        <div className='footer-page__inner' dangerouslySetInnerHTML={htmlContent}>
         {children}
        </div>
      </div>
    </section>
  );
};

FooterPagesLayout.propTypes = {
  className: PropTypes.string.isRequired,
  htmlContent: PropTypes.shape({
    __html: PropTypes.string
  }),
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ])
}

export default memo(FooterPagesLayout);
