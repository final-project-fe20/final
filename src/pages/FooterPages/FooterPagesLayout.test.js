import React from 'react';
import {render} from '@testing-library/react';
import FooterPagesLayout from './FooterPagesLayout';

describe('FooterPagesLayout testing', () => {
  test('smoke test', () => {
    render(<FooterPagesLayout className='test' />);
  });

  test('render with html content', () => {
    render(<FooterPagesLayout className='test' htmlContent={{__html: 'test'}} />);
  });

  test('render with children', () => {
    render(<FooterPagesLayout className='test'>[]</FooterPagesLayout>);
  });
});