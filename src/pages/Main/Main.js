import React, {useEffect} from 'react';
import setPageTitle from '../../utils/setPageTitle';
import BannerList from '../../components/BannerList/BannerList';
import Populars from '../../components/Populars/Populars';
import Promotions from '../../components/Promotions/Promotions';

const Main = () => {
  useEffect(() => setPageTitle('Интернет-магазин электроники'), []);

  return (
    <main className='main'>
      <BannerList/>
      <Populars/>
      <Promotions/>
    </main>
  )
}

export default Main;