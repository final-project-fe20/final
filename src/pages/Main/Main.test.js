import React from 'react';
import {render} from '@testing-library/react';
import Main from './Main';

jest.mock('../../components/BannerList/BannerList', () => () => <div data-testid='bannerListTest'>test</div>);
jest.mock('../../components/Populars/Populars', () => () => <div data-testid='popularsTest'>test</div>);
jest.mock('../../components/Promotions/Promotions', () => () => <div data-testid='promotionsTest'>test</div>);

describe('Main test', () => {
  test('smoke test', () => {
    render(<Main/>);
  });

  test('base sections are present', () => {
    const {getByTestId} = render(<Main/>);
    getByTestId('bannerListTest');
    getByTestId('popularsTest');
    getByTestId('promotionsTest');
  });
})