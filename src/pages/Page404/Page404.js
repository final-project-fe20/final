import React, {useEffect} from 'react';
import {useHistory} from 'react-router-dom';
import setPageTitle from '../../utils/setPageTitle';
import Button from '../../components/Button/Button';
import {Error404Icon} from './icons/Error404Icon';
import './Page404.scss';

const Page404 = () => {
  const history = useHistory();

  useEffect(() => setPageTitle('Страница не найдена'), []);

  return (
    <main className='page-404'>
      <div className='container'>
        <Error404Icon/>
        <h1 className='page-404__title'>
          Страница не найдена
        </h1>
        <Button
          className='page-404__btn'
          onClick={() => history.push('/')}
        >
          На главную
        </Button>
      </div>
    </main>
  )
}

export default Page404;