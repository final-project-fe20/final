import React from 'react';
import {render} from '@testing-library/react';
import Page404 from './Page404';

describe('Page404 test', () => {
  test('smoke test', () => {
    render(<Page404/>);
  });

  test('goToHome btn is present', () => {
    const {getByTestId} = render(<Page404/>);
    getByTestId('testButton');
  });
})