import React from 'react';
import Product from '../../components/Product/Product';

const ProductPage = () => {
  return (
    <main>
       <Product/>
    </main>
  )
}

export default ProductPage;