import React from 'react';
import {render} from '@testing-library/react';
import ProductPage from './ProductPage';

jest.mock('../../components/Product/Product', () => () => <div data-testid='productTest'>test</div>);

describe('ProductPage test', () => {
  test('smoke test', () => {
    render(<ProductPage/>);
  });

  test('component Product is present', () => {
    const {getByTestId} = render(<ProductPage/>);
    getByTestId('productTest');
  });
})