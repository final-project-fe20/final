import React from 'react';
import {Route, Switch} from 'react-router-dom';
import ScrollToTop from '../components/ScrollToTop/ScrollToTop';
import ProtectedRoute from './ProtectedRoute';
import Main from '../pages/Main/Main';
import Catalog from '../pages/Catalog/Catalog';
import Cart from '../pages/Cart/Cart';
import Checkout from '../pages/Checkout/Checkout';
import Cabinet from '../pages/Cabinet/Cabinet';
import Page404 from '../pages/Page404/Page404';
import FooterPagesInfo from '../pages/FooterPages/FooterPagesInfo';
import ProductPage from '../pages/ProductPage/ProductPage';

const FOOTER_PATHS_ARRAY = [
  '/about-us',
  '/sitemap',
  '/policy',
  '/credit',
  '/delivery-and-payment',
  '/service-and-warranty'
]

const AppRoutes = () => {
  return (
    <>
      <ScrollToTop/>
      <Switch>
        <Route exact path='/'><Main/></Route>
        <Route path='/catalog/:category?'><Catalog/></Route>
        <Route path='/cart'><Cart/></Route>
        <Route path='/checkout'><Checkout/></Route>
        <Route path='/product/:itemNo'><ProductPage/></Route>
        <ProtectedRoute path='/cabinet/:section'><Cabinet/></ProtectedRoute>
        <Route path={FOOTER_PATHS_ARRAY}><FooterPagesInfo/></Route>
        <Route path='*'><Page404/></Route>
      </Switch>
    </>
  );
};

export default AppRoutes;
