import React from 'react';
import {Route, Switch} from 'react-router-dom';
import CabinetPersonalInfo from '../components/CabinetPersonal/CabinetPersonal';
import ChangePassword from '../components/ChangePassword/ChangePassword';
import Orders from '../components/Orders/Orders';

const CabinetRoutes = ({showOkMsg}) => {
  return (
    <Switch>
      <Route exact path='/cabinet/personal-info'>
        <CabinetPersonalInfo showOkMsg={showOkMsg}/>
      </Route>
      <Route exact path='/cabinet/change-password'>
        <ChangePassword showOkMsg={showOkMsg}/>
      </Route>
      <Route exact path='/cabinet/orders'>
        <Orders/>
      </Route>
    </Switch>
  );
}

export default CabinetRoutes;