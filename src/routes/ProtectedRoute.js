import React from 'react';
import {Redirect, Route} from 'react-router-dom';
import {connect} from 'react-redux';
import Loader from '../components/Loader/Loader';

const ProtectedRoute = ({isLoading, isAuth, children, ...rest}) => {
  if (isLoading) return <Loader height='800px'/>

  return <Route {...rest} render={() => {
    if (isAuth) {
      return children
    } else {
      return <Redirect to='/'/>
    }
  }}/>
}

const mapStateToProps = (state) => {
  const isLoading = state.customer.isLoading;
  const isAuth = !!state.customer.data.firstName;
  return {
    isLoading,
    isAuth
  }
}

export default connect(mapStateToProps)(ProtectedRoute);