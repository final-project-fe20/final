import React from 'react';
import '@testing-library/jest-dom';

jest.mock('react-router-dom', () => (
  {
    Link: ({children, ...rest}) => <a href='/' data-testid='testLink' {...rest}>{children}</a>,
    NavLink: ({children, activeClassName, ...rest}) => <a href='/' data-testid='testNavLink' {...rest}>{children}</a>,
    useLocation: () => ({pathname: '/testPath'}),
    useParams: () => ({category: 'testCategory'}),
    useHistory: () => ({push: jest.fn()})
  }
));

window.scrollTo = jest.fn();

window.matchMedia = () => ({addListener: jest.fn()});