import {UPDATE_CART} from './types';
import API from '../../utils/API';

export const toggleCartProduct = (productId) => (dispatch, getState) => {
  const {cart} = getState();
  const index = cart.findIndex(({product}) => product === productId);
  const newCart = [...cart];
  if (index === -1) {
    newCart.push({
      product: productId,
      cartQuantity: 1
    })
  } else {
    newCart.splice(index, 1);
  }
  dispatch(updateCart(newCart));
}

export const incCartProductQuantity = (productId) => (dispatch, getState) => {
  const {cart} = getState();
  const index = cart.findIndex(({product}) => product === productId);
  const newCart = [...cart];
  if (index !== -1) newCart[index].cartQuantity++;
  dispatch(updateCart(newCart));
}

export const decCartProductQuantity = (productId) => (dispatch, getState) => {
  const {cart} = getState();
  const index = cart.findIndex(({product}) => product === productId);
  const newCart = [...cart];
  if (index !== -1) {
    if (newCart[index].cartQuantity >= 2) {
      newCart[index].cartQuantity--;
    } else {
      newCart.splice(index, 1);
    }
  }
  dispatch(updateCart(newCart));
}

export const deleteCartProduct = (productId) => (dispatch, getState) => {
  const {cart} = getState();
  const index = cart.findIndex(({product}) => product === productId);
  const newCart = [...cart];
  newCart.splice(index, 1);
  dispatch(updateCart(newCart));
}

export const clearCart = () => (dispatch) => {
  dispatch(updateCart([]));
}

export const setCart = (products) => (dispatch) => {
  dispatch(updateCart(products));
}

export const updateCart = (newCart) => (dispatch) => {
  localStorage.setItem('cart', JSON.stringify(newCart));
  if (API.token) {
    API.request(
      'cart',
      'PUT',
      {products: newCart.length ? newCart : '[]'}
    )
  }
  dispatch({
    type: UPDATE_CART,
    payload: newCart
  })
}