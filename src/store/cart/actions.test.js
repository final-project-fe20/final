import {toggleCartProduct, incCartProductQuantity, decCartProductQuantity, deleteCartProduct, clearCart, setCart} from './actions';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import {UPDATE_CART} from './types';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

const initialStore = {
  cart: [
    {
      product: '1',
      cartQuantity: 10
    }
  ]
}

const clone = obj => JSON.parse(JSON.stringify(obj));

describe('tesing cart actions', () => {
  test('testing toggleCartProduct', async () => {
    const expectedActions = [
      {
        type: UPDATE_CART,
        payload: []
      }
    ];
    const store = mockStore(clone(initialStore));
    await store.dispatch(toggleCartProduct('1'));
    expect(store.getActions().length).toBe(1);
    expect(store.getActions()).toEqual(expectedActions);
  });

  test('testing incCartProductQuantity', async () => {
    const expectedActions = [
      {
        type: UPDATE_CART,
        payload: [
          {
            product: '1',
            cartQuantity: 11
          }
        ]
      }
    ];
    const store = mockStore(clone(initialStore));
    await store.dispatch(incCartProductQuantity('1'));
    expect(store.getActions().length).toBe(1);
    expect(store.getActions()).toEqual(expectedActions);
  });

  test('testing decCartProductQuantity', async () => {
    const expectedActions = [
      {
        type: UPDATE_CART,
        payload: [
          {
            product: '1',
            cartQuantity: 9
          }
        ]
      }
    ];
    const store = mockStore(clone(initialStore));
    await store.dispatch(decCartProductQuantity('1'));
    expect(store.getActions().length).toBe(1);
    expect(store.getActions()).toEqual(expectedActions);
  });

  test('testing deleteCartProduct', async () => {
    const expectedActions = [
      {
        type: UPDATE_CART,
        payload: []
      }
    ];
    const store = mockStore(clone(initialStore));
    await store.dispatch(deleteCartProduct('1'));
    expect(store.getActions().length).toBe(1);
    expect(store.getActions()).toEqual(expectedActions);
  });

  test('testing clearCart', async () => {
    const expectedActions = [
      {
        type: UPDATE_CART,
        payload: []
      }
    ];
    const store = mockStore(clone(initialStore));
    await store.dispatch(clearCart());
    expect(store.getActions().length).toBe(1);
    expect(store.getActions()).toEqual(expectedActions);
  });

  test('testing setCart', async () => {
    const testCart = [
      {
        product: '2',
        cartQuantity: 7
      },
      {
        product: '3',
        cartQuantity: 12
      }
    ]
    const expectedActions = [
      {
        type: UPDATE_CART,
        payload: testCart
      }
    ];
    const store = mockStore(clone(initialStore));
    await store.dispatch(setCart(testCart));
    expect(store.getActions().length).toBe(1);
    expect(store.getActions()).toEqual(expectedActions);
  });
})