import {UPDATE_CART} from './types';

const initialState = JSON.parse(localStorage.getItem('cart')) || [];

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_CART: {
      return action.payload
    }
    default: {
      return state
    }
  }
}

export default reducer;