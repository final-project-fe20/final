import {SET_PRODUCTS, SET_FILTER_QUERY, SET_POPUP_TOP, SET_POPUP_SHOW} from './types';

export const setProducts = (products) => (dispatch) => {
  dispatch({type: SET_PRODUCTS, payload: products});
}

export const setFilterQuery = (filterQuery) => (dispatch) => {
  dispatch({type: SET_FILTER_QUERY, payload: filterQuery});
}

export const setPopupTop = (top) => (dispatch) => {
  dispatch({type: SET_POPUP_TOP, payload: top});
}

export const setPopupShow = (show) => (dispatch) => {
  dispatch({type: SET_POPUP_SHOW, payload: show});
}