import {SET_PRODUCTS, SET_FILTER_QUERY, SET_POPUP_TOP, SET_POPUP_SHOW} from './types';

const initialState = {
  products: [],
  filterQuery: '',
  popup: {
    show: false,
    top: 0
  }
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_PRODUCTS: {
      return {...state, products: action.payload}
    }
    case SET_FILTER_QUERY: {
      return {...state, filterQuery: action.payload}
    }
    case SET_POPUP_SHOW: {
      const popup = state.popup;
      return {...state, popup: {...popup, show: action.payload}}
    }
    case SET_POPUP_TOP: {
      const popup = state.popup;
      return {...state, popup: {...popup, top: action.payload}}
    }
    default: {
      return state
    }
  }
}

export default reducer;