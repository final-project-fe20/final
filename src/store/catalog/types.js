export const SET_PRODUCTS = 'SET_PRODUCTS ';
export const SET_FILTER_QUERY = 'SET_FILTER_QUERY';
export const SET_POPUP_TOP = 'SET_FILTER_POPUP_TOP';
export const SET_POPUP_SHOW = 'SET_FILTER_POPUP_SHOW';