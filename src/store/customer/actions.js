import {SAVE_CUSTOMER_INFO, CLEAR_CUSTOMER_INFO, SET_CUSTOMER_LOADING} from './types';

export const saveCustomerInfo = (info) => (dispatch) => {
  const {
    _id: customerId,
    firstName,
    lastName,
    email,
    telephone,
    gender,
    birthdate,
    isSubscribed
  } = info;
  const newInfo = {
    customerId,
    firstName,
    lastName,
    email,
    telephone,
    gender,
    birthdate: birthdate || '',
    isSubscribed
  }
  dispatch({
    type: SAVE_CUSTOMER_INFO,
    payload: newInfo
  });
};

export const clearCustomerInfo = () => (dispatch) => {
  dispatch({type: CLEAR_CUSTOMER_INFO});
};

export const setCustomerLoading = (isLoading) => (dispatch) => {
  dispatch({type: SET_CUSTOMER_LOADING, payload: isLoading});
};