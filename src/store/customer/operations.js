import {setCustomerLoading, saveCustomerInfo} from './actions';
import API from '../../utils/API';

export const getCustomerInfo = () => async (dispatch) => {
  dispatch(setCustomerLoading(true));
  const {data} = await API.request('customers/customer');
  const {data: { enabled }} = await API.request(`subscribers/${data.email}`);
  dispatch(saveCustomerInfo({...data, isSubscribed: enabled}));
  dispatch(setCustomerLoading(false));
};