import {SET_CUSTOMER_LOADING, SAVE_CUSTOMER_INFO, CLEAR_CUSTOMER_INFO} from './types';

const initialState = {
  isLoading: true,
  data: {
    customerId: '',
    firstName: '',
    lastName: '',
    email: '',
    telephone: '',
    gender: '',
    birthdate: ''
  }
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_CUSTOMER_LOADING: {
      return {
        ...state,
        isLoading: action.payload
      };
    }

    case SAVE_CUSTOMER_INFO: {
      return {
        ...state,
        data: action.payload
      };
    }
    case CLEAR_CUSTOMER_INFO: {
      return {
        ...state,
        data: {
          customerId: '',
          firstName: '',
          lastName: '',
          email: '',
          telephone: '',
          gender: '',
          birthdate: '',
        }
      };
    }
    default: {
      return state;
    }
  }
}

export default reducer;