import {SET_ERROR_VISIBLE, SET_ERROR_TEXT} from './types';

export const setErrorVisible = (isVisible) => (dispatch) => {
  dispatch({
    type: SET_ERROR_VISIBLE,
    payload: isVisible
  });
};

export const setErrorText = (text) => (dispatch) => {
  dispatch({
    type: SET_ERROR_TEXT,
    payload: text
  });
};