import {setErrorVisible, setErrorText} from './actions';

export const showErrorMsg = (text) => (dispatch) => {
  dispatch(setErrorText(text));
  dispatch(setErrorVisible(true));
  setTimeout(() => dispatch(setErrorVisible(false)), 3000);
};