import {SET_ERROR_TEXT, SET_ERROR_VISIBLE} from './types';

const initialState = {
  isVisible: false,
  text: ''
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_ERROR_VISIBLE: {
      return {
        ...state,
        isVisible: action.payload
      }
    }
    case SET_ERROR_TEXT: {
      return {
        ...state,
        text: action.payload
      }
    }
    default: {
      return state
    }
  }
}

export default reducer;