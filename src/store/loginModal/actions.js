import { CLOSE_LOGIN_MODAL, OPEN_LOGIN_MODAL } from './types';

export const openLoginModal = (isActive) => (dispatch) => {
  dispatch({ type: OPEN_LOGIN_MODAL, payload: isActive });
};

export const closeLoginModal = () => (dispatch) => {
  dispatch({ type: CLOSE_LOGIN_MODAL });
};