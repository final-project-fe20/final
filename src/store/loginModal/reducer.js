import {CLOSE_LOGIN_MODAL, OPEN_LOGIN_MODAL} from './types';

const initialState = {
  isActiveLoginModal: false,
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case OPEN_LOGIN_MODAL: {
      return {
        ...state,
        isActiveLoginModal: action.payload
      }
    }
    case CLOSE_LOGIN_MODAL: {
      return {
        ...state,
        isActiveLoginModal: false
      }
    }
    default: {
      return state;
    }
  }
}

export default reducer;