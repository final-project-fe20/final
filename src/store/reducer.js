import {combineReducers} from 'redux';
import loginModal from './loginModal/reducer';
import catalog from './catalog/reducer';
import cart from './cart/reducer';
import customer from './customer/reducer';
import errorMsg from './errorMsg/reducer';

const reducer = combineReducers({
  loginModal,
  catalog,
  cart,
  customer,
  errorMsg
})

export default reducer;