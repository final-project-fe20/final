export const CrossIcon = () => {
  return (
    <svg width='14' height='14' viewBox='0 0 14 14' fill='none' xmlns='http://www.w3.org/2000/svg'>
      <rect x='11.9443' y='0.000701904' width='2.53381' height='16.892' rx='1.2669' transform='rotate(45 11.9443 0.000701904)' fill='#A0A0A0'/>
      <rect x='0.000976562' y='1.79166' width='2.53381' height='16.892' rx='1.2669' transform='rotate(-45 0.000976562 1.79166)' fill='#A0A0A0'/>
    </svg>
  );
}