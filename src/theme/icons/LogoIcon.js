import PropTypes from 'prop-types';

export const LogoIcon = ({baseColor, className}) => (
  <svg
    xmlns='http://www.w3.org/2000/svg'
    className={className}
    width='166'
    height='45'
    fill='black'
    version='1.1'
    viewBox='0 0 166 45'
  >
    <defs>
      <linearGradient>
        <stop offset='0' stopColor='#51ad33' stopOpacity='1'/>
      </linearGradient>
      <linearGradient>
        <stop offset='0' stopColor='#51ad33' stopOpacity='1'/>
      </linearGradient>
    </defs>
    <path
      fill={baseColor || '#DF2725'}
      d='M19.777 27.891H7.1l-2.624 5.964H.966L11.769 10h3.373L25.98 33.855H22.4l-2.624-5.964zm-1.193-2.726l-5.146-11.689-5.145 11.689h10.291zm27.004-3.68c1.386.386 2.476 1.09 3.271 2.112.795 1 1.193 2.272 1.193 3.817 0 2.067-.784 3.657-2.351 4.77-1.545 1.114-3.806 1.67-6.782 1.67H29.537V10h10.7c2.727 0 4.828.546 6.305 1.636 1.5 1.09 2.25 2.601 2.25 4.532 0 1.25-.296 2.329-.887 3.238a5.546 5.546 0 01-2.317 2.078zM32.945 12.76v7.633h6.986c1.75 0 3.09-.318 4.021-.954.954-.66 1.431-1.613 1.431-2.863s-.477-2.192-1.431-2.828c-.931-.659-2.272-.988-4.021-.988h-6.986zm7.838 18.333c1.93 0 3.385-.318 4.362-.954.977-.636 1.465-1.636 1.465-2.999 0-2.658-1.942-3.987-5.827-3.987h-7.838v7.94h7.838zm25.212 3.033c-2.386 0-4.544-.522-6.475-1.567-1.909-1.068-3.408-2.522-4.498-4.362-1.09-1.863-1.636-3.953-1.636-6.27 0-2.318.545-4.397 1.636-6.237 1.09-1.863 2.6-3.317 4.532-4.362 1.931-1.068 4.09-1.601 6.475-1.601 1.863 0 3.567.318 5.111.954a10.297 10.297 0 013.953 2.76l-2.215 2.147c-1.794-1.886-4.032-2.828-6.713-2.828-1.772 0-3.374.397-4.805 1.192a8.734 8.734 0 00-3.374 3.306c-.795 1.386-1.192 2.942-1.192 4.668 0 1.727.397 3.295 1.192 4.703a8.809 8.809 0 003.374 3.271c1.431.796 3.033 1.193 4.805 1.193 2.658 0 4.896-.954 6.713-2.862l2.215 2.147a10.572 10.572 0 01-3.987 2.794c-1.545.636-3.248.954-5.111.954z'
    />
    <text
      xmlSpace='preserve'
      style={{
        lineHeight: '1.25',
        InkscapeFontSpecification: '\'sans-serif, Bold\'',
        fontVariantLigatures: 'normal',
        fontVariantCaps: 'normal',
        fontVariantNumeric: 'normal',
        fontVariantEastAsian: 'normal',
      }}
      x='79.724'
      y='33.242'
      fill={baseColor || '#51ad30'}
      fillOpacity='1'
      stroke='none'
      strokeWidth='0.82'
      fontFamily='sans-serif'
      fontSize='32'
      fontStretch='normal'
      fontStyle='normal'
      fontVariant='normal'
      fontWeight='bold'
      transform='scale(.96828 1.03276)'
    >
      <tspan x='79.724' y='33.242'>
        s
      </tspan>
    </text>
    <text
      xmlSpace='preserve'
      style={{
        lineHeight: '1.25',
        InkscapeFontSpecification: '\'sans-serif, Bold\'',
        fontVariantLigatures: 'normal',
        fontVariantCaps: 'normal',
        fontVariantNumeric: 'normal',
        fontVariantEastAsian: 'normal',
      }}
      x='92.809'
      y='34.308'
      fill={baseColor || '#51ad30'}
      fillOpacity='1'
      stroke='none'
      strokeWidth='0.911'
      fontFamily='sans-serif'
      fontSize='32'
      fontStretch='normal'
      fontStyle='normal'
      fontVariant='normal'
      fontWeight='bold'
      transform='scale(1.00676 .99329)'
    >
      <tspan x='92.809' y='34.308'>
        h
      </tspan>
    </text>
    <text
      xmlSpace='preserve'
      style={{
        lineHeight: '1.25',
        InkscapeFontSpecification: '\'sans-serif, Bold\'',
        fontVariantLigatures: 'normal',
        fontVariantCaps: 'normal',
        fontVariantNumeric: 'normal',
        fontVariantEastAsian: 'normal',
      }}
      x='144.471'
      y='33.639'
      fill={baseColor || '#51ad30'}
      fillOpacity='1'
      stroke='none'
      fontFamily='sans-serif'
      fontSize='32'
      fontStretch='normal'
      fontStyle='normal'
      fontVariant='normal'
      fontWeight='bold'
    >
      <tspan x='144.471' y='33.639'>
        p
      </tspan>
    </text>
    <g>
      <path
        fill={baseColor || '#51ad30'}
        strokeWidth='1.01'
        d='M137.588 14.157l-.036.015c-.02.009-2.181.918-4.67 2.633-1.201.827-2.614 1.94-3.899 3.305l.762.615c3.023.242 5.833 1.206 7.867 2.1 2.84 1.248 4.76 2.586 4.972 2.736l.455.297a13.994 13.994 0 00-.777-6.958 14.461 14.461 0 00-3.487-5.297z'
        opacity='0.75'
      />
      <path
        fill={baseColor || '#51ad30'}
        d='M124.355 10.935a.465.465 0 01-.004.037c-.002.023-.265 2.315.032 5.295.162 1.63.518 3.676 1.272 5.695a18.607 18.607 0 012.424-3.162c1.155-1.223 2.528-2.394 4.082-3.478 2.541-1.772 4.706-2.72 4.946-2.823l.504-.238a15.343 15.343 0 00-2.887-1.72 15.304 15.304 0 00-10.274-.886z'
        opacity='0.75'
      />
      <path
        fill={baseColor || '#51ad30'}
        d='M141.572 26.853a.825.825 0 01-.03-.022c-.018-.014-1.858-1.403-4.558-2.66-1.308-.608-2.967-1.252-4.785-1.652l-.165.955c1.26 2.73 1.783 5.65 1.99 7.865.29 3.084.054 5.426.026 5.684l-.038.536c3.428-1.43 6.163-4.091 7.736-7.547.361-.792.651-1.605.87-2.432z'
        opacity='0.75'
      />
      <path
        fill={baseColor || '#51ad30'}
        d='M132.482 36.877l.003-.038c.003-.023.266-2.344-.03-5.362-.145-1.46-.439-3.248-1.027-5.056l-.935.337a19.656 19.656 0 01-1.745 2.151c-1.155 1.24-2.528 2.424-4.082 3.522-2.54 1.795-4.706 2.755-4.946 2.86l-.494.236c.882.677 1.847 1.26 2.883 1.739 2.031.938 4.2 1.41 6.376 1.41 1.307 0 2.618-.17 3.902-.512z'
        opacity='0.75'
      />
      <path
        fill={baseColor || '#51ad30'}
        d='M115.257 20.974l.03.022c.019.014 1.858 1.403 4.56 2.66 1.306.607 2.964 1.25 4.781 1.65l.166-.965c-1.255-2.724-1.775-5.635-1.983-7.846-.289-3.083-.053-5.424-.025-5.683l.038-.546c-3.43 1.43-6.168 4.09-7.742 7.547a15.386 15.386 0 00-.872 2.434z'
        opacity='0.75'
      />
      <path
        fill={baseColor || '#51ad30'}
        d='M119.136 33.664l.035-.015c.02-.009 2.14-.918 4.583-2.633 1.18-.829 2.567-1.942 3.828-3.309l-.746-.614c-2.967-.243-5.722-1.207-7.718-2.101-2.785-1.247-4.67-2.586-4.878-2.736l-.446-.297a14.238 14.238 0 00.763 6.956c.75 2.022 1.92 3.819 3.423 5.3z'
        opacity='0.75'
      />
      <circle
        cx='128.47'
        cy='23.9'
        r='12'
        fill={baseColor || '#fff'}
        fillOpacity='1'
        stroke='none'
        strokeDasharray='none'
        strokeLinecap='butt'
        strokeLinejoin='miter'
        strokeMiterlimit='4'
        strokeOpacity='1'
        strokeWidth='0.783'
        opacity='0.7'
        paintOrder='normal'
      />
      <g
        opacity='1'
        transform='matrix(.2176 -.00015 .00015 .2192 114.394 10.257)'
      >
        <g fill='#62667c'>
          <path d='M99.158 41.3v42.414H71.175l-1.162 4.523H57.987l-1.162-4.523H28.842V41.3a3.543 3.543 0 013.536-3.536h63.243a3.532 3.532 0 013.537 3.536z'/>
          <path d='M57.645 85.422H28.842v-1.708h27.983z'/>
          <path d='M99.158 83.714v1.708H70.355l.82-1.708z'/>
        </g>
        <path
          fill={baseColor || '#69b0ee'}
          d='M45.353 29.22H83.34v63.037H45.353a.689.689 0 01-.689-.689V29.909a.689.689 0 01.689-.689z'
          transform='rotate(90 64.002 60.738)'
        />
        <path
          fill='#525666'
          d='M105 84.859v1.127a4.254 4.254 0 01-4.254 4.254H27.254A4.254 4.254 0 0123 85.986v-1.127a1.145 1.145 0 011.145-1.145h32.68l1.162 2.443h12.026l1.162-2.443h32.68A1.145 1.145 0 01105 84.859z'
        />
        <g fill='#eeefee' opacity='0.5'>
          <path d='M44.613 80.076h-1.894l28-38.676h1.894z'/>
          <path d='M57.281 80.076h-7.562l28-38.676h7.562z'/>
        </g>
      </g>
    </g>
  </svg>
);

LogoIcon.propTypes = {
  baseColor: PropTypes.string,
  className: PropTypes.string
}