import axios from 'axios';
import {showErrorMsg} from './showErrorMsg';

const BASE_URL = 'https://final-project-fe20.herokuapp.com/api';
const CANCEL_MESSAGE = 'request canceled';

class API {
  constructor (baseUrl) {
    this.baseUrl = baseUrl;
    this.token = '';
    this.cancel = () => {}
  };

  async request (url, method = 'GET', data, headers = {}) {
    return await axios(`${this.baseUrl}/${url}`, {
      method,
      data,
      headers: {
        ...headers,
        Authorization: this.token
      },
      cancelToken: new axios.CancelToken(c => this.cancel = () => c(CANCEL_MESSAGE))
    }).catch(err => {
      if (!url.startsWith('customers') && err.message !== CANCEL_MESSAGE) {
        showErrorMsg(err);
      }
      return Promise.reject(err)
    });
  }
}

export default new API(BASE_URL);