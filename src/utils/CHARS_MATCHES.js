export const CHARS_MATCHES = {
  brand: 'Бренд',
  screenSize: 'Диагональ экрана',
  screenType: 'Тип матрицы',
  cpu: 'Процессор',
  ram: 'Объем оперативной памяти',
  graphicsCard: 'Видеокарта',
  diskType: 'Тип накопителя',
  diskSize: 'Объем накопителя',
  gamePlatform: 'Игровая платформа',
  consoleCard: 'Видеокарта',
  memoryType: 'Тип памяти',
  usbInterface: 'USB Интерфейс',
  usbMemory: 'Объём памяти',
  laptopSize: 'Для диагонали экрана',
  gender: 'Пол',
  standMaterial: 'Материал',
  graphicMemory: 'Объем видеопамяти',
  memoryBus: 'Разрядность шины памяти',
  graphicsChip: 'Графический чип',
  integratedGraphics: 'Интегрированная графика',
  coreNumber: 'Количество ядер',
  frequency: 'Тактовая частота',
  ssdManCountry: 'Страна-производитель',
  cellType: 'Тип ячеек памяти',
  ssdSize: 'Объем накопителя'
}