const SITE_NAME = 'ABC-SHOP';

export default function setPageTitle (title) {
  if (typeof title !== 'string') return;
  document.title = `${title.charAt(0).toUpperCase() + title.substr(1)} — ${SITE_NAME}`;
}