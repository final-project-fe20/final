import store from '../store/store';
import {showErrorMsg as showMsg} from '../store/errorMsg/operations';

export const showErrorMsg = (err) => {
  const responseData = err.response.data;
  const responseText = responseData[Object.keys(responseData)[0]];
  const msgText = responseText.length > 5 ? responseText : err.message;
  store.dispatch(showMsg(msgText));
}